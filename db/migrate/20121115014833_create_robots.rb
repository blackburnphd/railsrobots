class CreateRobots < ActiveRecord::Migration
  def change
    create_table :robots do |t|
      t.string :name
      t.string :file_name
      t.text :comments
      t.boolean :inactive

      t.timestamps
    end
  end
end
