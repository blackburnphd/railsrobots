class RemoveDataPointColumnsFromResults < ActiveRecord::Migration
  def up
    remove_column :results, :losses
    remove_column :results, :remaining_energy
    remove_column :results, :survival_time
    remove_column :results, :wins
    remove_column :results, :kills
    remove_column :results, :damage_given
    remove_column :results, :draws
  end

  def down
    add_column :results, :losses, :integer
    add_column :results, :remaining_energy, :float
    add_column :results, :survival_time, :integer
    add_column :results, :wins, :integer
    add_column :results, :kills, :integer
    add_column :results, :damage_given, :float
    add_column :results, :draws, :integer
  end
end
