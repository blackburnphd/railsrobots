class CreateHelicopters < ActiveRecord::Migration
  def change
    create_table :helicopters do |t|
      t.string :name
      t.integer :expid
      t.timestamps
    end
  end
end
