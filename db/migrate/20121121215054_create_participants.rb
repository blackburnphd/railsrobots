class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.references :experiment, :index => true
      t.references :robot, :index => true

      t.timestamps
    end
  end
end
