class AddCalculatingSummaryToExperiments < ActiveRecord::Migration
  def change
    add_column :experiments, :calculating_summary, :boolean
  end
end
