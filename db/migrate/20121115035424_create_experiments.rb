class CreateExperiments < ActiveRecord::Migration
  def change
    create_table :experiments do |t|
      t.string :name
      t.integer :match_count
      t.text :notes
      t.boolean :running
      t.boolean :completed

      t.timestamps
    end
  end
end
