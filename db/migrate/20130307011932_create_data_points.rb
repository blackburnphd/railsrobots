class CreateDataPoints < ActiveRecord::Migration
  def change
    create_table :data_points do |t|
      t.references :parent, :polymorphic => true, :index => true
      t.string :data_key
      t.float :data_value

      t.timestamps
    end
  end
end
