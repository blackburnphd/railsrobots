class CreateMatches < ActiveRecord::Migration
  def change
      create_table :matches do |t|
      t.integer :duration
      t.references :experiment, :index => true

      t.timestamps
    end
  end
end
