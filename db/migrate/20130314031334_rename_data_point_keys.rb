class RenameDataPointKeys < ActiveRecord::Migration
  def up
    DataPoint.update_all("data_key = 'tot_losses'", "data_key = 'losses'")
    DataPoint.update_all("data_key = 'avg_remaining_energy'", "data_key = 'remaining_energy'")
    DataPoint.update_all("data_key = 'avg_survival_time'", "data_key = 'survival_time'")
    DataPoint.update_all("data_key = 'tot_wins'", "data_key = 'wins'")
    DataPoint.update_all("data_key = 'avg_kills'", "data_key = 'kills'")
    DataPoint.update_all("data_key = 'avg_damage_given'", "data_key = 'damage_given'")
    DataPoint.update_all("data_key = 'tot_draws'", "data_key = 'draws'")
  end

  def down
    DataPoint.update_all("data_key = 'losses'", "data_key = 'tot_losses'")
    DataPoint.update_all("data_key = 'remaining_energy'", "data_key = 'avg_remaining_energy'")
    DataPoint.update_all("data_key = 'survival_time'", "data_key = 'avg_survival_time'")
    DataPoint.update_all("data_key = 'wins'", "data_key = 'tot_wins'")
    DataPoint.update_all("data_key = 'kills'", "data_key = 'avg_kills'")
    DataPoint.update_all("data_key = 'damage_given'", "data_key = 'avg_damage_given'")
    DataPoint.update_all("data_key = 'draws'", "data_key = 'tot_draws'")
  end
end
