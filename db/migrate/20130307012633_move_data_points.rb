require_relative '../../lib/mixins/data_point_interactions'
class Result < ActiveRecord::Base
    include DataPointInteractions
    has_many :data_points, :as => :parent, :dependent => :destroy
  end
  
class MoveDataPoints < ActiveRecord::Migration
  # Create a local model class as suggested here: http://guides.rubyonrails.org/migrations.html#using-models-in-your-migrations
  
  
  def up
    # Create data_point objects for all of the data points associated with every result in the database.
    Result.reset_column_information()
    Result.all().each do |result|
      result.set_data_point("losses", result.losses)
      result.set_data_point("remaining_energy", result.remaining_energy)
      result.set_data_point("survival_time", result.survival_time)
      result.set_data_point("wins", result.wins)
      result.set_data_point("kills", result.kills)
      result.set_data_point("damage_given", result.damage_given)
      result.set_data_point("draws", result.draws)
    end  
  end

  def down
    # Loop through all results and restore data points to their proper attributes.
    Result.reset_column_information()
    Result.all.each do |result|
      # Note, the "|| 0" is to make sure we don't put nil values in these columns.
      result.losses = result.get_data_point("losses") || 0
      result.remaining_energy = result.get_data_point("remaining_energy") || 0
      result.survival_time = result.get_data_point("survival_time") || 0
      result.wins = result.get_data_point("wins") || 0
      result.kills = result.get_data_point("kills") || 0
      result.damage_given = result.get_data_point("damage_given") || 0
      result.draws = result.get_data_point("draws")
      result.save()
    end
  end
end
