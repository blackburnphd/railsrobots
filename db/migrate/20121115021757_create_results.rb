class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.references :robot, :index => true
      t.references :parent, :polymorphic => true, :index => true
      
      t.float :survival_time
      t.float :remaining_energy
      t.float :wins
      t.float :losses
      t.float :kills
      t.float :damage_given

      t.timestamps
    end
  end
end
