class AddDrawsToResults < ActiveRecord::Migration
  # Create a local model class as suggested here: http://guides.rubyonrails.org/migrations.html#using-models-in-your-migrations
  class Result < ActiveRecord::Base
  end
  
  def up
    # Add the new column to store the number of draws.
    add_column :results, :draws, :integer
    
    # Modify a few other columns that don't actually need to hold float values, they can be integer.
    change_column :results, :wins, :integer
    change_column :results, :losses, :integer
    change_column :results, :survival_time, :integer
    
    # Update the existing data and separate the number of draws from the number of wins.
    Result.reset_column_information()
    Result.update_all("draws = 0")
    Match.all.each do |match|
      
      # If more than one robot "won" the match, it was actually a draw between all
      # robots marked as winner.
      winners = []
      match.results.each do |result|
        if result.wins > 0
          winners << result
        end
      end
      
      if winners.length > 1
        winners.each do |actually_it_was_a_draw|
          actually_it_was_a_draw.update_attributes(:wins => 0, :draws => 1)
        end
      end
    end
    
    # Clear the summary results for all experiments.  They must be recalculated
    Experiment.all.each do |experiment|
      experiment.results.clear()
      experiment.save()
    end
  end
  
  def down
    # For all recorded results, add the number of draws to the number of wins.
    Result.update_all("wins = wins + draws")
    
    # Now that we've moved the data out of the draws column, we can drop it.
    remove_column :results, :draws
    
    # Finally, undo the other data type changes we made.
    change_column :results, :wins, :float
    change_column :results, :losses, :float
    change_column :results, :survival_time, :float
  end
end
