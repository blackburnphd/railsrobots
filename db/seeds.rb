# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
robots = Robot.create([{name: "Sitting Duck", file_name: "SittingDuck.rb", inactive: false}, \
                       {name: "Nervous Duck", file_name: "NervousDuck.rb", inactive: false}], \
                       {name: "Lame Duck", file_name: "LameDuck.rb", inactive: false}, \
                       {name: "Adaptive Bot", file_name:"AdaptiveBot.rb", inactive:false} \
                       )
