# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140124021020) do

  create_table "data_points", :force => true do |t|
    t.integer  "parent_id"
    t.string   "parent_type"
    t.string   "data_key"
    t.float    "data_value"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "experiments", :force => true do |t|
    t.string   "name"
    t.integer  "match_count"
    t.text     "notes"
    t.boolean  "running"
    t.boolean  "completed"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.boolean  "calculating_summary"
  end

  create_table "helicopters", :force => true do |t|
    t.string   "name"
    t.integer  "expid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "matches", :force => true do |t|
    t.integer  "duration"
    t.integer  "experiment_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "participants", :force => true do |t|
    t.integer  "experiment_id"
    t.integer  "robot_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "results", :force => true do |t|
    t.integer  "robot_id"
    t.integer  "parent_id"
    t.string   "parent_type"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "robots", :force => true do |t|
    t.string   "name"
    t.string   "file_name"
    t.text     "comments"
    t.boolean  "inactive"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
