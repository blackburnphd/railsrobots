Railsrobots::Application.routes.draw do
  

  
  get "reseterserver/resetme"

  get "helicopter/show"

  get "helicopter/new"

  get "helicopter/create"

  get "helicopter/update"

  get "helicopter/edit"

  get "helicopter/delete"

  get "helicopter/index"

  # Experiment export page
  get "experiments_export/index", :as => :experiments_export
  post "experiments_export/export"

  # Experiment import page
  get "upload/index", :as => :upload
  post "upload/upload_file"

  resources :participants
  resources :data_points  
  
  resources :robots
  post "robots/load_from_directory" => "robots#load_from_directory", :as => "load_from_directory"
  
  resources :matches do
    resources :results
  end
  
  resources :experiments do
    resources :robots
    resources :matches
  end
  post "experiments/:id/run" => "experiments#run", :as => "run_experiment"
  post "experiments/:id/reset" => "experiments#reset", :as => "reset_experiment"
  post "experiments/unstick" => "experiments#unstick", :as => "unstick_experiments"
  post "experiments/run_remaining" => "experiments#run_remaining", :as => "run_remaining_experiments"
  
  resources :results do
    resource :robot
  end

  get "home" => "home#index", :as => "home"
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'home#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
