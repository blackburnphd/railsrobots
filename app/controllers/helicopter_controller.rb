class HelicopterController < ApplicationController
  def index
    if params[:expid] == nil || params[:expid].empty?
      @helicopters = Helicopter.all
    else
      @helicopters = Helicopter.where("expid = ?", params[:expid])
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @helicopters }
    end
  end

  def show
  end

  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def delete
  end
end
