class ExperimentsExportController < ApplicationController
  def index
    # The view code uses this field list to build its array of check boxes.
    @field_list = DataPoint.joins("INNER JOIN results ON results.id = data_points.parent_id AND data_points.parent_type = 'Result'").where("results.parent_type = 'Experiment'").uniq().pluck(:data_key).sort()
  end

  # Export a file including the selected columns for all completed experiments
  def export
    # This parameter value contains the list of fields that the user picked to export.
    if params[:export_fields] == nil
      # No fields picked.  We always do name by default, so just initialize an empty array here.
      export_fields = []
    else
      # This will be an array of strings, one for each of the fields selected.
      export_fields = params[:export_fields]
    end
    
    # Get all completed experiments to export.
    experiments_to_export = Experiment.includes(:participants, :results => [:data_points, :robot]).where("completed = ?", true)
    
    # Find the maximum number of participants
    max_robots = 0
    experiments_to_export.each do |experiment|
        max_robots = [max_robots, experiment.participants.length].max()
    end
    
    # Build the header string, the top row of the CSV file.
    csv_header = ["experiment_name"]
    max_robots.times do |i|
      robot_title = "R" + (i + 1).to_s()
      
      csv_header << "#{robot_title}_name"
      
      export_fields.each do |field|
        csv_header << "#{robot_title}_#{field}"
      end    
    end
    
    # Now, actually build the CSV.
    require 'csv'
    csv_string = CSV.generate do |csv| 
      # header row 
      csv << csv_header 
   
      # data rows 
      experiments_to_export.each do |experiment|
        row_to_export = [experiment.name]
        
        experiment.results.each do |result|
          row_to_export << result.robot.name
          
          export_fields.each do |field|
            row_to_export << result.get_data_point(field) || 0
          end
        end
        
        csv << row_to_export
      end 
    end 
   
    # Finally, send the completed file to the browser.
    send_data csv_string, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=export.csv"      
  end
end
