class ApplicationController < ActionController::Base
  protect_from_forgery


  current_directory = Dir.pwd()
  
  Dir.chdir("/home/hatteam/repo/railsrobots/lib/rrobots/robots")
  all_robots = Dir.glob("*.rb")
  
  
  all_robots.each do |robot_file|
    robot_query = Robot.where("file_name = ?",robot_file)
    if robot_query.length == 0
      robot = Robot.new()
      robot.inactive = false
      robot.file_name= robot_file
      robot.name = File.basename(robot_file,".rb").gsub(/(?<=[A-Za-z])(?=[A-Z])/, ' ').gsub("_", " ")
      robot.save()
    end

  end

  Dir.chdir(current_directory)
  

end
