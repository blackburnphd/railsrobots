class UploadController < ApplicationController
  def index
  end

  def upload_file
    require 'fileutils'
    Rails.logger.info("XXXX detected Upload Controller XXXX")
    if params[:file_upload] == nil or params[:file_upload][:my_file] == nil
      # No file was selected
      respond_to do |format|
        format.html {redirect_to :back, notice: "No file selected."}
        format.json {head :no_content}
      end
      
    else
      uploaded_file = params[:file_upload][:my_file].tempfile
      Rails.logger.info("XXXX uploaded_file = XXXX")
      @file_name = params[:file_upload][:my_file].original_filename
  
      # Load the file into memory
      @columns, @rows, @row_count = parse_csv_file(uploaded_file)
      
      # Build a hash of robot data.  Assume everything after the first two columns of data is robot names.
      @robots = build_robots_hash(@columns[2..-1])
      
      # Add two new columns to store some information
      @columns << "Success" << "Notes"
      
      # Go through each row of the file and create the experiment for each one.
      experiment_ids = []
      @rows.each do |r|
        # Initialize these values
        r["Success"] = true
        r["Notes"] = ""
        
        # See if all the robots we need exist.
        robots_to_add = []
        @columns[2..-1].each do |robot_name|
          if r[robot_name] == "X"
            if @robots[robot_name] != nil
              robots_to_add << @robots[robot_name]
              Rails.logger.info("XXXX added r0b0t  XXXX")
            else
              r["Success"] = false
              r["Notes"] = "Unable to identify Robot #{robot_name}"
 Rails.logger.info("XXXX failed to add robotXXXX")
              break
            end
          end
        end
        
        # Check to see if an experiment with this name already exists.
        if r["Success"]
          experiment_query = Experiment.where("name = ?", r[@columns[0]])
          if experiment_query.count > 0
            r["Notes"] = "Replacing existing Experiment"
            experiment_query[0].delete()
   Rails.logger.info("XXXX deleting overwrite robotXXXX")         
            # Comment out the above and replace it with the below to disable overwriting
            #r["Success"] = false
            #r["Notes"] = "An Experiment with this name already exists"
          end
        end
        
        helo_name = nil
        
        # If we haven't failed yet, create the new experiment.
        if r["Success"]
          experiment = Experiment.new()
          experiment.name = r[@columns[0]]
          # modified to start at column 2 instead of column 1 
          # due to helicopter name - BP 26 Jan 2014
          experiment.match_count = r[@columns[1]].to_i()
          experiment.save()
          
          # get helo name - BP 26 Jan 2014
          puts "-> attempting to get helo"
          logger.debug("** attempting to get helo")
          helo_name = r[@columns[2]]
          
          
          robots_to_add.each do |robot|
            participant = Participant.new()
            participant.robot = robot
            participant.experiment = experiment
            participant.save()
          end
          
          if helo_name != nil then
            helicopter = Helicopter.new()
            helicopter.name = helo_name
            helicopter.expid = experiment.id
            helicopter.save()
          else
            puts "WARNING - No helicopter defined"
          end
          
          experiment_ids << experiment.id
            Rails.logger.info("XXXX added experiment XXXX")    
        end
      end
      
      
      logger.debug("** HELOS HAVE BEEN LOADED **")
      #Start a batch of all of the experiments
      if experiment_ids.length > 0
        require_relative '../../lib/workers/experiment_batch_worker'
        worker = ExperimentBatchWorker.new()
        worker.experiment_ids = experiment_ids
        worker.callback_site = "http://localhost:" + request.port.to_s() #request.protocol + request.host + ":" + request.port.to_s()
         Rails.logger.info("XXXX running experiment  XXXX")
        worker.run()
      end
    end
  end
  
  private
  
  #-------------------------------------------------------------------------------------------
  # Build and return two hashes and a row count given a CSV file to read.
  def parse_csv_file(file)
    columns = file.readline.chomp.split(',')
    rows = []
    row_count = 0
    
    until file.eof?
      row = file.readline.chomp.split(',')
      row = columns.zip(row).flatten()
      rows << Hash[*row]
      row_count += 1
    end
    
    return columns, rows, row_count
  end
  
  
  #-------------------------------------------------------------------------------------------
  # Build and return a hash of ActiveRecord objects for the specified robots.  If a robot
  # can't be found, that element of the hash is set to nil.
  def build_robots_hash(robot_list)
    robots = Hash.new()
    robot_list.each do |robot_name|
      possible_robots = Robot.where("name = ? or file_name = ?", robot_name, robot_name)
      
      if possible_robots.count == 1
        robots[robot_name] = possible_robots[0]
      else
        robots[robot_name] = nil
      end
    end
    
    return robots
  end
end
