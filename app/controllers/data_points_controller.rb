class DataPointsController < ApplicationController
  # GET /data_points.json
  def index
    @data_points = DataPoint.all

    respond_to do |format|
      format.json { render json: @data_points }
    end
  end

  # GET /data_points/1.json
  def show
    @data_point = DataPoint.find(params[:id])

    respond_to do |format|
      format.json { render json: @data_point }
    end
  end

  # GET /data_points/new.json
  def new
    @data_point = DataPoint.new

    respond_to do |format|
      format.json { render json: @data_point }
    end
  end

  # POST /data_points.json
  def create
    @data_point = DataPoint.new(params[:data_point])

    respond_to do |format|
      if @data_point.save
        format.json { render json: @data_point, status: :created, location: @data_point }
      else
        format.json { render json: @data_point.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /data_points/1.json
  def update
    @data_point = DataPoint.find(params[:id])

    respond_to do |format|
      if @data_point.update_attributes(params[:data_point])
        format.json { head :no_content }
      else
        format.json { render json: @data_point.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /data_points/1.json
  def destroy
    @data_point = DataPoint.find(params[:id])
    @data_point.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end
end
