class ExperimentsController < ApplicationController
  # GET /experiments
  # GET /experiments.json
  def index
    if params[:summary] and params[:experiment_id] != nil
      @experiments = DataPoint.find_by_sql(Experiment.get_summary_query(params[:experiment_id]))  
    else
      @experiments = Experiment.includes(:participants, :results => [:data_points, :robot]).all()
    end
          
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @experiments }
    end
  end

  # GET /experiments/1
  # GET /experiments/1.json
  def show
    @experiment = Experiment.includes(:participants, :results => [:data_points, :robot]).find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @experiment }
    end
  end

  # GET /experiments/new
  # GET /experiments/new.json
  def new
    @experiment = Experiment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @experiment }
    end
  end

  # GET /experiments/1/edit
  def edit
    @experiment = Experiment.find(params[:id])
  end

  # POST /experiments
  # POST /experiments.json
  def create
    @experiment = Experiment.new(params[:experiment])

    respond_to do |format|
      if @experiment.save
        format.html { redirect_to @experiment, notice: 'Experiment was successfully created.' }
        format.json { render json: @experiment, status: :created, location: @experiment }
      else
        format.html { render action: "new" }
        format.json { render json: @experiment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /experiments/1
  # PUT /experiments/1.json
  def update
    @experiment = Experiment.find(params[:id])

    respond_to do |format|
      if @experiment.update_attributes(params[:experiment])
        format.html { redirect_to @experiment, notice: 'Experiment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @experiment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /experiments/1
  # DELETE /experiments/1.json
  def destroy
    @experiment = Experiment.find(params[:id])
    @experiment.destroy

    respond_to do |format|
      format.html { redirect_to experiments_url }
      format.json { head :no_content }
    end
  end
  
  
  # New action to run experiments
  def run
    require_relative '../../lib/workers/experiment_worker.rb'
    worker = ExperimentWorker.new()
    
    # The worker class uses ActiveResource to write its data back in order to be thread safe.  Have to give it the site
    # URL to use, since we shouldn't assume that we're local host port 3000.
    worker.callback_site = "http://localhost:" + request.port.to_s() #request.protocol + request.host + ":" + request.port.to_s()

    worker.experiment_id = params[:id].to_i() 
    worker.run()
    
    respond_to do |format|
      format.html {redirect_to :back, notice: worker.status_message}
      format.json {head :no_content}
    end
  end
  
  
  # New action to reset the results of an experiment
  def reset
    @experiment = Experiment.find(params[:id])
    status_message = @experiment.reset()
    
    respond_to do |format|
      format.html {redirect_to :back, notice: status_message}
      format.json {head :no_content}
    end
  end
  
  
  # New action to deal with crashes.
  def unstick
    experiments = Experiment.where("running = 't' OR calculating_summary = 't'")
    
    count = experiments.count
    experiments.each do |experiment|
      experiment.reset(true)
    end
    
    respond_to do |format|
      format.html {redirect_to :back, notice: "Force-reset #{count} Experiment(s)."}
      format.json {head :no_content}
    end
  end
  
  
  # New action to deal with restarting after crashes
  def run_remaining
    require_relative '../../lib/workers/experiment_batch_worker.rb'
    worker = ExperimentBatchWorker.new()
    
    # The worker class uses ActiveResource to write its data back in order to be thread safe.  Have to give it the site
    # URL to use, since we shouldn't assume that we're local host port 3000.
    worker.callback_site = "http://localhost:" + request.port.to_s() #request.protocol + request.host + ":" + request.port.to_s()
    
    worker.experiment_ids = Experiment.where("completed = 'f' OR completed IS NULL").map {|experiment| experiment.id}
    
    worker.run()
    
    respond_to do |format|
      format.html {redirect_to :back, notice: worker.status_message}
      format.json {head :no_content}
    end 
  end
end