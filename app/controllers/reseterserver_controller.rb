class ReseterserverController < ApplicationController
  # Kill the rrobots process
  def resetme
    pid_location = "/home/hatteam/repo/railsrobots/tmp/pids/server.pid"
    process_number = nil
    if(File.exists?pid_location) then
      f = File.open(pid_location, "r")
      f.each_line do |line|
        process_number = line
      end
      f.close
      kill_command = "kill "+process_number
      puts "kill command = "+kill_command
      puts "  attempting kill command " + kill_command
      exec kill_command
      puts "!!!!!!!!!!!!!!!!!!!! "+kill_command      
    end
  end
end
