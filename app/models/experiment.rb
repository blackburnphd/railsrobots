class Experiment < ActiveRecord::Base
  has_many :participants, :dependent => :destroy
  has_many :robots, :through => :participants
  has_many :results, :as => :parent, :dependent => :destroy
  has_many :matches, :dependent => :destroy
  
  
  attr_accessible :match_count, :name, :notes, :participants_attributes, :running, :completed, :calculating_summary
  
  validates :name, :match_count, :presence => true
  validates :name, :uniqueness => { :case_sensitive => false }
  
  accepts_nested_attributes_for :participants, :allow_destroy => true,
                                :reject_if => proc { |p| p["robot_id"].blank?}
  
  
  # Prevent the timestamp fields from being sent via JSON.  This is to stop problems with ActiveResource
  def as_json(options = {})
    super(:except => [:created_at, :updated_at])
  end
  
  # Resets experiment data.  Optionally force reset even if the experiment still appears to be running.  Returns a status message.
  def reset(force = false)
     if self.running? and !force
       "Experiment is running.  Wait until it has finished."
     else
       self.matches.clear()
       self.results.clear()
       self.running = false
       self.completed = false
       self.calculating_summary = false
       self.save()
       return "Experiment data reset."
     end
  end
  
  # This is a special method just to keep the code DRY, so this query is only written out in once
  # place.
  def self.get_summary_query(experiment_id)
    return "SELECT r.robot_id, d.data_key,
                  MIN(d.data_value) AS min_data_value, 
                  MAX(d.data_value) AS max_data_value, 
                  AVG(d.data_value) AS avg_data_value, 
                  SUM(d.data_value) AS tot_data_value 
          FROM data_points AS d 
          INNER JOIN results AS r ON r.id = d.parent_id AND d.parent_type = 'Result'   
          INNER JOIN matches AS m ON m.id = r.parent_id AND r.parent_type = 'Match' 
          INNER JOIN experiments AS e ON e.id = m.experiment_id 
          WHERE e.id = #{experiment_id} 
          GROUP BY r.robot_id, d.data_key"
  end
end

