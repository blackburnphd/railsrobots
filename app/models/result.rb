require_relative '../../lib/mixins/data_point_interactions'

class Result < ActiveRecord::Base
  include DataPointInteractions
  
  # A Result doesn't actually belong to a robot, but this is how you declare a foreign key relationship in rails.
  belongs_to :robot
  
  belongs_to :parent, :polymorphic => true
  
  has_many :data_points, :as => :parent, :dependent => :destroy

  # These are the attributes we have to expose as writeable so that ActiveResource access can update the relationships between objects.
  attr_accessible :robot_id, :parent_id, :parent_type
  
  
  # Prevent the timestamp fields from being sent via JSON.  This is to stop problems with ActiveResource
  def as_json(options = {})
    super(:except => [:created_at, :updated_at])
  end
end
