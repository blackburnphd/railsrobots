class Robot < ActiveRecord::Base
  attr_accessible :comments, :file_name, :inactive, :name
  
  validates :name, :file_name, :presence => true
  validates :name, :file_name, :uniqueness => { :case_sensitive => false }
  
  # Prevent the timestamp fields from being sent via JSON.  This is to stop problems with ActiveResource
  def as_json(options = {})
    super(:except => [:created_at, :updated_at])
  end
end
