class Match < ActiveRecord::Base
  belongs_to :experiment
  has_many :results, :as => :parent, :dependent => :destroy
  
  attr_accessible :duration
  
  attr_accessible :experiment_id
  
  # Prevent the timestamp fields from being sent via JSON.  This is to stop problems with ActiveResource
  def as_json(options = {})
    super(:except => [:created_at, :updated_at])
  end
end
