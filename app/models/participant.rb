class Participant < ActiveRecord::Base
  belongs_to :experiment
  belongs_to :robot
  
  attr_accessible :robot_id
end
