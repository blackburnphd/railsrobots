class DataPoint < ActiveRecord::Base
  belongs_to :parent, :polymorphic => true
  attr_accessible :data_key, :data_value
  attr_accessible :parent_id, :parent_type
  
  # Prevent the timestamp fields from being sent via JSON.  This is to stop problems with ActiveResource
  def as_json(options = {})
    super(:except => [:created_at, :updated_at])
  end
  
end
