class Battlereport
  attr_accessor :time
  attr_accessor :tankClass
  attr_accessor :x
  attr_accessor :y
  attr_accessor :speed
  attr_accessor :gun_heading
  attr_accessor :heading      
  attr_accessor :distToWall
  attr_accessor :distanceFromStart
  attr_accessor :turretRotation
  attr_accessor :shots
  attr_accessor :lastShotPower
  attr_accessor :suspectedAimingStrategy
        
        
  def initialize
    @time =0
    @tankClass = "unknown"
    @x =0
    @y =0
    @speed = 0
    @gun_heading =0
    @heading = 0
    @distToWall = 999999
    @distanceFromStart =999999
    @turretRotation=0
    @shots =0
    @lastShotPower=0
    @suspectedAimingStrategy=-1
    
  end      
        
  def report
    myline=  @time.to_s+"\t"+tankClass+"\t"
    myline = myline+ @x.to_s+"\t"+@y.to_s+"\t"+@speed.to_s
    myline = myline+@heading.to_s+"\t"+@gun_heading.to_s
    myline = myline + "\t" + @distToWall.to_s
    myline = myline + "\t" + @distanceFromStart.to_s
    myline = myline + "\t" + @turretRotation.to_s
    myline = myline + "\t" + @shots.to_s
    myline = myline + "\t" + @lastShotPowerst_shot_power.to_s
    myline = myline + "\t" + @suspectedAimingStrategy.to_s
    return myline
  end
  
end