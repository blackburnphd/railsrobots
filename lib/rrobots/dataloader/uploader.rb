require 'net/http/post/multipart'

server = "192.168.0.20"
#server = "localhost"

url = URI.parse('http://'+server+':3000/upload/upload_file')
puts "server name = " + url.to_s

#filepath = "/home/hatteam/repo-rails-1/railsrobots/lib/rrobots/dataloader/ztest.rb.csv"

filepath = "C:\\Users\\Brian\\Documents\\Aptana Studio 3 Workspace\\railsrobots\\sourceFiles\\Helo50_HB_0_0p1_0_0_0.rbsmall.csv"
shortname = "Helo50_HB_0_0p1_0_0_0.rbsmall.csv"

if File.file?(shortname) then
	puts "can not find file " + filepath
end
	

puts "preparing to upload"


req = Net::HTTP::Post::Multipart.new url.path,   \
  "file_upload[my_file]" => UploadIO.new(File.new(filepath),    \
  "text/csv", shortname),  "utf8" => "&#x2713",  \
  "authenticity_token"=>"B82xPNEpx7ksNjzsS6XIF4DxBBlrmCnN5LHY0++RgUY="

res = Net::HTTP.start(url.host, url.port) do |http|
	res= http.request(req)
	puts res.to_s
end
