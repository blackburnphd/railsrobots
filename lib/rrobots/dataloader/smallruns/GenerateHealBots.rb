puts "Generate Heal Bots"


files = []

heloCollection = ["Helo25","Helo50","Helo75","Helo100"]



Dir.glob('../../robots/HB_*.rb') do |rb_file|
  myfile = File.basename(rb_file, ".rb")
  myfile = myfile+".rb"
  puts "Added a file "+myfile
  files << myfile 
end



def createInputFile(heloName,robotName, fileIndex, files)

myfilename = "./"+heloName+"_"+robotName+"small.csv"
#myfilename = "C:/Users/Brian/bot_test50/"+heloName+"_"+robotName+".csv"
  File.open(myfilename, 'w') do |hb| 
   
    
    # write the header here
    hb.print "Experiment Name,"
    hb.print "Trial Count,"
    hb.print "Helicopter"
    files.each do |myfile|
      hb.print ","
      hb.print myfile
    end
    hb.puts
    
    # write one line per experiment
    i=0
    
#    files.each do |myfile|

      files.each do |myfile2|
        firstpart = "Match_"+heloName + "_" + fileIndex.to_s + ",10," + heloName
        #firstpart = "Match_"+heloName + "_" + fileIndex.to_s + ",100," + heloName
        fileIndex = fileIndex +1
        s = getFileXString(files,myfile2,robotName)
        if s != nil then
          hb.puts firstpart + s
          
        end
      end  
 #   end
    
   end
    
  #  puts "created file "+myfilename+" fileIndex"
    return fileIndex
end


# Create the list that correlates which robot fights which robot.
# f is a list of all robot names
# r1 is robot 1
# r2 is robot 2
def getFileXString(f,r1,r2)
  if r1==r2 then
    return nil
  end
  s = "";
  f.each do |myfile|
    s= s + ","
    if myfile == r1 then
      s=s + "X"
    end
    if myfile == r2 then
      s=s+  "X"
    end
  end
  return s
end






=begin
  
MULTI-LINE comment



firing_strategy_list = [0,1,2]
fire_power_list = [0.1, 0.6, 1.1, 1.6, 2.1, 2.6,1.0]
aiming_strategy_list = [0,1]
movement_strategy_list = [0,1,2,3,4]
resupply_strategy_list = [0,1,2,3]


header1 = "require_relative '../robust_robot'"
header2 = "require_relative './StaticBasis'"

ct =0
firing_strategy_list.each do |firing_strat|
  fire_power_list.each do |fire_power|
    aiming_strategy_list.each do |aim_strat|
      movement_strategy_list.each do |move_strat|
        resupply_strategy_list.each do |resup_strat|
          myclassname = "HB_"+firing_strat.to_s+"_"+fire_power.to_s.gsub("\.","p")+"_"+aim_strat.to_s+"_"+move_strat.to_s+"_"+resup_strat.to_s
          myfilename = "../robots/"+myclassname+".rb";
          puts myfilename
          ct = ct +1
          
          File.open(myfilename, 'w') do |hb|  
            hb.puts header1
            hb.puts header2
            hb.puts "class "+myclassname+"  < StaticBasis"
            hb.puts "  include RobustRobot\n"
            hb.puts "  def initialize"
            hb.puts "    super()"
            hb.puts "    @firing_strategy = "+firing_strat.to_s
            hb.puts "    @fire_power = "+fire_power.to_s
            hb.puts "    @aiming_strategy = "+aim_strat.to_s
            hb.puts "    @movement_strategy = "+move_strat.to_s
            hb.puts "    @resupply_strategy = "+resup_strat.to_s
            hb.puts "  end"
            hb.puts "end" 
          end
        end
      end
    end
  end
end

puts "Total new tank classes ="+ct.to_s


=end
matchid =0
heloCollection.each do |heloname|
  
  files.each do |therobotfile|
    puts "creating file for robot "+therobotfile
    matchid = createInputFile(heloname,therobotfile,matchid,files)
  end
end





