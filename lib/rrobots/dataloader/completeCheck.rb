
# Use this ruby script to count the input files and make sure they are all present.
# report if any are missing

@filenames_expected = Array.new
@prefix = "export_export__HB_"

@filenames_not_found = Array.new
@output_directory = "C:\\Users\\Brian\\Documents\\APTANA~1\\railsrobots\\output\\"
@suffix = ".CSV"



# export_export__HB_2_2p6_1_4_3.CSV
def generateFilenames

  items1=["0","1","2"]
  items2=["0p1","0p6","1p0","1p1","1p6","2p1","2p6"]
  items3=["0","1"]
  items4=["0","1","2","3","4"]
  items5=["0","1","2","3"]
  
  ct = items1.length*items2.length*items3.length*items4.length*items5.length
  puts "generating filenames for " + ct.to_s + " files"
  
  items1.each do |it1|
    items2.each do |it2|
      items3.each do |it3|
        items4.each do |it4|
          items5.each do |it5|
            filename =  @prefix
            filename = filename + it1 + "_" + it2 + "_" + it3 + "_" + it4 + "_" + it5+@suffix
            filename = @output_directory+filename
            @filenames_expected << filename
            if File.exists?(filename) == false then
              @filenames_not_found << filename
              puts "missing: "+filename
            end
          end
        end
      end
    end
  end
  
end

generateFilenames