require 'csv'
require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'C:\Users\Brian\Documents\Aptana Studio 3 Workspace\railsrobots\lib\rrobots\dataloader\renamer.rb'
require 'yaml'
require 'net/ssh'
require 'net/http/post/multipart'
require 'net/http'



$ip_addresses = Array.new
$status_values = Array.new 
$time_stamps = Array.new
$ready_to_download = Array.new
$ready_to_shutdown = Array.new
$robots_loaded = Array.new
$authorization_tags = Array.new

$configuration_file="./config.yaml"

# These are configuration parameters - make sure to change
# then when you install on a different server
$FILESOURCE = "/home/hatteam/sourceFiles/"
$FILERUNNING = "/home/hatteam/runningFiles/"
$FILEDONE = "/home/hatteam/finishedFiles/"
$DOWNLOAD_TARGET = "/home/hatteam/Downloads/z.download"
$RESULTS_DIRECTORY="/home/hatteam/Downloads/output/"
$PREPEND_TO_OUTPUTFILE="export_"
$statusfile="/home/hatteam/repo/railsrobots/systemStatus.csv"
$webgetter_target = "~/Downloads/download.csv"
$webgetter_command = "java -jar /home/hatteam/repo/railsrobots/lib/rrobots/dataloader/webgetter.jar "  
$webputter_command = "java -jar /home/hatteam/repo/railsrobots/lib/rrobots/dataloader/webputter.jar " 

# this is useful for marking special purpose experiment files
$SPECIAL_TAG=".rbsmall.csv"
$status_value_running="running"
$status_value_complete="complete"
$status_value_upload="ready to upload file"

# reporting data structures goes here
$startTime = Time.new
@listOfDownloadedFiles = Array.new
@listOfStartedServers = Array.new 
@listOfShutdownServers = Array.new
@already_started_hash = Hash.new 
@filesThatWereLoaded = Array.new
 

# This function returns the number of seconds since the start of the program.
# Use this to report performance
def getTimeFromStart
  currentTime = Time.new
  time_diff_seconds = currentTime - $startTime
  time_diff_seconds = time_diff_seconds.to_i.abs 
  return time_diff_seconds
end


# Read in the status file to deterine ip addresses, time stamps, and download readyness
# set up control variables for other functions.
def getStatusStartingValues
  if File.exists?($statusfile) == false then
    puts "No status file exists at " + $statusfile
  end
  
  CSV.foreach($statusfile) do |row|
    $ip_addresses << row[0]
    $status_values << row[1]
    $time_stamps << row[2]  
    $ready_to_download << false
    $ready_to_shutdown << false
    $robots_loaded << false
    $authorization_tags << nil
  end
end


# Display the starting status values for debugging purposes
def reportStatusValues
  $ip_addresses.each_index do |my_index| 
    line = $ip_addresses[my_index] + "," + $status_values[my_index] + "," + $time_stamps[my_index]
    puts line
  end
end


# find any files in the source directory that also exist in the running directory.  Clean those out.
def removeFilesFromSourceThatExistInRuns
   mypattern = $FILERUNNING+"*csv"
      Dir.glob(mypattern).each do |fileOfConcern|
        file_to_remove = $FILESOURCE + File.basename(fileOfConcern) 
        if File.exists?(file_to_remove) then
          File.delete(file_to_remove)
        end
      end
end


# find any files in the finished directory that also exist in the running directory, clean those out
def removeFilesFromRunningThatExistInFinished
   mypattern = $FILEDONE + "*.csv"
      Dir.glob(mypattern).each do |fileOfConcern|
        if File.exists?( fileOfConcern) == false then
          puts "CAN NOT FIND "+mypattern
          next
        end
        file_to_remove = $FILERUNNING + File.basename(fileOfConcern) 
        if File.exists?(file_to_remove) then
          File.delete(file_to_remove)
        end
      end
end


# Load in a yaml configuration file to change these parameters as needed.
def loadConfiguration
  if File.exists?($configuration_file)==false
    return
  end
  config = YAML.load_file(File.new $configuration_file)
  $FILESOURCE= config['config']['FILESOURCE']
  $FILERUNNING = config['config']['FILERUNNING']
  $FILEDONE = config['config']['FILEDONE']
  $DOWNLOAD_TARGET = config['config']['DOWNLOAD_TARGET']
  $RESULTS_DIRECTORY = config['config']['RESULTS_DIRECTORY']
  $PREPEND_TO_OUTPUTFILE = config['config']['PREPEND_TO_OUTPUTFILE']
  $statusfile = config['config']['statusfile']
  $webgetter_target = config['config']['webgetter_target']
  $webgetter_command = config['config']['webgetter_command']
  $webputter_command = config['config']['webputter_command']
  puts 'loaded new configuration file'
  puts 'statusfile=' + $statusfile
end


# Read each live server web pabe to determine any necessary authorization tokens
def getAuthorizationTokens
  $ip_addresses.each_index do |my_index|
    #authorization_tags
    url = "http://"+$ip_addresses[my_index]+":3000/upload/index"
    page = {}
     begin
       page = Nokogiri::HTML(open(url))
     rescue 
       next
     end
     token = page.css("input[name=authenticity_token]")
     puts "token length = "+token.length.to_s
     myauth = token.first['value']
     $authorization_tags[my_index] = myauth
  end
end


# for each ip address, determine if that server is ready 
# to receive a new input file
def checkReadyToLoad()
   $ip_addresses.each_index do |my_index| 
     if $status_values[my_index]==$status_value_complete then
   	 next
     end
     url = "http://"+$ip_addresses[my_index]+":3000/experiments"
     page = {}
     begin
     page = Nokogiri::HTML(open(url))
       rescue 
         $status_values[my_index]="please start server"
          startRemoteViaSSH($ip_addresses[my_index], "hatteam","hatteam")
          puts $ip_addresses[my_index] + " needs to start"
         next
     end
     rows = page.css("tr")
     time1 = Time.new
     mytime= time1.strftime("%Y-%m-%d %H:%M:%S")
     $time_stamps[my_index]=mytime
     if rows.size == 1 then
       $status_values[my_index]=$status_value_upload      
       puts $ip_addresses[my_index] + " ready to upload new experiment"
     end
     if rows.size > 1 then
       $status_values[my_index] = $status_value_running
       puts $ip_addresses[my_index] + " currently running"
     end
   end
end
 
 
# Start a remote server via ssh.  This will spawn a new rrobots daemon using the ~/cleanup script.
def startRemoteViaSSH(host, username, password)
   begin
    puts "starting remote at "+host+" user name = "+username + " pw = "+password
    ssh = Net::SSH.start(host, username, :password => password)
    puts 'setting the command_cleanup trigger'
    cmd ="[[ -s \"$HOME/.rvm/scripts/rvm\" ]] && source \"$HOME/.rvm/scripts/rvm\" ; nohup /home/hatteam/cleanup"
    Thread.new {
      res = ssh.exec(cmd)
      ssh.close
      puts res  
    }
    @listOfStartedServers << host
  rescue
    puts "Unable to connect to #{@hostname} using #{@username}/#{@password}"
  end
  sleep(60.0)
end 


# Detect when this URL is ready for a download
# set the $ready_to_download[] value to true to indicate it is ready. 
def checkForFinishedServers(host,my_index)
#   $ip_addresses.each_index do |my_index| 
     url = "http://" + host + ":3000/experiments"
     
     page = {}
     begin
       page = Nokogiri::HTML(open(url))
     rescue 
       puts "failed to open "+url
       return
     end
     #puts rows
     rows = page.css("tr")
     if rows.size == 0 then 
       return
     end
     goodrows = 0
     for index in 1 ... rows.size
        myrow =rows[index]
        children = myrow.css("td")
        matchcount = children[1].text
        content_left = children[3].text.strip
        content_right = children[4].text.strip
        if content_right == nil then
           next
        end
        if content_left ==nil then
           next
        end
        if content_right == "" then
           next
        end
        if content_left == "" then
           next
        end
        cl = content_left.index(")")
        cr = content_right.index(")")  
        if cl != nil  then
            if cr != nil then
              goodrows = goodrows +1
            end
        end
     end
     v = goodrows.to_f / ( rows.size - 1 ).to_f
     if v == 1.0 then
       $ready_to_download[my_index] = true
       puts "found a ready download: "+host
     end
#  end 
end


# Copy a file from one place to another.
def copyFile(from_file,to_file)
  read_data = IO.read(from_file)
  File.open(to_file, "w") do |f|
    f.write read_data
  end
end


# loop through each server and check to see if 
# it is ready for download
def prepareFinishedServers
  $ip_addresses.each_index do |myindex|
    checkForFinishedServers($ip_addresses[myindex],myindex)
  end
end


# Iterate through all servers to find finished runs, 
# download from that server.
def downloadFilesFromFinishedServersAndRename
  puts "downloading from finished servers"
  $ip_addresses.each_index do |my_index| 
    myIP = $ip_addresses[my_index]
    readyToDl = $ready_to_download[my_index]
    if readyToDl == true then
      puts "preparing to download file"      
      cmd = $webgetter_command + " "+myIP 
      puts cmd
      system cmd      
      puts "moving file"
      # RENAME HERE AND PLACE RESULTS IN STORAGE
      source_file = "download.csv"  
      renamedfile = renameFile(source_file, $PREPEND_TO_OUTPUTFILE)          
      source_file = renamedfile
      sink_file = $RESULTS_DIRECTORY+(File.basename renamedfile)
      puts "downloading file " + source_file + " to " + sink_file
      if File.exists?(sink_file) then
        File.delete(sink_file)
        puts "ERROR - file "+sink_file+" already existed.  You are running this experiment multiple times."
        $ready_to_shutdown[my_index]=true       
        next
      end
      File.rename source_file, sink_file
      @listOfDownloadedFiles << sink_file
      # MOVE EXPERIMENT FILE TO CORRECT DIRECTORY - this indicates a finished run
      #  a.  get the base name of the file that we just downloaded
      #  b.  remove the last .CSV from the file name
      #  c.  match all files in the $FILERUNNING directory
      #  d.  move all matches to the $FILEDONE directory
      base_name = File.basename renamedfile
      l = base_name.length - 5
      base_name = base_name[7..l]
      mypattern = $FILERUNNING+"*"+base_name+"*"
      Dir.glob(mypattern).each do |fileOfConcern|
        place_to_copy = $FILEDONE + File.basename(fileOfConcern) 
        if File.exists?(place_to_copy) then
          File.delete(place_to_copy)
          puts "ERROR - file "+place_to_copy+" already existed.  You are running this experiment multiple times."
        end
        copyFile(fileOfConcern,place_to_copy)
      end
      $status_values[my_index]=$status_value_complete
      time1 = Time.new
      mytime= time1.strftime("%Y-%m-%d %H:%M:%S")
      $time_stamps[my_index]=mytime
      $ready_to_shutdown[my_index]=true          
    end
  end
end


# Use the resetme web service to kill the rrobots daemon
def shutdownFinishedServers
    puts "preparing server shutdown"
  $ip_addresses.each_index do |my_index|
    if $ready_to_shutdown[my_index]==true then
      url_str = "http://"+$ip_addresses[my_index]+":3000/"
      puts "  shutting down "+ url_str
      url_str = url_str + "reseterserver/resetme"    
      url = URI.parse(url_str)
      puts "  preparing get statement"
      req = Net::HTTP::Get.new(url.to_s)
      begin
        puts "  shuting down now"
        res = Net::HTTP.start(url.host,url.port){|http|
             http.request(req) }
        puts res.body
      rescue
        # this shuts the server down, so it triggers an error because
        # the response will die
        puts "shut server down "+$ip_addresses[my_index]
        @listOfShutdownServers << $ip_addresses[my_index]
       end
    end #end if ready_to_shutdown
  end #end each_index
end #end def

 
# load a single csv experiment file
# to a single server.
# move the source file to the running directory.
def loadExperimentFile(myfilename, myipaddress,myAuthTag)
  puts "  @@ loading experiment file "+ myfilename
  puts "  @@ loading experiment file "+ myipaddress
  
  url = 'http://'+myipaddress+':3000/upload/upload_file'
  
  if File.file?(myfilename)==false then
    puts "cannot file file [" + myfilename +"]"
    return false
  end
  copy_file_here = $FILERUNNING + File.basename(myfilename) 
  puts "loading experiment file"
  begin
    myfile = File.new myfilename
    mybasename = File.basename(myfilename)   
    puts " ** preparing to upload experiment file **"      
    cmd = $webputter_command + " -url "+ url + " -file \""+myfilename+"\"" 
    if myAuthTag then
      cmd = cmd + " -auth "+myAuthTag
    end
    puts cmd
    system cmd      
    puts "moving experiment file "
  end
  return true
end



#upload an experiment CSV file for processing.
def loadExperimentsIntoServers
  $status_values.each_index do |myindex|
    if $status_values[myindex]== $status_value_upload
      puts "finding experiment file to load"
      if $robots_loaded[myindex]==true then
        puts "  >skip this one"
        next
      end
      myfile =""
      if Dir.exists?($FILESOURCE) == false then
        puts "FILESOURCE directory does not exist"
        return
      end
      # DETERMINE WHAT FILE TO UPLOAD
      puts "Searching in "+$FILESOURCE
      pattern = $FILESOURCE+"*.csv"
      Dir.glob(pattern) do |file| 
        myfile = file
        if @already_started_hash[myfile] != nil then
          next
        end
        @already_started_hash[myfile] = myfile
        puts " found a file"
        break
      end
      sourceFileName =myfile
      puts "  checking file name "+myfile
      if myfile == "" then
        puts "did not file a file to upload"
        return
      end
      filename_from = myfile 
      ipaddr = $ip_addresses[myindex]  
      puts "loading "+filename_from + " into " + ipaddr
      @filesThatWereLoaded << filename_from
      if loadExperimentFile(filename_from, ipaddr, $authorization_tags[myindex]) then
       # MOVE UPLOAD FILE TO $FILERUNNING 
        movedFileName = $FILERUNNING + File.basename(myfile)
        puts "FAKE moved file name = "+movedFileName
        copyFile(sourceFileName,movedFileName)    
        $robots_loaded[myindex]=true
      end     
    end 
  end 
end 

# Report on all files that were uploaded
def reportLoadedFiles
  puts "executing start file report..."
  @filesThatWereLoaded.each do |filename|
     puts "  #{filename}"
  end
end

# report all files that were downloaded
def reportFinisehdFiles
  puts "executing finished file report..."
  @listOfDownloadedFiles.each do |filename|
     puts "  #{filename}"
  end  
end

# report all servers that were shut down
def reportShutDownServers
  puts "executing shut down server report..."
  @listOfShutdownServers.each do |servername| 
    puts "  #{servername}"
  end
end

# report all servers that were started
def reportStartingServers
  puts "executing start server report..."
  @listOfStartedServers.each do |servername|
    puts "  #{servername}"
  end
end


puts "loading configuration"
loadConfiguration()

puts "removing extra files"
removeFilesFromSourceThatExistInRuns()
removeFilesFromRunningThatExistInFinished()

puts "starting execution"
getStatusStartingValues()

#Deprecated 11/29/2014 BP
#puts "getting authenticity tokens"
#getAuthorizationTokens()

puts "ready to load"
checkReadyToLoad()

puts "reporting status values"
reportStatusValues()

puts "preparing finished servers"
prepareFinishedServers() 

puts "download files "
downloadFilesFromFinishedServersAndRename()


puts "--load experiments--"
loadExperimentsIntoServers()

puts "shutting down servers"
shutdownFinishedServers()

reportLoadedFiles()
reportFinisehdFiles()
reportShutDownServers()
reportStartingServers()

seconds = getTimeFromStart()
minutes = seconds / 60

puts "run time report ..."
puts "total seconds run time = " + seconds.to_s
puts "total minutes run time = " + minutes.to_s

puts "!!!!!!!!!!  finished with checkSystemStatus.rb  !!!!!!!!"

