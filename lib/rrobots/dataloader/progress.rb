require 'rubygems'
require 'nokogiri'
require 'open-uri'

# Use this script to count the number of RRobots experiments that have results.
# Divide the number of rows with results by the total number of rows.  Output that value.
# A value of 100 indicates a complete run
# If the web page cannot be downloaded, or has no rows, then it returns -1.

def checkForFinish()

  if ARGV.length == 0 then
    puts "error - no URL"
    exit!
  end
  url = ARGV[0]

  page = Nokogiri::HTML(open(url))
  rows = page.css("tr")
  puts rows

  goodrows = 0

  for index in 1 ... rows.size
    myrow =rows[index]
    children = myrow.css("td")
    matchcount = children[1].text
    content_left = children[3].text.strip
    content_right = children[4].text.strip
    if content_left.index(")") >0 then
      if content_right.index(")")>0 then
      goodrows = goodrows +1
      end
    end
  end

  v = goodrows.to_f / ( rows.size - 1 ).to_f
  v=v*100
  return v
rescue
  return -1
end

done = checkForFinish();
puts done.to_s
