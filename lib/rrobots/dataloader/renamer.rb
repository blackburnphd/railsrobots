


# create the prefix from this filename
def namerepair(n)
  if n == nil then
   return
  end
  l = n.length
  newstring = "HB_"
  newstring = newstring + n[4,l]
  return newstring
end


# Inspect the file and change its name
# use the rename tag to put a special run id on the name
def renameFile(theFile, renameTag)
      if renameTag == nil then
        puts 'rename tag not found'
        return
      end     
      if theFile == nil then
        puts "the file not found "+theFile
        return
      end 
      puts "renaming "+theFile
      puts "renameTag "+renameTag
  
      if File.exists?(theFile) == false then
        puts "the file "+theFile+" does not exist"
        return;
      end
      scores = Hash.new
      
      f = File.open(theFile)
      f.each do |line|
        if line.start_with?('experiment')
          next
        end
        puts 'reading file'
        line = line.strip
        entries = line.split(',')
        tank1 = entries[1]
        tank2 = entries[58]
        tank1 = namerepair tank1
        tank2 = namerepair tank2
        tank1 = tank1.gsub!(' ','_')
        tank2 = tank2.gsub!(' ','_')
        s1 = scores[tank1]
        if s1 == nil then
          s1 =0
        end
        s2 =scores[tank2]
        if s2==nil then
          s2 =0
        end
        
        s1 =s1 +1
        s2 = s2+1
        scores[tank1]=s1
        scores[tank2]=s2
      end
      
      f.close
      bestscore = 0
      besthash = nil
      scores.keys.each do |key|
        val = scores[key]
        if val > bestscore then
          bestscore = val
          besthash = key
        end
      end
      # figure out how many matches we have
      matches =0
      scores.keys.each do |key|
        val = scores[key]
        if val == bestscore then
          matches = matches +1
        end
      end
      besthash = "export_"+renameTag+"_"+besthash
      if matches >1 then
        besthash = besthash+"x"+matches.to_s
      end
      parent=File.dirname(theFile)
      newfilename = parent + "/"+ besthash +".CSV"
      puts "[RENAMER] new file name = "+newfilename
      File.rename(theFile,newfilename)
      return newfilename
end
    

if ARGV.empty? then
  puts "no arguements found"
else
  renameFile(ARGV[0],ARGV[1])
end


