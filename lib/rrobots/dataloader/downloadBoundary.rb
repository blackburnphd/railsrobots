require "net/http"
require "uri"




server = "192.168.20.29"
#server = "localhost"

url = URI.parse('http://'+server+':3000/experiments_export/export')
puts "server name = " + url.to_s


# Token used to terminate the file in the post body. Make sure it is not
# present in the file you're uploading.
BOUNDARY = "AaB03x"
b=BOUNDARY
bbb="-----------------------------4925106532041"
fields = []
 
  fields <<  "avg_body_rotation"
  fields <<  "avg_damage_given"       
  fields <<  "avg_distance_traveled"  
  fields <<  "avg_draws"              
  fields <<  "avg_gun_rotation"       
  fields <<  "avg_kills"              
  fields <<  "avg_losses"             
  fields <<  "avg_radar_rotation"      
  fields <<  "avg_remaining_energy"    
  fields <<  "avg_resupply" 
  fields <<  "avg_shots_fired" 
  fields <<  "avg_survival_time" 
  fields <<  "avg_times_hit" 
  fields <<  "avg_wins" 
  fields <<  "max_body_rotation" 
  fields <<  "max_damage_given" 
  fields <<  "max_distance_traveled" 
  fields <<  "max_draws" 
  fields <<  "max_gun_rotation" 
  fields <<  "max_kills" 
  fields <<  "max_losses" 
  fields <<  "max_radar_rotation" 
  fields <<  "max_remaining_energy" 
  fields <<  "max_resupply" 
  fields <<  "max_shots_fired" 
  fields <<  "max_survival_time" 
  fields <<  "max_times_hit" 
  fields <<  "max_wins" 
  fields <<  "min_body_rotation" 
  fields <<  "min_damage_given" 
  fields <<  "min_distance_traveled" 
  fields <<  "min_draws" 
  fields <<  "min_gun_rotation" 
  fields <<  "min_kills" 
  fields <<  "min_losses" 
  fields <<  "min_radar_rotation" 
  fields <<  "min_remaining_energy" 
  fields <<  "min_resupply" 
  fields <<  "min_shots_fired" 
  fields <<  "min_survival_time" 
  fields <<  "min_times_hit" 
  fields <<  "min_wins" 
  fields <<  "tot_body_rotation" 
  fields <<  "tot_damage_given" 
  fields <<  "tot_distance_traveled" 
  fields <<  "tot_draws" 
  fields << 	"tot_gun_rotation" 
  fields << 	"tot_kills" 
  fields << 	"tot_losses" 
  fields << 	"tot_radar_rotation" 
  fields << 	"tot_remaining_energy" 
  fields << 	"tot_resupply" 
  fields << 	"tot_shots_fired" 
  fields << 	"tot_survival_time" 
  fields << 	"tot_times_hit" 
  fields << 	"tot_wins" 

CRLF = "\u000A\u000D"
generic = "Content-Disposition: form-data; name=\"export_fields[]\""+CRLF


post_body = []
post_body << "--#{b}"+CRLF
post_body << "Content-Disposition: form-data; name=\"utf8\""+CRLF
post_body << "true"+CRLF
	 

fields.each{|x| 
	post_body << "--#{b}"+CRLF

	post_body << generic
	post_body << "Content-Type: text/plain"+CRLF 
	post_body << CRLF
	post_body << x+CRLF
	
}
post_body << "--#{b}"+CRLF

post_body << "Content-Disposition: form-data; name=\"commit\""+CRLF
post_body << "Content-Type: text/plain"+CRLF
post_body << "export"+CRLF

post_body << "--#{b}"+CRLF
puts post_body.to_s



http = Net::HTTP.new(url.host, url.port)
request = Net::HTTP::Post.new(url.request_uri)
request.body = post_body.join
request["Content-Type"] = "multipart/form-data, boundary=#{b}"
puts request["Content-Type"]
http.request(request)








