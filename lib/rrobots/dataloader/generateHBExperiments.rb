# Create experiment files for Adapting HealBots

# 100           15%       3000         5%      Adapting2_t100_e0p15_t3000_e0p05_HB
# 500           15%       3000         5%      Adapting2_t500_e0p15_t3000_e0p05_HB
# 1000          15%       3000         5%      Adapting2_t1000_e0p15_t3000_e0p05_HB
# 1500          15%       3000         5%      Adapting2_t1500_e0p15_t3000_e0p05_HB
# 2000          15%       3000         5%      Adapting2_t2000_e0p15_t3000_e0p05_HB
# 2500          15%       3000         5%      Adapting2_t2500_e0p15_t3000_e0p05_HB
# 100           15%       1000         5%      Adapting2_t100_e0p15_t1000_e0p05_HB
# 500           15%       1000         5%      Adapting2_t500_e0p15_t1000_e0p05_HB
# 100           15%       2000         5%      Adapting2_t100_e0p15_t2000_e0p05_HB
# 500           15%       2000         5%      Adapting2_500_e0p15_t2000_e0p05_HB
# 1000          15%       2000         5%      Adapting2_t1000_e0p15_t2000_e0p05_HB
# 1500          15%       2000         5%      Adapting2_t1500_e0p15_t2000_e0p05_HB


$adaptingBots = ["Adapting2_t100_e0p15_t3000_e0p05_HB",
  "Adapting2_t500_e0p15_t3000_e0p05_HB",
  "Adapting2_t1000_e0p15_t3000_e0p05_HB",
  "Adapting2_t1500_e0p15_t3000_e0p05_HB",
  "Adapting2_t2000_e0p15_t3000_e0p05_HB",
  "Adapting2_t2500_e0p15_t3000_e0p05_HB",
  "Adapting2_t100_e0p15_t1000_e0p05_HB",
  "Adapting2_t500_e0p15_t1000_e0p05_HB",
  "Adapting2_t100_e0p15_t2000_e0p05_HB",
  "Adapting2_500_e0p15_t2000_e0p05_HB",
  "Adapting2_t1000_e0p15_t2000_e0p05_HB",
  "Adapting2_t1500_e0p15_t2000_e0p05_HB"
  ]
  


$outputdir = "C:/Users/Brian/Documents/Aptana Studio 3 Workspace/railsrobots/sourceFiles"
$heloName = "Helo25"
$combatantHBs = Array.new

def findCombatants
  Dir.glob("../robots/HB_*.rb").each do |file|
    puts File.basename(file)
    $combatantHBs << File.basename(file )
  end
end

def generateExperimentFile(forAdaptingAgent)
  # create header
  filename = $outputdir+"/input_"+forAdaptingAgent+".csv"
  if File.exists? filename then
    File.delete filename
  end
  
  line ="Experiment Name,Trial Count,Helicopter,"+forAdaptingAgent+".rb"
  $combatantHBs.each do |r|
    line = line + "," + r
  end
  
  
  # write header line
  open(filename, 'w') do |f|
    f.puts(line)
    puts line
    
    # now the meat
    ct = 0
    $combatantHBs.each do |r|
      line = forAdaptingAgent+"_"+ct.to_s+ ",100,Helo25,X"
      $combatantHBs.each_with_index do |item, index|
        line = line +","
        if ct == index then
          line = line+"X"
        end
      end
      f.puts line
      ct = ct +1
    end
    
  end
   
  
end



def createExperiments
  $adaptingBots.each do |item|
    generateExperimentFile(item)
  end 
  
end


findCombatants()
createExperiments()
