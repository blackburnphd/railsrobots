require_relative './robots/behaviors/OrderOfBattle'
require_relative './robots/StaticBasis'

class RobotRunner

  STATE_IVARS = [ :x, :y, :gun_heat, :heading, :gun_heading, :radar_heading, :time, :survival_time, :size, :speed, :energy, :team, :times_hit, :distance_traveled, :shots_fired, :body_rotation, :gun_rotation, :radar_rotation,:lastShotPower,:resupply ]
  NUMERIC_ACTIONS = [ :fire, :turn, :turn_gun, :turn_radar, :accelerate ]
  STRING_ACTIONS = [ :say, :broadcast ]

  STATE_IVARS.each{|iv|
    attr_accessor iv
  }
  NUMERIC_ACTIONS.each{|iv|
    attr_accessor "#{iv}_min", "#{iv}_max"
  }
  STRING_ACTIONS.each{|iv|
    attr_accessor "#{iv}_max"
  }

  #AI of this robot
  attr_accessor :robot

  #team of this robot
  attr_accessor :team

  #keeps track of total damage done by this robot
  attr_accessor :damage_given

  #keeps track of the kills
  attr_accessor :kills

  attr_reader :actions, :speech


  attr_reader :cargoHitObserved
  attr_reader :heloHitObserved
  


  def initialize robot, bf, team=0
    @robot = robot
    @battlefield = bf
    @team = team
    set_action_limits
    set_initial_state
    @events = Hash.new{|h, k| h[k]=[]}
    @actions = Hash.new(0)
    @resupply_x = -999
    @resupply_y = -999
    @resupply_name=nil
    if robot.is_a? StaticBasis then
      puts "StaticBasis has arrived!"
    end
    @cargoHitObserved = "cargoHitObserved"
    @heloHitObserved = "heloHitObserved"
  end

  def skin_prefix
    @robot.skin_prefix
  end

  def set_initial_state
    @x = @battlefield.width / 2
    @y = @battlefield.height / 2
    @speech_counter = -1
    @speech = nil
    @time = 0
    @size = 60
    @speed = 0
    @energy = 100
    @damage_given = 0
    @kills = 0
    
    # AP 2012-11-28 - Added
    @survival_time = 0
    
    # AP 2013-03-13 - Added
    @times_hit = 0
    @distance_traveled = 0
    @shots_fired = 0
    @body_rotation = 0
    @gun_rotation = 0
    @radar_rotation = 0
    

    @resupply = rand(10)
    
    teleport
  end

  def teleport(distance_x=@battlefield.width / 2, distance_y=@battlefield.height / 2)
    @x += ((rand-0.5) * 2 * distance_x).to_i
    @y += ((rand-0.5) * 2 * distance_y).to_i
    @gun_heat = 3
    @heading = (rand * 360).to_i
    @gun_heading = @heading
    @radar_heading = @heading
  end

  def set_action_limits
    @fire_min, @fire_max = 0, 3
    @turn_min, @turn_max = -10, 10
    @turn_gun_min, @turn_gun_max = -30, 30
    @turn_radar_min, @turn_radar_max = -60, 60
    @accelerate_min, @accelerate_max = -1, 1
    @teleport_min, @teleport_max = 0, 100
    @say_max = 256
    @broadcast_max = 16
  end

  def hit bullet
    damage = bullet.energy
    @energy -= damage
    @events['got_hit'] << [@energy]
    
    # AP 2013-03-13 - New metrics
    @times_hit += 1
    
    damage
  end

  def dead
    @energy < 0
  end

  def clamp(var, min, max)
    val = 0 + var # to guard against poisoned vars
    if val > max
      max
    elsif val < min
      min
    else
      val
    end
  end

  def internal_tick
    update_state
    robot_tick
    parse_actions
    fire
    turn
    move
    scan
    speak
    broadcast
    @time += 1
    
    if @energy >= 0
      @survival_time += 1
    end
  end

  def parse_actions
    @actions.clear
    NUMERIC_ACTIONS.each{|an|
      @actions[an] = clamp(@robot.actions[an], send("#{an}_min"), send("#{an}_max"))
    }
    STRING_ACTIONS.each{|an|
      if @robot.actions[an] != 0
        @actions[an] = String(@robot.actions[an])[0, send("#{an}_max")]
      end
    }
    @actions
  end

  def state
    current_state = {}
    STATE_IVARS.each{|iv|
      current_state[iv] = send(iv)
    }
    current_state[:battlefield_width] = @battlefield.width
    current_state[:battlefield_height] = @battlefield.height
    current_state[:game_over] = @battlefield.game_over
    current_state
  end

  def update_state
    new_state = state
    @robot.state = new_state
    new_state.each{|k,v|
      @robot.send("#{k}=", v)
    }
    
    
    @robot.events = @events.dup
    @robot.actions ||= Hash.new(0)
    @robot.actions.clear
  end

  def robot_tick
    # if @robot.kind_of?(StaticBasis) then
      # if @robot.respond_to?(:doesResupplyExist) then
        # if @robot.doesResupplyExist then
          # puts "robot has the points"
        # end
      # end
    # end
    
    @robot.tick @robot.events
    @events.clear



    
    
  end


  


  def fire
    if (@actions[:fire] > 0) && (@gun_heat == 0)
      bullet = Bullet.new(@battlefield, @x, @y, @gun_heading, 30, @actions[:fire]*3.0, self)
      3.times{bullet.tick}
      @battlefield << bullet
      @gun_heat = @actions[:fire]


     # added a last shot report for record purposes
     @lastShotPower=@actions[:fire]

      
      # AP 2013-03-13 - New metrics
      @shots_fired += 1
      # BP 2013-10-06 - Make sure to capture existing shots
      
      if @shots_fired < @robot.shots_fired then
        @shots_fired = @robot.shots_fired
      end

    end
    @gun_heat -= 0.1
    @gun_heat = 0 if @gun_heat < 0
  end

  def turn
    
    # AP 2013-03-13 - New metrics
    @body_rotation += @actions[:turn].abs()
    @gun_rotation += @actions[:turn_gun].abs()
    @radar_rotation += @actions[:turn_radar].abs()
    
    @old_radar_heading = @radar_heading
    @heading += @actions[:turn]
    @gun_heading += (@actions[:turn] + @actions[:turn_gun])
    @radar_heading += (@actions[:turn] + @actions[:turn_gun] + @actions[:turn_radar])
    @new_radar_heading = @radar_heading

    @heading %= 360
    @gun_heading %= 360
    @radar_heading %= 360
  end

  def move
    @speed += @actions[:accelerate]
    @speed = 8 if @speed > 8
    @speed = -8 if @speed < -8

    # AP 2013-03-13 - New metrics
    old_x = @x
    old_y = @y

    @x += Math::cos(@heading.to_rad) * @speed
    @y -= Math::sin(@heading.to_rad) * @speed

    @x = @size if @x - @size < 0
    @y = @size if @y - @size < 0
    @x = @battlefield.width - @size if @x + @size >= @battlefield.width
    @y = @battlefield.height - @size if @y + @size >= @battlefield.height
    
    # AP 2013-03-13 - New metrics
    @distance_traveled += Math.hypot(@y - old_y, old_x - @x)
    
      # determine which robots are in the match
      # modified the code to provide this work-around
      # until a classifier is built.
      
      OrderOfBattle.clear()
    @battlefield.robots.each do |other|
     
       
       OrderOfBattle.add(other.robot.class.name)  
      
    end
     
  end

  def scan
    @battlefield.robots.each do |other|
      if (other != self) && (!other.dead)
        a = Math.atan2(@y - other.y, other.x - @x) / Math::PI * 180 % 360
        if (@old_radar_heading <= a && a <= @new_radar_heading) || (@old_radar_heading >= a && a >= @new_radar_heading) ||
          (@old_radar_heading <= a+360 && a+360 <= @new_radar_heading) || (@old_radar_heading >= a+360 && a+360 >= new_radar_heading) ||
          (@old_radar_heading <= a-360 && a-360 <= @new_radar_heading) || (@old_radar_heading >= a-360 && a-360 >= @new_radar_heading)
           @events['robot_scanned'] << [Math.hypot(@y - other.y, other.x - @x)]
        end
      end
    end
  end

  def speak
    if @actions[:say] != 0
      @speech = @actions[:say]
      @speech_counter = 50
    elsif @speech and (@speech_counter -= 1) < 0
      @speech = nil
    end
  end

  def broadcast
    @battlefield.robots.each do |other|
      if (other != self) && (!other.dead)
        msg = other.actions[:broadcast]
        if msg != 0
          a = Math.atan2(@y - other.y, other.x - @x) / Math::PI * 180 % 360
          dir = 'east'
          dir = 'north' if a.between? 45,135
          dir = 'west' if a.between? 135,225
          dir = 'south' if a.between? 225,315
          @events['broadcasts'] << [msg, dir]
        end
      end
    end
  end

  def to_s
    @robot.class.name
  end

  def name
    @robot.class.name
  end
  
  def getRobotClassName
    return @robot.class.name
  end


  def notifyHitOnSupply(robot_source_of_hit)
    
    if(robot_source_of_hit == @robot) then
      return
    end
      
    if @robot.respond_to?(:notifyHitCargoFunction) then
      @robot.cargoHitObserved
      
    end
    
    if @robot.respond_to?(:notifyHitHeloFunction) then
      @robot.heloHitObserved
    end
    
  end
  




  def getLastShot
   # puts "last shot ="+@robot.lastShotPower.to_s
    return @lastShotPower
  end

  # this is called by battlefield when the robot hits the resupply cargo  
  def resupplyAction(resupply_energy)
    if @robot.respond_to?(:resupplyEnergyAward)
        @robot.resupplyEnergyAward(resupply_energy)
        puts "Robot energy is now "+@robot.energy.to_s
        @energy = @robot.energy
    end          
  end
  
  

# called by the battlefield when the resupply cargo arrives
  def resupplyArrives(myresupplyX,myresupplyY,myresupplyName)
     @resupply_x = myresupplyX
     @resupply_y = myresupplyY
     @resupply_name = myresupplyName
     puts "resupply arrives x="+@resupply_x.to_s+" y="+@resupply_y.to_s+" name="+@resupply_name
    
     if @robot.respond_to?(:resupplyArrives) then
       puts "resupplied a static basis"
       @robot.resupplyArrives(myresupplyX, myresupplyY, myresupplyName)
     end
  end

  def clearResupply
      if @robot.respond_to?(:clearResupplyData) then
        @robot.clearResupplyData
        @robot.resupplyY = -999
        @robot.resupplyX = -999
        
        
      end
  end
  
  # if this tank can process resupplyIsDestroyed messages, then send it along.
  def resupplyIsDestroyed(cargoName)
    
    if @robot.respond_to?(:resupplyIsDestroyed) then
      @robot.resupplyIsDestroyed(cargoName);
    end
  end
  
  # if the robot supports updateHeloInformation(hx,hy,isonscr), 
  # pass the message along
  def updateHeloInformation(hx,hy,isonscr)
    if @robot.respond_to?(:updateHeloInformation)
      robot.updateHeloInformation(hx,hy,isonscr)
    end
  end
  
  
end