require_relative 'robot'
require_relative 'robot_runner'

# AP 2012-11-08 - Added these
require_relative 'surrogates/surrogate_server'
require_relative 'robust_robot'



class Numeric
   TO_RAD = Math::PI / 180.0
   TO_DEG = 180.0 / Math::PI
   def to_rad
     self * TO_RAD
   end
   def to_deg
     self * TO_DEG
   end
end

require_relative 'battlefield'

require_relative 'explosion'

require_relative 'bullet'


##############################################
# arena
##############################################

def usage
  puts "usage: rrobots.rb [resolution] [#match] [-nogui] [-speed=<N>] [-timeout=<N>] [-teams=<N>] [-helo=<heloname>]<FirstRobotClassName[.rb]> <SecondRobotClassName[.rb]> <...>"
  puts "\t[resolution] (optional) should be of the form 640x480 or 800*600. default is 800x800"
  puts "\t[match] (optional) to replay a match, put the match# here, including the #sign.  "
  puts "\t[-nogui] (optional) run the match without the gui, for highest possible speed.(ignores speed value if present)"
  puts "\t[-speed=<N>] (optional, defaults to 1) updates GUI after every N ticks.  The higher the N, the faster the match will play."
  puts "\t[-timeout=<N>] (optional, default 50000) number of ticks a match will last at most."
  puts "\t[-teams=<N>] (optional) split robots into N teams. Match ends when only one team has robots left."
  puts "\tthe names of the rb files have to match the class names of the robots"
  puts "\t(up to 8 robots)"
  puts "\te.g. 'ruby rrobots.rb SittingDuck NervousDuck'"
  puts "\t or 'ruby rrobots.rb 600x600 #1234567890 SittingDuck NervousDuck'"
  exit
end

def run_out_of_gui(battlefield)
  $stderr.puts 'match ends if only 1 bot/team left or dots get here-->|'

  until battlefield.game_over
    battlefield.tick
    $stderr.print "." if battlefield.time % (battlefield.timeout / 54).to_i == 0
  end
  print_outcome(battlefield)
  exit 0
end

def run_in_gui(battlefield, xres, yres, speed_multiplier)
  require_relative 'tkarena'
  arena = TkArena.new(battlefield, xres, yres, speed_multiplier)
  game_over_counter = battlefield.teams.all?{|k,t| t.size < 2} ? 250 : 500
  outcome_printed = false
  
  # AP 2012-10-30 Renamed this battlefield variable to remove a shadowing warning from Aptana Studio
  arena.on_game_over{|game_battlefield|
    unless outcome_printed
      print_outcome(game_battlefield)
      outcome_printed = true
    end
    exit 0 if game_over_counter < 0
    game_over_counter -= 1
  }
  arena.run
end

def print_outcome(battlefield)
  winners = battlefield.robots.find_all{|robot| !robot.dead}
  puts
  if battlefield.robots.size > battlefield.teams.size
    teams = battlefield.teams.find_all{|name,team| !team.all?{|robot| robot.dead} }
    puts "winner_is:     { #{
      teams.map do |name,team|
        "Team #{name}: [#{team.join(', ')}]"
      end.join(', ')
    } }"
    puts "winner_energy: { #{
      teams.map do |name,team|
        "Team #{name}: [#{team.map do |w| ('%.1f' % w.energy) end.join(', ')}]"
      end.join(', ')
    } }"
  else
    puts "winner_is:     [#{winners.map{|w|w.name}.join(', ')}]"
    puts "winner_energy: [#{winners.map{|w|'%.1f' % w.energy}.join(', ')}]"
  end
  puts "elapsed_ticks: #{battlefield.time}"
  puts "seed :         #{battlefield.seed}"
  puts
  puts "robots :"
  battlefield.robots.each do |robot|
    puts "  #{robot.name}:"
    puts "    damage_given: #{'%.1f' % robot.damage_given}"
    puts "    damage_taken: #{'%.1f' % (100 - robot.energy)}"
    puts "    kills:        #{robot.kills}"
  end
  
  # AP 2012-11-17 - Write experiment data to the rails server.
  # AP 2013-03-07 - Changing the way this works.  We'll spit out some YAML and let the calling code
  #                 write the data to the database.  Now we don't need ActiveResource anymore.
  if battlefield.experiment_id != 0
    # AP 2013-02-23 - We're tracking draws now, so we have to keep track of how many non-dead robots are left.
    have_a_winner = false
    have_a_draw = false
    battlefield.robots.each do |robot|
      if !robot.dead
        if have_a_winner
          # We already have a winner, that means this is the second "winner", so it's actually a draw
          have_a_draw = true
          break
        else
          # First non-dead robot, so this could be the winner.
          have_a_winner = true
        end
      end
    end
    
    puts "# Begin RailsRobots Output"
    puts "experiment_id : #{battlefield.experiment_id}"
    puts "duration : #{battlefield.time}"
    
    puts "robots :"
    battlefield.robots.each do |robot|
      # TODO: Eventually, just using the name and adding ".rb" won't be enough.  Need to add a file_name property.
      #       Check the compositional robots branch to see how I dealt with this over there...
      puts "  #{robot.name}.rb:"
      puts "    survival_time : #{robot.survival_time}"
      puts "    remaining_energy : #{robot.energy}"
      puts "    kills : #{robot.kills}"
      puts "    damage_given : #{robot.damage_given}"
      
      # AP 2013-03-13 - Add new metrics to the output string.
      puts "    times_hit : #{robot.times_hit}"
      puts "    distance_traveled : #{robot.distance_traveled}"
      puts "    shots_fired : #{robot.shots_fired}"
      puts "    body_rotation : #{robot.body_rotation}"
      puts "    gun_rotation : #{robot.gun_rotation}"
      puts "    radar_rotation : #{robot.radar_rotation}"
      
      # BP 2014-02-01 - Add resupply metrics to system
      puts "    resupply : #{robot.resupply}"
      
      if robot.dead
        # Robot is dead, it is a loss.
        puts "    wins : 0"
        puts "    losses : 1"
        puts "    draws : 0"
       else
        if have_a_draw
          # Draw
          puts "    wins : 0"
          puts "    draws : 1"
        else
          # Win
          puts "    wins : 1"
          puts "    draws : 0"
        end
        puts "    losses : 0"
      end
    end    
    
    puts "# End RailsRobots Output"
  end
  # End AP - Write experiment data to the rails server.
end

$stdout.sync = true

# AP 2011-11-17 - Adding new argument to specify the Experiment id.  If coming from RailsRobots, that tells us
#  that there is a rails server running (for now we're going to assume it's on localhost:3000), and that we
#  should log the results of this match to that server using the specified experiment id.
experiment_id = 0
ARGV.grep(/^-experiment=*(\d+)/) do |item|
  experiment_id = $1.to_i()
  ARGV.delete(item)
end
# End AP 2011-11-17
puts "here we go"


# Look for Helicopter class
@helo_class=nil
ARGV.grep(/-helo=/ )do |item|
puts "helo!!!!!!!!! "+item.to_str
  myheloclass = item.to_str.slice(6,item.to_str.length)
  @helo_class = myheloclass
  ARGV.delete(item)
  
  puts "using helo class "+@helo_class
  
end

puts "again"

# look for resolution arg
xres, yres = 800, 800
ARGV.grep(/^(\d+)[x\*](\d+$)/) do |item|
  xres, yres = $1.to_i, $2.to_i
  ARGV.delete(item)
end

# look for match arg
seed = Time.now.to_i + Process.pid
ARGV.grep(/^#(\d+)/) do |item|
  seed = $1.to_i
  ARGV.delete(item)
end

#look for mode arg
mode = :run_in_gui
ARGV.grep( /^(-nogui)/ )do |item|
  mode = :run_out_of_gui
  ARGV.delete(item)
end

#look for speed multiplier arg
speed_multiplier = 1
ARGV.grep( /^-speed=(\d\d?)/ )do |item|
  x = $1.to_i
  speed_multiplier = x if x > 0 && x < 100
  ARGV.delete(item)
end

#look for timeout arg
timeout = 50000
ARGV.grep( /^-timeout=(\d+)/ )do |item|
  timeout = $1.to_i
  ARGV.delete(item)
end

#look for teams arg
team_count = 8
ARGV.grep( /^-teams=(\d)/ )do |item|
  x = $1.to_i
  team_count = x if x > 0 && x < 8
  ARGV.delete(item)
end
teams = Array.new([team_count, ARGV.size].min){ [] }





usage if ARGV.size > 9 || ARGV.empty?

battlefield = Battlefield.new xres*2, yres*2, timeout, seed, experiment_id
puts "NEW BATTLEFIELD" 


# TODO: Put this back when we start using surrogates.
# AP 2012-11-08 - We're about to start loading robots.  Start the surrogate server.
#surrogate_server = SurrogateServer.new(50000)
#surrogate_server.debug = false

#surrogate_server.run_server()

c = 0
team_divider = (ARGV.size / teams.size.to_f).ceil
ARGV.map! do |robot|
  begin
    begin
      require_relative robot.downcase
    rescue LoadError
    end
    begin
      require_relative robot
    rescue LoadError
    end
    
    # AP 2012-11-23 - Added a few extra cases now that I moved the robots into a subfolder
    begin
      require_relative 'robots/' + robot.downcase
    rescue LoadError
    end
    begin
      require_relative 'robots/' + robot
    rescue LoadError
  end
  in_game_name = File.basename(robot).sub(/\..*$/, '')
  in_game_name[0] = in_game_name[0,1].upcase
  team = c / team_divider
  c += 1
  
  # AP 2012-11-08 - Had to modify this code so that we get the robot as a variable first.  I might need to
  #  attach the surrogate server information to it.
  new_robot = Object.const_get(in_game_name).new
  if new_robot.is_a? RobustRobot
    puts "One of my robots is a robust robot"
    # Only attach surrogate information if this robot defines a surrogate type.
    if new_robot.surrogate_type != nil && !new_robot.surrogate_type.empty?
      new_robot.surrogate_server_host = 'localhost'
      new_robot.surrogate_server_port = surrogate_server.port
      new_robot.request_surrogate()
    end
  end
  # End AP Changes
  
  robotrunner = RobotRunner.new(new_robot, battlefield, team)
  battlefield << robotrunner
  rescue Exception => error
    puts 'Error loading ' + robot + '!'
    puts error
    warn error
    usage
  end
  in_game_name
end


puts "********* PREPARE BATTLEFIELD *********"


battlefield.setHeloName @helo_class
battlefield.helo.resX = xres
battlefield.helo.resY = yres


if mode == :run_out_of_gui
  run_out_of_gui(battlefield)
else
  run_in_gui(battlefield, xres, yres, speed_multiplier)
end

puts "******  FINALIZE BATTLEFIELD **********"

# TODO: Put this back when we start using surrogates.
# AP 2012-11-08 - Match is over, stop the surrogate server.
#surrogate_server.halt_server()
