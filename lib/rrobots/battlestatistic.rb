class Battlestatistic
  
  attr_accessor :maxtime
  attr_accessor :mintime
  attr_accessor :tankClass
  attr_accessor :avgX
  attr_accessor :stdevX
  attr_accessor :avgY
  attr_accessor :stdevY
  attr_accessor :distToWall
  attr_accessor :distTraveled
  attr_accessor :avgTurretRotation
  attr_accessor :stdevTurretRotation
  attr_accessor :shots
  attr_accessor :lastShotPower
  attr_accessor :suspectedAimingStrategy
  
  def initialize
    @maxtime=0
    @mintime = 99999999
    @tankClass = "unknown"
    @avgX = 0
    @avgY = 0
    @stdevX = 0
    @stdevY = 0 
    @distToWall= 0
    @distTraveled=99999999.0
    @shots=0
    @avgTurretRotation=0
    @stdevTurretRotation=0
    @shots=0
    @lastShotPower=0
    @suspectedAimingStrategy=(-1)
  end
  
  def heading
    r2="Tank Type\t Min Time\tMax Time\tAvgX\tavgY\tStDevX\tStdDevY\tAvgDistToWall\tDistTravelled"
    r2=r2 + "\tAvgTurretRotation\tStdevTurretRotation\tShots\tLastShotPower\tSuspectedAimingStrategy"
    return r2
  end
  
  def report
   
    r = @tankClass + "\t"  + @mintime.to_s+"\t" + @maxtime.to_s 
    r= r + "\t"+ @avgX.to_s + "\t" + @avgY.to_s+ "\t"+@stdevX.to_s+"\t"+@stdevY.to_s
    r = r+ "\t" + @distToWall.to_s
    r = r+ "\t" + @distTraveled.to_s
    r=r+"\t"+@avgTurretRotation.to_s
    r=r+"\t"+@stdevTurretRotation.to_s
    r=r+"\t"+@shots.to_s
    r=r+"\t"+@lastShotPower.to_s
    r=r+"\t"+@suspectedAimingStrategy.to_s
    
    return r
  end
  
end