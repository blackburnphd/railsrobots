
require_relative '../robot'

class Static_v4_1
   include Robot
   @shotLast = 0

  def tick events
         
    accelerate  1
    if (@shotLast.to_i > 0) and (@shotLast.to_i < 5)
        turn -3
        turn_gun 15/2
        fire 1.0e-001
        say "One after scan"
        @shotlast =+ 1
    elsif events['robot_scanned'].empty?
      fire 1.0e-001
      turn 0
      turn_gun -15
      accelerate  1*3
      @shotLast = 1
      say "Scanned"
    else
        turn_gun 15
        turn 5
        fire 0
        @shotLast = 0
        say "Looking"
    end
    turn_radar 0
     
     
     
     
  end
end
