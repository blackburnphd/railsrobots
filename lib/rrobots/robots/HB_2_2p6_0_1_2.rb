require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_2_2p6_0_1_2  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 2
    @fire_power = 2.6
    @aiming_strategy = 0
    @movement_strategy = 1
    @resupply_strategy = 2
  end
end
