require_relative '../robust_robot'
require_relative './behaviors/OrderOfBattle'
require_relative './behaviors/behaviorBase'
require_relative './behaviors/ProbabilityLoader'
require_relative './behaviors/Agent_0_0p1_0_0'
require_relative './behaviors/Agent_0_0p1_0_1'
require_relative './behaviors/Agent_0_0p1_0_2'
require_relative './behaviors/Agent_0_0p1_0_3'
require_relative './behaviors/Agent_0_0p1_0_4'
require_relative './behaviors/Agent_0_0p1_1_0'
require_relative './behaviors/Agent_0_0p1_1_1'
require_relative './behaviors/Agent_0_0p1_1_2'
require_relative './behaviors/Agent_0_0p1_1_3'
require_relative './behaviors/Agent_0_0p1_1_4'
require_relative './behaviors/Agent_1_0p1_0_0'
require_relative './behaviors/Agent_1_0p1_0_1'
require_relative './behaviors/Agent_1_0p1_0_2'
require_relative './behaviors/Agent_1_0p1_0_3'
require_relative './behaviors/Agent_1_0p1_0_4'
require_relative './behaviors/Agent_1_0p1_1_0'
require_relative './behaviors/Agent_1_0p1_1_1'
require_relative './behaviors/Agent_1_0p1_1_2'
require_relative './behaviors/Agent_1_0p1_1_3'
require_relative './behaviors/Agent_1_0p1_1_4'
require_relative './behaviors/Agent_2_0p1_0_0'
require_relative './behaviors/Agent_2_0p1_0_1'
require_relative './behaviors/Agent_2_0p1_0_2'
require_relative './behaviors/Agent_2_0p1_0_3'
require_relative './behaviors/Agent_2_0p1_0_4'
require_relative './behaviors/Agent_2_0p1_1_0'
require_relative './behaviors/Agent_2_0p1_1_1'
require_relative './behaviors/Agent_2_0p1_1_2'
require_relative './behaviors/Agent_2_0p1_1_3'
require_relative './behaviors/Agent_2_0p1_1_4'
require_relative './behaviors/Agent_0_0p6_0_0'
require_relative './behaviors/Agent_0_0p6_0_1'
require_relative './behaviors/Agent_0_0p6_0_2'
require_relative './behaviors/Agent_0_0p6_0_3'
require_relative './behaviors/Agent_0_0p6_0_4'
require_relative './behaviors/Agent_0_0p6_1_0'
require_relative './behaviors/Agent_0_0p6_1_1'
require_relative './behaviors/Agent_0_0p6_1_2'
require_relative './behaviors/Agent_0_0p6_1_3'
require_relative './behaviors/Agent_0_0p6_1_4'
require_relative './behaviors/Agent_1_0p6_0_0'
require_relative './behaviors/Agent_1_0p6_0_1'
require_relative './behaviors/Agent_1_0p6_0_2'
require_relative './behaviors/Agent_1_0p6_0_3'
require_relative './behaviors/Agent_1_0p6_0_4'
require_relative './behaviors/Agent_1_0p6_1_0'
require_relative './behaviors/Agent_1_0p6_1_1'
require_relative './behaviors/Agent_1_0p6_1_2'
require_relative './behaviors/Agent_1_0p6_1_3'
require_relative './behaviors/Agent_1_0p6_1_4'
require_relative './behaviors/Agent_2_0p6_0_0'
require_relative './behaviors/Agent_2_0p6_0_1'
require_relative './behaviors/Agent_2_0p6_0_2'
require_relative './behaviors/Agent_2_0p6_0_3'
require_relative './behaviors/Agent_2_0p6_0_4'
require_relative './behaviors/Agent_2_0p6_1_0'
require_relative './behaviors/Agent_2_0p6_1_1'
require_relative './behaviors/Agent_2_0p6_1_2'
require_relative './behaviors/Agent_2_0p6_1_3'
require_relative './behaviors/Agent_2_0p6_1_4'
require_relative './behaviors/Agent_0_1p1_0_0'
require_relative './behaviors/Agent_0_1p1_0_1'
require_relative './behaviors/Agent_0_1p1_0_2'
require_relative './behaviors/Agent_0_1p1_0_3'
require_relative './behaviors/Agent_0_1p1_0_4'
require_relative './behaviors/Agent_0_1p1_1_0'
require_relative './behaviors/Agent_0_1p1_1_1'
require_relative './behaviors/Agent_0_1p1_1_2'
require_relative './behaviors/Agent_0_1p1_1_3'
require_relative './behaviors/Agent_0_1p1_1_4'
require_relative './behaviors/Agent_1_1p1_0_0'
require_relative './behaviors/Agent_1_1p1_0_1'
require_relative './behaviors/Agent_1_1p1_0_2'
require_relative './behaviors/Agent_1_1p1_0_3'
require_relative './behaviors/Agent_1_1p1_0_4'
require_relative './behaviors/Agent_1_1p1_1_0'
require_relative './behaviors/Agent_1_1p1_1_1'
require_relative './behaviors/Agent_1_1p1_1_2'
require_relative './behaviors/Agent_1_1p1_1_3'
require_relative './behaviors/Agent_1_1p1_1_4'
require_relative './behaviors/Agent_2_1p1_0_0'
require_relative './behaviors/Agent_2_1p1_0_1'
require_relative './behaviors/Agent_2_1p1_0_2'
require_relative './behaviors/Agent_2_1p1_0_3'
require_relative './behaviors/Agent_2_1p1_0_4'
require_relative './behaviors/Agent_2_1p1_1_0'
require_relative './behaviors/Agent_2_1p1_1_1'
require_relative './behaviors/Agent_2_1p1_1_2'
require_relative './behaviors/Agent_2_1p1_1_3'
require_relative './behaviors/Agent_2_1p1_1_4'
require_relative './behaviors/Agent_0_1p6_0_0'
require_relative './behaviors/Agent_0_1p6_0_1'
require_relative './behaviors/Agent_0_1p6_0_2'
require_relative './behaviors/Agent_0_1p6_0_3'
require_relative './behaviors/Agent_0_1p6_0_4'
require_relative './behaviors/Agent_0_1p6_1_0'
require_relative './behaviors/Agent_0_1p6_1_1'
require_relative './behaviors/Agent_0_1p6_1_2'
require_relative './behaviors/Agent_0_1p6_1_3'
require_relative './behaviors/Agent_0_1p6_1_4'
require_relative './behaviors/Agent_1_1p6_0_0'
require_relative './behaviors/Agent_1_1p6_0_1'
require_relative './behaviors/Agent_1_1p6_0_2'
require_relative './behaviors/Agent_1_1p6_0_3'
require_relative './behaviors/Agent_1_1p6_0_4'
require_relative './behaviors/Agent_1_1p6_1_0'
require_relative './behaviors/Agent_1_1p6_1_1'
require_relative './behaviors/Agent_1_1p6_1_2'
require_relative './behaviors/Agent_1_1p6_1_3'
require_relative './behaviors/Agent_1_1p6_1_4'
require_relative './behaviors/Agent_2_1p6_0_0'
require_relative './behaviors/Agent_2_1p6_0_1'
require_relative './behaviors/Agent_2_1p6_0_2'
require_relative './behaviors/Agent_2_1p6_0_3'
require_relative './behaviors/Agent_2_1p6_0_4'
require_relative './behaviors/Agent_2_1p6_1_0'
require_relative './behaviors/Agent_2_1p6_1_1'
require_relative './behaviors/Agent_2_1p6_1_2'
require_relative './behaviors/Agent_2_1p6_1_3'
require_relative './behaviors/Agent_2_1p6_1_4'
require_relative './behaviors/Agent_0_2p1_0_0'
require_relative './behaviors/Agent_0_2p1_0_1'
require_relative './behaviors/Agent_0_2p1_0_2'
require_relative './behaviors/Agent_0_2p1_0_3'
require_relative './behaviors/Agent_0_2p1_0_4'
require_relative './behaviors/Agent_0_2p1_1_0'
require_relative './behaviors/Agent_0_2p1_1_1'
require_relative './behaviors/Agent_0_2p1_1_2'
require_relative './behaviors/Agent_0_2p1_1_3'
require_relative './behaviors/Agent_0_2p1_1_4'
require_relative './behaviors/Agent_1_2p1_0_0'
require_relative './behaviors/Agent_1_2p1_0_1'
require_relative './behaviors/Agent_1_2p1_0_2'
require_relative './behaviors/Agent_1_2p1_0_3'
require_relative './behaviors/Agent_1_2p1_0_4'
require_relative './behaviors/Agent_1_2p1_1_0'
require_relative './behaviors/Agent_1_2p1_1_1'
require_relative './behaviors/Agent_1_2p1_1_2'
require_relative './behaviors/Agent_1_2p1_1_3'
require_relative './behaviors/Agent_1_2p1_1_4'
require_relative './behaviors/Agent_2_2p1_0_0'
require_relative './behaviors/Agent_2_2p1_0_1'
require_relative './behaviors/Agent_2_2p1_0_2'
require_relative './behaviors/Agent_2_2p1_0_3'
require_relative './behaviors/Agent_2_2p1_0_4'
require_relative './behaviors/Agent_2_2p1_1_0'
require_relative './behaviors/Agent_2_2p1_1_1'
require_relative './behaviors/Agent_2_2p1_1_2'
require_relative './behaviors/Agent_2_2p1_1_3'
require_relative './behaviors/Agent_2_2p1_1_4'
require_relative './behaviors/Agent_0_2p6_0_0'
require_relative './behaviors/Agent_0_2p6_0_1'
require_relative './behaviors/Agent_0_2p6_0_2'
require_relative './behaviors/Agent_0_2p6_0_3'
require_relative './behaviors/Agent_0_2p6_0_4'
require_relative './behaviors/Agent_0_2p6_1_0'
require_relative './behaviors/Agent_0_2p6_1_1'
require_relative './behaviors/Agent_0_2p6_1_2'
require_relative './behaviors/Agent_0_2p6_1_3'
require_relative './behaviors/Agent_0_2p6_1_4'
require_relative './behaviors/Agent_1_2p6_0_0'
require_relative './behaviors/Agent_1_2p6_0_1'
require_relative './behaviors/Agent_1_2p6_0_2'
require_relative './behaviors/Agent_1_2p6_0_3'
require_relative './behaviors/Agent_1_2p6_0_4'
require_relative './behaviors/Agent_1_2p6_1_0'
require_relative './behaviors/Agent_1_2p6_1_1'
require_relative './behaviors/Agent_1_2p6_1_2'
require_relative './behaviors/Agent_1_2p6_1_3'
require_relative './behaviors/Agent_1_2p6_1_4'
require_relative './behaviors/Agent_2_2p6_0_0'
require_relative './behaviors/Agent_2_2p6_0_1'
require_relative './behaviors/Agent_2_2p6_0_2'
require_relative './behaviors/Agent_2_2p6_0_3'
require_relative './behaviors/Agent_2_2p6_0_4'
require_relative './behaviors/Agent_2_2p6_1_0'
require_relative './behaviors/Agent_2_2p6_1_1'
require_relative './behaviors/Agent_2_2p6_1_2'
require_relative './behaviors/Agent_2_2p6_1_3'
require_relative './behaviors/Agent_2_2p6_1_4'
require_relative './behaviors/Agent_0_1p0_0_0'
require_relative './behaviors/Agent_0_1p0_0_1'
require_relative './behaviors/Agent_0_1p0_0_2'
require_relative './behaviors/Agent_0_1p0_0_3'
require_relative './behaviors/Agent_0_1p0_0_4'
require_relative './behaviors/Agent_0_1p0_1_0'
require_relative './behaviors/Agent_0_1p0_1_1'
require_relative './behaviors/Agent_0_1p0_1_2'
require_relative './behaviors/Agent_0_1p0_1_3'
require_relative './behaviors/Agent_0_1p0_1_4'
require_relative './behaviors/Agent_1_1p0_0_0'
require_relative './behaviors/Agent_1_1p0_0_1'
require_relative './behaviors/Agent_1_1p0_0_2'
require_relative './behaviors/Agent_1_1p0_0_3'
require_relative './behaviors/Agent_1_1p0_0_4'
require_relative './behaviors/Agent_1_1p0_1_0'
require_relative './behaviors/Agent_1_1p0_1_1'
require_relative './behaviors/Agent_1_1p0_1_2'
require_relative './behaviors/Agent_1_1p0_1_3'
require_relative './behaviors/Agent_1_1p0_1_4'
require_relative './behaviors/Agent_2_1p0_0_0'
require_relative './behaviors/Agent_2_1p0_0_1'
require_relative './behaviors/Agent_2_1p0_0_2'
require_relative './behaviors/Agent_2_1p0_0_3'
require_relative './behaviors/Agent_2_1p0_0_4'
require_relative './behaviors/Agent_2_1p0_1_0'
require_relative './behaviors/Agent_2_1p0_1_1'
require_relative './behaviors/Agent_2_1p0_1_2'
require_relative './behaviors/Agent_2_1p0_1_3'
require_relative './behaviors/Agent_2_1p0_1_4'
require_relative './behaviors/Agent_0_2p0_0_0'
require_relative './behaviors/Agent_0_2p0_0_1'
require_relative './behaviors/Agent_0_2p0_0_2'
require_relative './behaviors/Agent_0_2p0_0_3'
require_relative './behaviors/Agent_0_2p0_0_4'
require_relative './behaviors/Agent_0_2p0_1_0'
require_relative './behaviors/Agent_0_2p0_1_1'
require_relative './behaviors/Agent_0_2p0_1_2'
require_relative './behaviors/Agent_0_2p0_1_3'
require_relative './behaviors/Agent_0_2p0_1_4'
require_relative './behaviors/Agent_1_2p0_0_0'
require_relative './behaviors/Agent_1_2p0_0_1'
require_relative './behaviors/Agent_1_2p0_0_2'
require_relative './behaviors/Agent_1_2p0_0_3'
require_relative './behaviors/Agent_1_2p0_0_4'
require_relative './behaviors/Agent_1_2p0_1_0'
require_relative './behaviors/Agent_1_2p0_1_1'
require_relative './behaviors/Agent_1_2p0_1_2'
require_relative './behaviors/Agent_1_2p0_1_3'
require_relative './behaviors/Agent_1_2p0_1_4'
require_relative './behaviors/Agent_2_2p0_0_0'
require_relative './behaviors/Agent_2_2p0_0_1'
require_relative './behaviors/Agent_2_2p0_0_2'
require_relative './behaviors/Agent_2_2p0_0_3'
require_relative './behaviors/Agent_2_2p0_0_4'
require_relative './behaviors/Agent_2_2p0_1_0'
require_relative './behaviors/Agent_2_2p0_1_1'
require_relative './behaviors/Agent_2_2p0_1_2'
require_relative './behaviors/Agent_2_2p0_1_3'
require_relative './behaviors/Agent_2_2p0_1_4'


class Adaptorator
 include RobustRobot

  @nameOfOpponent=nil
  @opponentIndex=(-1)
  

# this is the default starting duck.  
# also sets the default behavior agent name
  def decideHowToInitialize
    @behaviorAgentName="Agent_0_0p1_0_0"
    return @behaviorAgentName
  end

# makes sure that the agent names are aligned to the right format
def ensureNamingCorrect(agentName)
  parts = agentName.split("_")
  if parts[2].length ==1 then
    parts[2]=parts[2]+"p0"
  end
  
  answer=""
  first = false
  parts.each do |item|
     place=""
     if first then
       place = "_"
     end
     
     answer+=place+item
     first = true
     
  end
     
  return answer
  
end

def changeClassNaming(cs)
  if cs==nil then
    puts("Received a nil cs")
    return
  end 
  if cs.start_with?("StaticBaseDuck") then
     
     return cs.sub("StaticBaseDuck","Agent")
  end
  return cs
end


  def initialize
    puts("setting up")
      @loadThisBot = decideHowToInitialize
      ProbabilityLoader.loadProbabilities()
  

      
      @behaviors = {  "Agent_0_0p1_0_0"=>Agent_0_0p1_0_0.new,
                      "Agent_0_0p1_0_1"=>Agent_0_0p1_0_1.new,
                      "Agent_0_0p1_0_2"=>Agent_0_0p1_0_2.new,
                      "Agent_0_0p1_0_3"=>Agent_0_0p1_0_3.new,
                      "Agent_0_0p1_0_4"=>Agent_0_0p1_0_4.new,
                      "Agent_0_0p1_1_0"=>Agent_0_0p1_1_0.new,
                      "Agent_0_0p1_1_1"=>Agent_0_0p1_1_1.new,
                      "Agent_0_0p1_1_2"=>Agent_0_0p1_1_2.new,
                      "Agent_0_0p1_1_3"=>Agent_0_0p1_1_3.new,
                      "Agent_0_0p1_1_4"=>Agent_0_0p1_1_4.new,
                      "Agent_1_0p1_0_0"=>Agent_1_0p1_0_0.new,
                      "Agent_1_0p1_0_1"=>Agent_1_0p1_0_1.new,
                      "Agent_1_0p1_0_2"=>Agent_1_0p1_0_2.new,
                      "Agent_1_0p1_0_3"=>Agent_1_0p1_0_3.new,
                      "Agent_1_0p1_0_4"=>Agent_1_0p1_0_4.new,
                      "Agent_1_0p1_1_0"=>Agent_1_0p1_1_0.new,
                      "Agent_1_0p1_1_1"=>Agent_1_0p1_1_1.new,
                      "Agent_1_0p1_1_2"=>Agent_1_0p1_1_2.new,
                      "Agent_1_0p1_1_3"=>Agent_1_0p1_1_3.new,
                      "Agent_1_0p1_1_4"=>Agent_1_0p1_1_4.new,
                      "Agent_2_0p1_0_0"=>Agent_2_0p1_0_0.new,
                      "Agent_2_0p1_0_1"=>Agent_2_0p1_0_1.new,
                      "Agent_2_0p1_0_2"=>Agent_2_0p1_0_2.new,
                      "Agent_2_0p1_0_3"=>Agent_2_0p1_0_3.new,
                      "Agent_2_0p1_0_4"=>Agent_2_0p1_0_4.new,
                      "Agent_2_0p1_1_0"=>Agent_2_0p1_1_0.new,
                      "Agent_2_0p1_1_1"=>Agent_2_0p1_1_1.new,
                      "Agent_2_0p1_1_2"=>Agent_2_0p1_1_2.new,
                      "Agent_2_0p1_1_3"=>Agent_2_0p1_1_3.new,
                      "Agent_2_0p1_1_4"=>Agent_2_0p1_1_4.new,
                      "Agent_0_0p6_0_0"=>Agent_0_0p6_0_0.new,
                      "Agent_0_0p6_0_1"=>Agent_0_0p6_0_1.new,
                      "Agent_0_0p6_0_2"=>Agent_0_0p6_0_2.new,
                      "Agent_0_0p6_0_3"=>Agent_0_0p6_0_3.new,
                      "Agent_0_0p6_0_4"=>Agent_0_0p6_0_4.new,
                      "Agent_0_0p6_1_0"=>Agent_0_0p6_1_0.new,
                      "Agent_0_0p6_1_1"=>Agent_0_0p6_1_1.new,
                      "Agent_0_0p6_1_2"=>Agent_0_0p6_1_2.new,
                      "Agent_0_0p6_1_3"=>Agent_0_0p6_1_3.new,
                      "Agent_0_0p6_1_4"=>Agent_0_0p6_1_4.new,
                      "Agent_1_0p6_0_0"=>Agent_1_0p6_0_0.new,
                      "Agent_1_0p6_0_1"=>Agent_1_0p6_0_1.new,
                      "Agent_1_0p6_0_2"=>Agent_1_0p6_0_2.new,
                      "Agent_1_0p6_0_3"=>Agent_1_0p6_0_3.new,
                      "Agent_1_0p6_0_4"=>Agent_1_0p6_0_4.new,
                      "Agent_1_0p6_1_0"=>Agent_1_0p6_1_0.new,
                      "Agent_1_0p6_1_1"=>Agent_1_0p6_1_1.new,
                      "Agent_1_0p6_1_2"=>Agent_1_0p6_1_2.new,
                      "Agent_1_0p6_1_3"=>Agent_1_0p6_1_3.new,
                      "Agent_1_0p6_1_4"=>Agent_1_0p6_1_4.new,
                      "Agent_2_0p6_0_0"=>Agent_2_0p6_0_0.new,
                      "Agent_2_0p6_0_1"=>Agent_2_0p6_0_1.new,
                      "Agent_2_0p6_0_2"=>Agent_2_0p6_0_2.new,
                      "Agent_2_0p6_0_3"=>Agent_2_0p6_0_3.new,
                      "Agent_2_0p6_0_4"=>Agent_2_0p6_0_4.new,
                      "Agent_2_0p6_1_0"=>Agent_2_0p6_1_0.new,
                      "Agent_2_0p6_1_1"=>Agent_2_0p6_1_1.new,
                      "Agent_2_0p6_1_2"=>Agent_2_0p6_1_2.new,
                      "Agent_2_0p6_1_3"=>Agent_2_0p6_1_3.new,
                      "Agent_2_0p6_1_4"=>Agent_2_0p6_1_4.new,
                      "Agent_0_1p1_0_0"=>Agent_0_1p1_0_0.new,
                      "Agent_0_1p1_0_1"=>Agent_0_1p1_0_1.new,
                      "Agent_0_1p1_0_2"=>Agent_0_1p1_0_2.new,
                      "Agent_0_1p1_0_3"=>Agent_0_1p1_0_3.new,
                      "Agent_0_1p1_0_4"=>Agent_0_1p1_0_4.new,
                      "Agent_0_1p1_1_0"=>Agent_0_1p1_1_0.new,
                      "Agent_0_1p1_1_1"=>Agent_0_1p1_1_1.new,
                      "Agent_0_1p1_1_2"=>Agent_0_1p1_1_2.new,
                      "Agent_0_1p1_1_3"=>Agent_0_1p1_1_3.new,
                      "Agent_0_1p1_1_4"=>Agent_0_1p1_1_4.new,
                      "Agent_1_1p1_0_0"=>Agent_1_1p1_0_0.new,
                      "Agent_1_1p1_0_1"=>Agent_1_1p1_0_1.new,
                      "Agent_1_1p1_0_2"=>Agent_1_1p1_0_2.new,
                      "Agent_1_1p1_0_3"=>Agent_1_1p1_0_3.new,
                      "Agent_1_1p1_0_4"=>Agent_1_1p1_0_4.new,
                      "Agent_1_1p1_1_0"=>Agent_1_1p1_1_0.new,
                      "Agent_1_1p1_1_1"=>Agent_1_1p1_1_1.new,
                      "Agent_1_1p1_1_2"=>Agent_1_1p1_1_2.new,
                      "Agent_1_1p1_1_3"=>Agent_1_1p1_1_3.new,
                      "Agent_1_1p1_1_4"=>Agent_1_1p1_1_4.new,
                      "Agent_2_1p1_0_0"=>Agent_2_1p1_0_0.new,
                      "Agent_2_1p1_0_1"=>Agent_2_1p1_0_1.new,
                      "Agent_2_1p1_0_2"=>Agent_2_1p1_0_2.new,
                      "Agent_2_1p1_0_3"=>Agent_2_1p1_0_3.new,
                      "Agent_2_1p1_0_4"=>Agent_2_1p1_0_4.new,
                      "Agent_2_1p1_1_0"=>Agent_2_1p1_1_0.new,
                      "Agent_2_1p1_1_1"=>Agent_2_1p1_1_1.new,
                      "Agent_2_1p1_1_2"=>Agent_2_1p1_1_2.new,
                      "Agent_2_1p1_1_3"=>Agent_2_1p1_1_3.new,
                      "Agent_2_1p1_1_4"=>Agent_2_1p1_1_4.new,
                      "Agent_0_1p6_0_0"=>Agent_0_1p6_0_0.new,
                      "Agent_0_1p6_0_1"=>Agent_0_1p6_0_1.new,
                      "Agent_0_1p6_0_2"=>Agent_0_1p6_0_2.new,
                      "Agent_0_1p6_0_3"=>Agent_0_1p6_0_3.new,
                      "Agent_0_1p6_0_4"=>Agent_0_1p6_0_4.new,
                      "Agent_0_1p6_1_0"=>Agent_0_1p6_1_0.new,
                      "Agent_0_1p6_1_1"=>Agent_0_1p6_1_1.new,
                      "Agent_0_1p6_1_2"=>Agent_0_1p6_1_2.new,
                      "Agent_0_1p6_1_3"=>Agent_0_1p6_1_3.new,
                      "Agent_0_1p6_1_4"=>Agent_0_1p6_1_4.new,
                      "Agent_1_1p6_0_0"=>Agent_1_1p6_0_0.new,
                      "Agent_1_1p6_0_1"=>Agent_1_1p6_0_1.new,
                      "Agent_1_1p6_0_2"=>Agent_1_1p6_0_2.new,
                      "Agent_1_1p6_0_3"=>Agent_1_1p6_0_3.new,
                      "Agent_1_1p6_0_4"=>Agent_1_1p6_0_4.new,
                      "Agent_1_1p6_1_0"=>Agent_1_1p6_1_0.new,
                      "Agent_1_1p6_1_1"=>Agent_1_1p6_1_1.new,
                      "Agent_1_1p6_1_2"=>Agent_1_1p6_1_2.new,
                      "Agent_1_1p6_1_3"=>Agent_1_1p6_1_3.new,
                      "Agent_1_1p6_1_4"=>Agent_1_1p6_1_4.new,
                      "Agent_2_1p6_0_0"=>Agent_2_1p6_0_0.new,
                      "Agent_2_1p6_0_1"=>Agent_2_1p6_0_1.new,
                      "Agent_2_1p6_0_2"=>Agent_2_1p6_0_2.new,
                      "Agent_2_1p6_0_3"=>Agent_2_1p6_0_3.new,
                      "Agent_2_1p6_0_4"=>Agent_2_1p6_0_4.new,
                      "Agent_2_1p6_1_0"=>Agent_2_1p6_1_0.new,
                      "Agent_2_1p6_1_1"=>Agent_2_1p6_1_1.new,
                      "Agent_2_1p6_1_2"=>Agent_2_1p6_1_2.new,
                      "Agent_2_1p6_1_3"=>Agent_2_1p6_1_3.new,
                      "Agent_2_1p6_1_4"=>Agent_2_1p6_1_4.new,
                      "Agent_0_2p1_0_0"=>Agent_0_2p1_0_0.new,
                      "Agent_0_2p1_0_1"=>Agent_0_2p1_0_1.new,
                      "Agent_0_2p1_0_2"=>Agent_0_2p1_0_2.new,
                      "Agent_0_2p1_0_3"=>Agent_0_2p1_0_3.new,
                      "Agent_0_2p1_0_4"=>Agent_0_2p1_0_4.new,
                      "Agent_0_2p1_1_0"=>Agent_0_2p1_1_0.new,
                      "Agent_0_2p1_1_1"=>Agent_0_2p1_1_1.new,
                      "Agent_0_2p1_1_2"=>Agent_0_2p1_1_2.new,
                      "Agent_0_2p1_1_3"=>Agent_0_2p1_1_3.new,
                      "Agent_0_2p1_1_4"=>Agent_0_2p1_1_4.new,
                      "Agent_1_2p1_0_0"=>Agent_1_2p1_0_0.new,
                      "Agent_1_2p1_0_1"=>Agent_1_2p1_0_1.new,
                      "Agent_1_2p1_0_2"=>Agent_1_2p1_0_2.new,
                      "Agent_1_2p1_0_3"=>Agent_1_2p1_0_3.new,
                      "Agent_1_2p1_0_4"=>Agent_1_2p1_0_4.new,
                      "Agent_1_2p1_1_0"=>Agent_1_2p1_1_0.new,
                      "Agent_1_2p1_1_1"=>Agent_1_2p1_1_1.new,
                      "Agent_1_2p1_1_2"=>Agent_1_2p1_1_2.new,
                      "Agent_1_2p1_1_3"=>Agent_1_2p1_1_3.new,
                      "Agent_1_2p1_1_4"=>Agent_1_2p1_1_4.new,
                      "Agent_2_2p1_0_0"=>Agent_2_2p1_0_0.new,
                      "Agent_2_2p1_0_1"=>Agent_2_2p1_0_1.new,
                      "Agent_2_2p1_0_2"=>Agent_2_2p1_0_2.new,
                      "Agent_2_2p1_0_3"=>Agent_2_2p1_0_3.new,
                      "Agent_2_2p1_0_4"=>Agent_2_2p1_0_4.new,
                      "Agent_2_2p1_1_0"=>Agent_2_2p1_1_0.new,
                      "Agent_2_2p1_1_1"=>Agent_2_2p1_1_1.new,
                      "Agent_2_2p1_1_2"=>Agent_2_2p1_1_2.new,
                      "Agent_2_2p1_1_3"=>Agent_2_2p1_1_3.new,
                      "Agent_2_2p1_1_4"=>Agent_2_2p1_1_4.new,
                      "Agent_0_2p6_0_0"=>Agent_0_2p6_0_0.new,
                      "Agent_0_2p6_0_1"=>Agent_0_2p6_0_1.new,
                      "Agent_0_2p6_0_2"=>Agent_0_2p6_0_2.new,
                      "Agent_0_2p6_0_3"=>Agent_0_2p6_0_3.new,
                      "Agent_0_2p6_0_4"=>Agent_0_2p6_0_4.new,
                      "Agent_0_2p6_1_0"=>Agent_0_2p6_1_0.new,
                      "Agent_0_2p6_1_1"=>Agent_0_2p6_1_1.new,
                      "Agent_0_2p6_1_2"=>Agent_0_2p6_1_2.new,
                      "Agent_0_2p6_1_3"=>Agent_0_2p6_1_3.new,
                      "Agent_0_2p6_1_4"=>Agent_0_2p6_1_4.new,
                      "Agent_1_2p6_0_0"=>Agent_1_2p6_0_0.new,
                      "Agent_1_2p6_0_1"=>Agent_1_2p6_0_1.new,
                      "Agent_1_2p6_0_2"=>Agent_1_2p6_0_2.new,
                      "Agent_1_2p6_0_3"=>Agent_1_2p6_0_3.new,
                      "Agent_1_2p6_0_4"=>Agent_1_2p6_0_4.new,
                      "Agent_1_2p6_1_0"=>Agent_1_2p6_1_0.new,
                      "Agent_1_2p6_1_1"=>Agent_1_2p6_1_1.new,
                      "Agent_1_2p6_1_2"=>Agent_1_2p6_1_2.new,
                      "Agent_1_2p6_1_3"=>Agent_1_2p6_1_3.new,
                      "Agent_1_2p6_1_4"=>Agent_1_2p6_1_4.new,
                      "Agent_2_2p6_0_0"=>Agent_2_2p6_0_0.new,
                      "Agent_2_2p6_0_1"=>Agent_2_2p6_0_1.new,
                      "Agent_2_2p6_0_2"=>Agent_2_2p6_0_2.new,
                      "Agent_2_2p6_0_3"=>Agent_2_2p6_0_3.new,
                      "Agent_2_2p6_0_4"=>Agent_2_2p6_0_4.new,
                      "Agent_2_2p6_1_0"=>Agent_2_2p6_1_0.new,
                      "Agent_2_2p6_1_1"=>Agent_2_2p6_1_1.new,
                      "Agent_2_2p6_1_2"=>Agent_2_2p6_1_2.new,
                      "Agent_2_2p6_1_3"=>Agent_2_2p6_1_3.new,
                      "Agent_2_2p6_1_4"=>Agent_2_2p6_1_4.new,
                      "Agent_0_1p0_0_0"=>Agent_0_1p0_0_0.new,
                      "Agent_0_1p0_0_1"=>Agent_0_1p0_0_1.new,
                      "Agent_0_1p0_0_2"=>Agent_0_1p0_0_2.new,
                      "Agent_0_1p0_0_3"=>Agent_0_1p0_0_3.new,
                      "Agent_0_1p0_0_4"=>Agent_0_1p0_0_4.new,
                      "Agent_0_1p0_1_0"=>Agent_0_1p0_1_0.new,
                      "Agent_0_1p0_1_1"=>Agent_0_1p0_1_1.new,
                      "Agent_0_1p0_1_2"=>Agent_0_1p0_1_2.new,
                      "Agent_0_1p0_1_3"=>Agent_0_1p0_1_3.new,
                      "Agent_0_1p0_1_4"=>Agent_0_1p0_1_4.new,
                      "Agent_1_1p0_0_0"=>Agent_1_1p0_0_0.new,
                      "Agent_1_1p0_0_1"=>Agent_1_1p0_0_1.new,
                      "Agent_1_1p0_0_2"=>Agent_1_1p0_0_2.new,
                      "Agent_1_1p0_0_3"=>Agent_1_1p0_0_3.new,
                      "Agent_1_1p0_0_4"=>Agent_1_1p0_0_4.new,
                      "Agent_1_1p0_1_0"=>Agent_1_1p0_1_0.new,
                      "Agent_1_1p0_1_1"=>Agent_1_1p0_1_1.new,
                      "Agent_1_1p0_1_2"=>Agent_1_1p0_1_2.new,
                      "Agent_1_1p0_1_3"=>Agent_1_1p0_1_3.new,
                      "Agent_1_1p0_1_4"=>Agent_1_1p0_1_4.new,
                      "Agent_2_1p0_0_0"=>Agent_2_1p0_0_0.new,
                      "Agent_2_1p0_0_1"=>Agent_2_1p0_0_1.new,
                      "Agent_2_1p0_0_2"=>Agent_2_1p0_0_2.new,
                      "Agent_2_1p0_0_3"=>Agent_2_1p0_0_3.new,
                      "Agent_2_1p0_0_4"=>Agent_2_1p0_0_4.new,
                      "Agent_2_1p0_1_0"=>Agent_2_1p0_1_0.new,
                      "Agent_2_1p0_1_1"=>Agent_2_1p0_1_1.new,
                      "Agent_2_1p0_1_2"=>Agent_2_1p0_1_2.new,
                      "Agent_2_1p0_1_3"=>Agent_2_1p0_1_3.new,
                      "Agent_2_1p0_1_4"=>Agent_2_1p0_1_4.new,
                      "Agent_0_2p0_0_0"=>Agent_0_2p0_0_0.new,
                      "Agent_0_2p0_0_1"=>Agent_0_2p0_0_1.new,
                      "Agent_0_2p0_0_2"=>Agent_0_2p0_0_2.new,
                      "Agent_0_2p0_0_3"=>Agent_0_2p0_0_3.new,
                      "Agent_0_2p0_0_4"=>Agent_0_2p0_0_4.new,
                      "Agent_0_2p0_1_0"=>Agent_0_2p0_1_0.new,
                      "Agent_0_2p0_1_1"=>Agent_0_2p0_1_1.new,
                      "Agent_0_2p0_1_2"=>Agent_0_2p0_1_2.new,
                      "Agent_0_2p0_1_3"=>Agent_0_2p0_1_3.new,
                      "Agent_0_2p0_1_4"=>Agent_0_2p0_1_4.new,
                      "Agent_1_2p0_0_0"=>Agent_1_2p0_0_0.new,
                      "Agent_1_2p0_0_1"=>Agent_1_2p0_0_1.new,
                      "Agent_1_2p0_0_2"=>Agent_1_2p0_0_2.new,
                      "Agent_1_2p0_0_3"=>Agent_1_2p0_0_3.new,
                      "Agent_1_2p0_0_4"=>Agent_1_2p0_0_4.new,
                      "Agent_1_2p0_1_0"=>Agent_1_2p0_1_0.new,
                      "Agent_1_2p0_1_1"=>Agent_1_2p0_1_1.new,
                      "Agent_1_2p0_1_2"=>Agent_1_2p0_1_2.new,
                      "Agent_1_2p0_1_3"=>Agent_1_2p0_1_3.new,
                      "Agent_1_2p0_1_4"=>Agent_1_2p0_1_4.new,
                      "Agent_2_2p0_0_0"=>Agent_2_2p0_0_0.new,
                      "Agent_2_2p0_0_1"=>Agent_2_2p0_0_1.new,
                      "Agent_2_2p0_0_2"=>Agent_2_2p0_0_2.new,
                      "Agent_2_2p0_0_3"=>Agent_2_2p0_0_3.new,
                      "Agent_2_2p0_0_4"=>Agent_2_2p0_0_4.new,
                      "Agent_2_2p0_1_0"=>Agent_2_2p0_1_0.new,
                      "Agent_2_2p0_1_1"=>Agent_2_2p0_1_1.new,
                      "Agent_2_2p0_1_2"=>Agent_2_2p0_1_2.new,
                      "Agent_2_2p0_1_3"=>Agent_2_2p0_1_3.new,
                      "Agent_2_2p0_1_4"=>Agent_2_2p0_1_4.new

      }
      
    clazz = Object.const_get(@loadThisBot)  
    @myduck= clazz.new
   
   # @myduck = @behaviors[@loadThisBot];
   
  end
  

  




  # execute the tick method the selected behavior
  def tick events
    puts("ticking")
     @myduck.clearall()
     #initialize states
     @myduck.fire=0
     @myduck.time = time
     @myduck.x=@x
     @myduck.y=@y
     @myduck.size=@size
     @myduck.battlefield_height=@battlefield_height
     @myduck.battlefield_width=@battlefield_width
     @myduck.energy = @energy
     @myduck.dead = @dead
     @myduck.heading = @heading
     
     
     
     
     
     @myduck.tick(events)
    
     
    
     opposing_force= OrderOfBattle.getOpponentName(self.class.name)
     if opposing_force==nil then
       puts("No opposing class")
       
     else
       
       puts("Enemy="+opposing_force)
       opposing_force = changeClassNaming(opposing_force)
       puts("Renamed Enemy="+opposing_force)
       if @nameOfOpponent==nil then
          
           myIndex=  ProbabilityLoader.getAgentIndex(opposing_force)
           puts("agent index ="+myIndex.to_s())
           if myIndex<(-1) then
             if opposing_force!=nil then
               puts("No candidate system identified!  Make sure that "+opposing_force+" is in the data set!")
             end
           else
             
             
             @nameOfOpponent = opposing_force
             @opponentIndex=  ProbabilityLoader.getAgentIndex(opposing_force)
             
             puts("opponentIndex="+@opponentIndex.to_s())
             
             if @opponentIndex<0 then
               puts("I can not find this enemy-"+opposing_force)
             end
             
             puts("solving probabilityLoader")
             ProbabilityLoader.solve(@nameOfOpponent)
             
             bestIndex= ProbabilityLoader.pickBestIndex(@opponentIndex)
             puts("best index ="+bestIndex.to_s())
             @behaviorAgentName=ProbabilityLoader.getAgentName(bestIndex)
             @behaviorAgentName = ensureNamingCorrect(@behaviorAgentName)
             puts("best strategy name= "+@behaviorAgentName)
             
             
           
             # now change myduck to adapt to the behavior
             puts("BEHAVIOR CHANGE = "+@behaviorAgentName)
             @myduck = @behaviors[@behaviorAgentName]
           #    clazz = Object.const_get(@loadThisBot)  
           #     @myduck= clazz.new
             
             if @myduck==nil then 
               puts("CRITICAL FAIL NO DUCK!!!")
               
             end
             
             @myduck.fire=0
             @myduck.time = time
             @myduck.x=@x
             @myduck.y=@y
             @myduck.size=@size
             @myduck.battlefield_height=@battlefield_height
             @myduck.battlefield_width=@battlefield_width
             @myduck.energy = @energy
             @myduck.dead = @dead
             @myduck.heading = @heading
             @myduck.tick(events)
           end
             
           
           
           
           
       end
         
     
     
     
     end
     
     
    
     
    
       
     puts("accelerating "+@myduck.performAccelerate.to_s())
    accelerate(@myduck.performAccelerate)
    
    puts("myduck.fire="+@myduck.fire.to_s())
    
    fire(@myduck.fire)
    puts("turning gun to "+@myduck.turn_gun.to_s())
    turn_gun(@myduck.turn_gun)
    puts("turning "+@myduck.turn.to_s())
    turn(@myduck.turn)
    
    puts("turning radar to "+@myduck.turn_radar.to_s())
    turn_radar(@myduck.turn_radar)
   
    
    puts("saying things")
    if @myduck.say!=nil then
        say(@myduck.say)
    end
        
    if @myduck.performStop then
       puts("stopping")
       stop
    end
    
 
   
  end
  
  
end