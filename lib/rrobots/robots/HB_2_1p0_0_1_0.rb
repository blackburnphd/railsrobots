require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_2_1p0_0_1_0  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 2
    @fire_power = 1.0
    @aiming_strategy = 0
    @movement_strategy = 1
    @resupply_strategy = 0
  end
end
