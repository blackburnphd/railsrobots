require_relative 'AdaptingSelfHealingAgentTwoBranch'

class Adapting3_t100_e0p15_t3000_e0p05_HB < AdaptingSelfHealingAgentTwoBranch

def initialize
  

  
  
   # Adapting2_t100_e0p15_t3000_e0p05_HB
    puts "Initializing Adapting3_t100_e0p15_t3000_e0p05_HB"
    super()    
    # do not adapt before this time
    @adaptAtTime=[100,3000]
    # probability that the opponent will be misclassified
    @probabilityClassifierError = [0.15,0.05]
end

end
