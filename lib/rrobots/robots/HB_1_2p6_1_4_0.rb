require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_2p6_1_4_0  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 2.6
    @aiming_strategy = 1
    @movement_strategy = 4
    @resupply_strategy = 0
  end
end
