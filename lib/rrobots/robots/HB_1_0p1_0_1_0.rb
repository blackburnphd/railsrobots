require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_0p1_0_1_0  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 0.1
    @aiming_strategy = 0
    @movement_strategy = 1
    @resupply_strategy = 0
  end
end
