
require_relative '../robot'

class Static_v3_4
   include Robot

  def tick events
         
    accelerate  1
    if events['robot_scanned'].empty?
      fire 1.0e-001
      turn 0
      turn_gun -30
      accelerate  1*2
      @shot = 1
    else
      if @shotLost ==1 
        turn -2
        turn_gun 30/2
        fire 1.0e-001
      else
        turn_gun 30
        turn 10
        fire 0
        @shotLost = 0
      end
      accelerate  1
    end
    turn_radar 0
     
     
     
     
  end
end
