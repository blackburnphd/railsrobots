require_relative 'BoundaryTracker_Base'

class BoundaryTracker_L < BoundaryTracker_Base
  def initialize
    super
    @circle_direction = :counter_clockwise
  end
end