
require_relative '../robot'

class Static_v2_4
   include Robot

  def tick events
    accelerate 1
    turn 10
    fire 5.0e-001 unless events['robot_scanned'].empty?
    turn_gun 30
    turn_radar 0

  end
end
