
require_relative '../robot'

class DuckBase114
   include Robot

  def tick events
    accelerate 1
    turn 10
    fire 1.0e+000 # unless events['robot_scanned'].empty?
    turn_gun 0
    turn_radar 0

  end
end
