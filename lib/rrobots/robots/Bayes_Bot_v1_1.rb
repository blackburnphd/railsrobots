#require 'rubygems'
require 'sbn'
require_relative '../robot'

class Bayes_Bot_v1_1
   include Robot
   
   def initialize_RN
      net       = Sbn::Net.new("Grass Wetness Belief Net")
      cloudy    = Sbn::Variable.new(net, :cloudy, [0.5, 0.5])
      sprinkler = Sbn::Variable.new(net, :sprinkler, [0.1, 0.9, 0.5, 0.5])
      rain      = Sbn::Variable.new(net, :rain, [0.8, 0.2, 0.2, 0.8])
      grass_wet = Sbn::Variable.new(net, :grass_wet, [0.99, 0.01, 0.9, 0.1, 0.9, 0.1, 0.0, 1.0])
      cloudy.add_child(sprinkler)        # also creates parent relationship
      cloudy.add_child(rain)
      sprinkler.add_child(grass_wet)
      rain.add_child(grass_wet)
      evidence = {:sprinkler => :false, :rain => :true}
      net.set_evidence(evidence)
      puts net.query_variable(:grass_wet)
   end

   
   def initialize
   
      @net           = Sbn::Net.new("Adapt")
      @recent_hit    = Sbn::Variable.new(@net, :recent_hit, [0.5, 0.5])
      @near_boundary = Sbn::Variable.new(@net, :near_boundary, [0.1, 0.9])
      @change_movement      = Sbn::Variable.new(@net, :change_movement, [0.99, 0.01, 0.9, 0.1, 0.9, 0.1, 0.0, 1.0])
      @recent_hit.add_child(@change_movement)        # also creates parent relationship
      @near_boundary.add_child(@change_movement)
      @evidence = {:recent_hit => :false, :near_boundary => :true}
      #evidence = {:recent_hit => :false}
      @net.set_evidence(@evidence)
      puts @net.query_variable(:change_movement)
   end


  def tick events
    @net.set_evidence(@evidence)
    #puts @net.query_variable(:change_movement)
    say @net.query_variable(:change_movement)
    accelerate 1
    turn 5
    fire 5.0e-001 unless events['robot_scanned'].empty?
    turn_gun 15
    turn_radar 0


  end


end
