require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_0_2p6_0_4_1  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 0
    @fire_power = 2.6
    @aiming_strategy = 0
    @movement_strategy = 4
    @resupply_strategy = 1
  end
end
