require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_1p0_0_0_3  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 1.0
    @aiming_strategy = 0
    @movement_strategy = 0
    @resupply_strategy = 3
  end
end
