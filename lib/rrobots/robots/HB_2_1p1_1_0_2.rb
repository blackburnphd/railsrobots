require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_2_1p1_1_0_2  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 2
    @fire_power = 1.1
    @aiming_strategy = 1
    @movement_strategy = 0
    @resupply_strategy = 2
  end
end
