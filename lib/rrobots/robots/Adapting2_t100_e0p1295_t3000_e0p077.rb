require_relative 'AdaptingAgent2'

class Adapting2_t100_e0p1295_t3000_e0p077 < AdaptingAgent2

def initialize
# Adapting2_t100_e0p1295_t3000_e0p077
    super()
    
    # do not adapt before this time
    @adaptAtTime=[100,3000]

    # probability that the opponent will be misclassified
    @probabilityClassifierError = [0.1295,0.0777]
end

end
