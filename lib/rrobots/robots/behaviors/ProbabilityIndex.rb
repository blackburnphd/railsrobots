class ProbabilityIndex
  
   attr_accessor :probability
   attr_accessor :index
  
  def initialize
    @probability=0
    @index=0
  end
  
  # override the comparison operator
  def <=>(probability_index) 
   probability_index.probability<=>probability
  end
  
end