require_relative './ProbabilityIndex'

class ProbabilityLoader

   @@isLoaded=false
   @@lastApriorIndex=(-1)
   @@jointProbabilityMatrix=nil
  
   @@probabilityFile = "./pWin.txt"
   @@delimiter="\t"
   
   def self.useSelfHealingProbabilities
     @@probabilityFile = "./pWinSelfHealing.csv"
     @@delimiter = ","
   end
  
  def self.pickBestIndex(opponentIndex)
  
    sortingArray = Array.new
    size = @@agentNames.length
    for each_agent in 0..(size-1)
      pi=ProbabilityIndex.new
      pi.index=each_agent
      pi.probability=@@posteriorProbabilities[each_agent][opponentIndex]
      sortingArray.push(pi)
    end
    sortingArray.sort!
    # for now, I will allways pick the best item
    return sortingArray[0].index 
    
    
    
  
  end
  
  
 
  #  Look up the probability of wind for this agent agains another opponent agent
  def self.getProbabilityWin(nameOfAgent, nameOfOpponent)
    
    myIndex=  @@agentNames.index(nameOfAgent)
    if myIndex==nil then
      puts("No myIndex found for agent "+nameOfAgent)
      puts("agentNames.length="+@@agentNames.length.to_s())
      puts("name of agent =["+nameOfAgent+"]")
    end
    opponentIndex = @@agentNames.index(nameOfOpponent)
    
    if @@probabilityMatrix==nil then
      puts("can not find probabliity matrix")
        
    end
    return @@probabilityMatrix[myIndex][opponentIndex]
  end
  
# return the probabilityt that the agent defeats the opponent  
  def self.getProbabilityWinByIndex(agentIndex, opponentIndex)
    return @@probabilityMatrix[agentIndex][opponentIndex]
  end
  
  
  # get the number of agents in this data set
  def self.getAgentCount
    return @@agentNames.length
  end
  
  # return the name of this agent based on index
  def self.getAgentName(index)
    return @@agentNames[index]
  end

# Return the index of the agent on the name list
# this gives an index into all of the other tables.
# remember that the tables should be mxm square  
  def self.getAgentIndex(agentname)
    answer= @@agentNames.index(agentname)
    if answer == nil then 
      answer = -1
    end
    return answer
  end
  

  
  
  # choose the best behavior from the selection available
  def self.pick
    computeClassConditionals()
  end
     

  def self.loadProbabilities
    # ensure that we only load once.
    if @@isLoaded then
      @@isLoaded=true
    end
    
    @@agentNames = Array.new
    
  
    
    counter = 0
    begin
        puts "loading " + @@probabilityFile
         config_path = File.expand_path(File.join(File.dirname(__FILE__), @@probabilityFile))
  
      # refactor - make this an input parameter
      file = File.new(config_path, "r")
      while (line = file.gets)
          
          lineArray= line.split(@@delimiter)
          if counter==0 then
            # remove first column tab
           
            
           
            lineArray.delete_at(0)

            puts 'setting names'
            lineArray.each  { |thename| @@agentNames.push(thename.strip) }
                
            
            numberOfNames = @@agentNames.length
           
            @@probabilityMatrix = Array.new(numberOfNames) { Array.new(numberOfNames)}
            
          else
            # remove the name reference, assume that it is the data row based on @@opponentNames
            
            if lineArray==nil then
              break
            end
            lineArray.delete_at(0)              
            rowindex = counter-1
            lineArray.each_with_index  { |value,index| @@probabilityMatrix[rowindex][index]=value.to_f() }
          end
          counter = counter + 1
      end
      file.close
    rescue => err
      puts "Exception: #{err}"
      err
    end
  end

# solve the class conditional probabilities
  def self.computeClassConditionals
    numberOfNames = @@opponentNames.length
    @@classConditionalProbabilityMatrix = Array.new(numberOfNames) { Array.new(numberOfNames)}
    @@totalProbabilitiesForOpponent = Array.new(numberOfNames)
    @@totalProbabilitiesForOpponent.fill(0.0)
    
    #total all of the probabilities
    @@probabilityMatrix.each_with_index do |x, xi|
       x.each_with_index do |y, yi|
         systemIndex=xi
         opponentIndex=yi
         @@totalProbabilitiesForOpponent[opponentIndex]+=@@probabilityMatrix[systemIndex][opponentIndex]
       end  
    end    
  

  end
  
  
  # Generate a proportional assumption probability set
# this depends on specifying an opponent agent that
# we compute against.
def self.generateApriorProportionalByIndex(opponentIndex)
  
    if @@lastApriorIndex==opponentIndex then
      # don't bother creating the same apriori set again and again
      return
    end
  
    agentct = getAgentCount()
    @@apriori=Array.new(agentct)
    total=0
    for i in 0..(agentct-1)
       total = total +@@probabilityMatrix[i][opponentIndex]
    end
    
    for i in 0..(agentct-1)
       if total>0 then
         @@apriori[i] = @@probabilityMatrix[i][opponentIndex]/total       
       else
         @@apriori[i]=0.0
       end 
    end
    @@lastApriorIndex = opponentIndex
  end
  
  # get the specific apriori probability for this proportional assumption
def self.getAprioriByIndex(agentIndex)
  return @@apriori[agentIndex]
end  
  
# look up the class conditional probability in the 2D matrix.
def self.getClassConditionalProbabilityByIndex(agentIndex, opponentIndex)
     if @@classConditionalProbabilityMatrix==nil then
         computeClassConditionals()
       end
     
      return @@classConditionalProbabilityMatrix[agentIndex][opponentIndex]
end
  
  
# get a specific joint probability   
def self.getJointProbablityByIndex(objectIndex,opponentIndex)
 
  if @@jointProbabilityMatrix == nil then
    puts(" looking for a joint probability the does not exist")
    return 0
    
  end
  p= @@jointProbabilityMatrix[objectIndex][opponentIndex]
  return p
end  


def self.getTotalJointProbabilityByIndex(index)

  return @@totalJointProbabilities[index]
end

#create the joint probability matrix
def self.generateJointProbabilities
    numberOfNames = @@agentNames.length
    @@jointProbabilityMatrix = Array.new(numberOfNames) { Array.new(numberOfNames)}
    k=0
    #total all of the probabilities
    @@probabilityMatrix.each_with_index do |x, xi|
       x.each_with_index do |y, yi|
         startingProbability = y
         theApriori=@@apriori[xi]
         @@jointProbabilityMatrix[xi][yi]=startingProbability*theApriori
         
         
       end  
    end
        
    generateTotalJointProbabilities()
end

# This is called by the generateJointProbabilities call
# don't call it yourself
def self.generateTotalJointProbabilities
    numberOfNames = @@agentNames.length
    @@totalJointProbabilities = Array.new(numberOfNames)
    @@totalJointProbabilities.fill(0)
    k=0
    
    @@probabilityMatrix.each_with_index do |x, xi|
       x.each_with_index do |y, yi|
         p=@@jointProbabilityMatrix[xi][yi]
         @@totalJointProbabilities[yi]=@@totalJointProbabilities[yi]+p
         if yi==0 then 
           # puts("* "+p.to_s())
           k+=p
         end
       end  
    end
    
end
# fetch a specific posterior probability
def self.getPosteriorProbabilityByIndex(objectIndex,opponentIndex)
  if @@posteriorProbabilities == nil then
    puts(" looking for a posterior probability the does not exist")
    return 0
    
  end
  p= @@posteriorProbabilities[objectIndex][opponentIndex]
  return p
end

# create the set of all posterior probabilities
def self.generatePosteriorProbabilities
  numberOfNames=@@agentNames.length
  
   
  @@posteriorProbabilities = Array.new(numberOfNames){Array.new(numberOfNames)}
  @@jointProbabilityMatrix.each_with_index do |x, xi|
       x.each_with_index do |y, yi|
         jointProbability = y
         totalJoint=@@totalJointProbabilities[yi]
         @@posteriorProbabilities[xi][yi]=jointProbability/totalJoint
       
         
       end  
    end
        
  
end
  
# Solve the bayesian matrix
# Provide an assumed opponent name to create a semi-reliable 
# apriori scheme

def self.solve(assumedOpponentName)
  opponentIndex = @@agentNames.index(assumedOpponentName)
  if opponentIndex<0 then
    puts("error getting opponent index")
    return 
  end
  generateApriorProportionalByIndex(opponentIndex)
  generateJointProbabilities()
  generatePosteriorProbabilities()
end

  
  
  #end class
end