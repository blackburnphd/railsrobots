require_relative './ProbabilityLoader'

class ProbabilityTest
  
  ProbabilityLoader.loadProbabilities()
  nameOfAgent="Agent_0_0p1_0_0"
  nameOfOpponent="Agent_0_0p1_0_1"
  ProbabilityLoader.solve(nameOfOpponent)
  
  myIndex=  ProbabilityLoader.getAgentIndex(nameOfAgent)
  myOpponentIndex=  ProbabilityLoader.getAgentIndex(nameOfOpponent)
  
  puts("Index of "+nameOfAgent+" = "+myIndex.to_s())
  puts("Index of "+nameOfOpponent+" = "+myOpponentIndex.to_s())
  
  
  
  prob=ProbabilityLoader.getProbabilityWinByIndex(myIndex, myOpponentIndex)
  
  puts "Probability that "+nameOfAgent+" defeats "+nameOfOpponent+" = "+prob.to_s()
  
  
  for i in 0..5 
  
    puts( i.to_s()+":"+ProbabilityLoader.getAgentName(i))
    
  
  end
  
  # names appear to be ok, now lets check values
  
  for each_agent in 0..5
    s=""
    for each_opponent in 0..5
      p= ProbabilityLoader.getProbabilityWinByIndex(each_agent,each_opponent)
      s=s+p.to_s()+","
    end
    puts(s)
  end
  
  # now work the apriori stuff
  
  
  puts("")
  puts("APRIORI")
  puts("")
  
  for each_apriori in 0..5 
    apriori = ProbabilityLoader.getAprioriByIndex(each_apriori)
    s = ProbabilityLoader.getAgentName(each_apriori) 
    puts(s+" = "+apriori.to_s()) 
  end
  
  # solve joint probabilities
 
  puts("")
  puts("Joint Probabilities")
  puts("")
   
  for each_agent in 0..5
    s=""
    for each_opponent in 0..5
      p= ProbabilityLoader.getJointProbablityByIndex(each_agent,each_opponent)
      s=s+p.to_s()+","
    end
    puts(s)
  end
  
  # total joint probabilities
  puts("")
  puts("Total Joint Probabilities")
  puts("")
  
  for each_agent in 0..5
    p=ProbabilityLoader.getTotalJointProbabilityByIndex(each_agent)
    s=s+p.to_s()+","
  end
  
  puts(s)
  
   # solve posterior probabilities
 
  puts("")
  puts("Posterior Probabilities")
  puts("")
    
  for each_agent in 0..5
    s=""
    for each_opponent in 0..5
      p= ProbabilityLoader.getPosteriorProbabilityByIndex(each_agent,each_opponent)
      s=s+p.to_s()+","
    end
    puts(s)
  end
  
    # Pick the best system to defeat your opponent 
    puts()
    puts("Best system to defeat "+nameOfOpponent)
    puts()
  
    bestIndex= ProbabilityLoader.pickBestIndex(myOpponentIndex)
    puts("best index ="+bestIndex.to_s())
    puts("best strategy name= "+ProbabilityLoader.getAgentName(bestIndex))
    
end