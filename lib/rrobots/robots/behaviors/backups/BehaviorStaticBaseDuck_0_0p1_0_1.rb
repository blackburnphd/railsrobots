
require_relative 'behaviorBase'

class BehaviorStaticBaseDuck_0_0p1_0_1 < BehaviorBase
  
 def initialize
    super()
    @fire_power = 0.1
    @gun_direction = 1
    @lost_level = 3
    @turn_rate = 30
    @lost = 3
    @level2 = 0
    @level1 = 0
    @level0 = 0
    @level3_rate = 30
    @level2_rate = 15
    @level1_rate = 5
    @level0_rate = 1
    
    @max_fire_power = 3
    @min_fire_power = 0.1
    @max_power_distance = 2260
    @min_power_distance = 10
    @slope = -(@max_fire_power - @min_fire_power)/(@max_power_distance - @min_power_distance)
    
    @current_direction = 0
    @desired_heading = 0
    @circle_direction = :counter_clockwise #options are ":counter_clockwise" or ":clockwise"
                
    @shotLast = 0
    
    @firing_strategy = 0
    @aiming_strategy = 0
    @movement_strategy = 1
    
  end
  
  
  
# ====================  Implements behaviors here  ======================  
 def tick events

    detect = !(events['robot_scanned'].empty?)

# firing code
          
    case @firing_strategy
          
      when 0 # use default fire power
            
        if detect then
         @fire =@fire_power 
         
        end
      
      when 1 # use adaptive fire power based on distance to target
        
        if detect
          
          @dist = events['robot_scanned'][0][0]
          mod_fire_power = @slope*@dist + @max_fire_power
          
          @say= "#{mod_fire_power}"
          
          @fire= mod_fire_power
          
        else
          
          @fire= 0.1
        
        end
        
      when 2 # no firing - null case

        if !detect
          @fire=1
          @shotLast =1
        else
          if @shotLast == 1
            @fire= 1
          else
            @fire= 0
            @shotLast = 0
          end
        end
        
    end      
            
# aiming code
    
    case @aiming_strategy
          
      when 0 # Javier's aiming code
           
        if !detect && @lost == 2 && @level2 >= 3
          @lost = 3
          @level2 = @level1 = @level0 = 0
          @turn_rate = @level3_rate
        elsif !detect && @lost == 1 && @level1 >= 4
          @lost = 2
          @level2 = @level1 = @level0 = 0
          @turn_rate = @level2_rate
        elsif !detect && @lost == 0 && @level0 >= 6
          @lost = 1
          @level2 = @level1 = @level0 = 0
          @turn_rate = @level1_rate   
        elsif detect && @lost == 3
          @lost = 2
          @level2 = @level1 = @level0 = 0
          @level2 = @level2 + 1
          @turn_rate = @level2_rate
          @gun_direction = @gun_direction * (-1)
        elsif detect && @lost == 2
          @lost = 1
          @level2 = @level1 = @level0 = 0
          @level1 = @level1 + 1
          @turn_rate = @level1_rate
          @gun_direction = @gun_direction * (-1)
        elsif detect && @lost == 1
          @lost = 0
          @level2 = @level1 = @level0 = 0
          @level0 = @level0 + 1
          @turn_rate = @level0_rate
          @gun_direction = @gun_direction * (-1)
        elsif detect && @lost == 0
          @gun_direction = @gun_direction * (-1)
        else
          @level2 = (@level2 + 1) if @lost == 2
          @level1 = (@level1 + 1) if @lost == 1
          @level0 = (@level0 + 1) if @lost == 0
        end
    
        @turn_gun= (@turn_rate * @gun_direction)
     
    when 1 # Staticv2_2's aiming
      
      if !detect
        @turn_gun =-10
        @shotLast =1
      else
        if @shotLast == 1
          @turn_gun= 15
        else
          @turn_gun =5
          @shotLast = 0
        end
      end
      @turn_radar= 0
       
    end
    
# movement code
        
    case @movement_strategy
      
      when 0 # circular motion without evasion
        
        doTurn(4)
        accelerate(1)   
           
      when 1 # circular motion with evasion
      
        @last_hit = time unless events['got_hit'].empty? 
        if @last_hit && time - @last_hit < 60
          doTurn(30)
          accelerate(5)
        else
          doTurn(5)
          accelerate(-1) 
        end
      
      when 2 # stand still
      
      when 3 # boundary tracker
        
          # Set the initial current_direciton based on whichever direction we start pointing
          @current_direction = heading / 90 if time == 0
          
          update_current_direction()
          @desired_heading = @current_direction * 90
          
          # Turn towards our desired_heading
          turn_towards(@desired_heading)
          
          # Always move forward
          accelerate(5)
         
      when 4 # Static_v2_2's movement
        
        accelerate 1
      
        if !detect
          doTurn( 0)
          accelerate( 2)
          @shotLast = 1
        else
          if @shotLast == 1
            doTurn(-2)
          else
            doTurn( 10)
            @shotLast = 0
          end
          accelerate(1)
        end
                 
    end
        
  end 
  
  
  
  
  def update_current_direction()
    if @circle_direction == :counter_clockwise
      # If we've hit the wall we were heading towards, update the current_direction
      case @current_direction
      when 0 #heading towards the right wall
        @current_direction = 1 if x + size == battlefield_width
        
      when 1 #heading towards the top wall
        @current_direction = 2 if y == size
        
      when 2 #heading toward the left wall
        @current_direction = 3 if x == size
        
      when 3
        @current_direction = 0 if y + size == battlefield_height
      end
    else
      case @current_direction
      when 0 #heading towards the right wall
        @current_direction = 3 if x + size == battlefield_width
        
      when 1 #heading towards the top wall
        @current_direction = 0 if y == size
        
      when 2 #heading toward the left wall
        @current_direction = 1 if x == size
        
      when 3
        @current_direction = 2 if y + size == battlefield_height
      end
    end
  end
  
  
end