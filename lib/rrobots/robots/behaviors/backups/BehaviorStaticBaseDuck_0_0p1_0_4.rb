require_relative 'behaviorBase'

class BehaviorStaticBaseDuck_0_0p1_0_4 < BehaviorBase
  
  
  def initialize
    super()
    @fire_power = 0.1
    @gun_direction = 1
    @lost_level = 3
    @turn_rate = 30
    @lost = 3
    @level2 = 0
    @level1 = 0
    @level0 = 0
    @level3_rate = 30
    @level2_rate = 15
    @level1_rate = 5
    @level0_rate = 1
    
    @max_fire_power = 3
    @min_fire_power = 0.1
    @max_power_distance = 2260
    @min_power_distance = 10
    @slope = -(@max_fire_power - @min_fire_power)/(@max_power_distance - @min_power_distance)
    
    @current_direction = 0
    @desired_heading = 0
    @circle_direction = :counter_clockwise #options are ":counter_clockwise" or ":clockwise"
                
    @shotLast = 0
    
    @firing_strategy = 0
    @aiming_strategy = 0
    @movement_strategy = 4
    
  end
  
  # =====================================================
  
  
  
  
  
  
  
  # =====================================================


def update_current_direction()
    if @circle_direction == :counter_clockwise
      # If we've hit the wall we were heading towards, update the current_direction
      case @current_direction
      when 0 #heading towards the right wall
        @current_direction = 1 if x + size == battlefield_width
        
      when 1 #heading towards the top wall
        @current_direction = 2 if y == size
        
      when 2 #heading toward the left wall
        @current_direction = 3 if x == size
        
      when 3
        @current_direction = 0 if y + size == battlefield_height
      end
    else
      case @current_direction
      when 0 #heading towards the right wall
        @current_direction = 3 if x + size == battlefield_width
        
      when 1 #heading towards the top wall
        @current_direction = 0 if y == size
        
      when 2 #heading toward the left wall
        @current_direction = 1 if x == size
        
      when 3
        @current_direction = 2 if y + size == battlefield_height
      end
    end
  end


end