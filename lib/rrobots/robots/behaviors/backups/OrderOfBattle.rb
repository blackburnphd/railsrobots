
# this is a knowledge base of active classes.  Expect this to go away 
# as identity classifiers mature

class OrderOfBattle
  
  @oob = Array.new
  
  # remove all pairings
  def self.clear
    @oob.clear
  end
  
  # add init
  def add(classname)
    oob.push(classname)
  end

# search for the first class that is not myclassname  
  def self.getOpponentName(myclassname)
    @oob.each do |opponent|
      if opponent != myclassname then
        return opponent
      end
    end
    return nil
  end
  
end