

class BehaviorBase
  
  
  attr_accessor :time
  attr_accessor :fire
  attr_accessor :turn_gun
  attr_accessor :turn_radar
  attr_accessor :performAccelerate
  attr_accessor :performStop
  
  attr_accessor :x
  attr_accessor :y
  attr_accessor :size
  attr_accessor :battlefield_height
  attr_accessor :battlefield_width
  attr_accessor :energy
  attr_accessor :dead
  attr_accessor :turn
  attr_accessor :heading
  attr_accessor :turn_towards
  attr_accessor :say
  
  
  def initialize
    @time =0
    @fire =0
    @turn_gun=0
    @turn_radar=0
    @performAccelerate=0
    @performStop=false
    
    @x=0
    @y=0
    @size=0
    @battlefield_height=0
    @battlefield_width=0
    @energy=9999
    @turn=0
    @dead=false
    @heading=0
    @say
    
    
    
  end
  
  #restatement of turn_towards function within the robust_robot module
  #use this to avoid confusion as to whether to turn or turn_towards
  def turn_towards(new_heading, maximum_turn = 360)
    @turn=(angle_distance_with_max(heading, new_heading, maximum_turn))
  end
  
   # Calculate the difference between two angles, finding the shortest turn direction.
  def angle_distance(from_angle, to_angle)
    if to_angle > from_angle
      return angle_distance(to_angle, from_angle) * -1
    else
      if from_angle - to_angle > 180
        return (to_angle + 360) - from_angle
      else
        return (from_angle - to_angle) * -1
      end
    end
  end
  
  
  # Calculate the difference between two angles, finding the shortest turn direction,
  # and allowing the caller to specify that the result should be no greater than +/- maximum_angle
  def angle_distance_with_max(from_angle, to_angle, maximum_angle)
    angle = angle_distance(from_angle, to_angle)
    if angle > 0
      angle = [angle, maximum_angle].min()
    else
      angle = [angle, maximum_angle * -1].max()
    end  
  end
  
  
  def accelerate(a)
    @performAccelerate=a
  end  
  
  def doTurn(t)
    @turn = t
  end
  
  
  def clearall
    time=0
    fire=0
    turn_gun=0
    turn_radar=0
    performAccelerate=0
    performStop=false
    
    x=0
    y=0
    size=0
    battlefield_height=0
    battlefield_width=0
    energy=9999
    turn =0
    dead = false
    heading=0
    say=nil
  
  end
  

  def tick events
    
    
    
  end
end