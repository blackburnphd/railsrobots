require_relative 'behaviorBase'

class HealingAdaptiveAgentBehaviorSurrogate < BehaviorBase
  
  
  @surrogatename = "none"
  
  def initialize
      super()
      @fire_power = 0.1
      @firing_strategy = 0
      @aiming_strategy = 0
      @movement_strategy = 0
      @resupply_strategy =0
      
  end
  
  
  def makeACopy
    cp = HealingAdaptiveAgentBehaviorSurrogate.new
    cp.setStrategyByString(@surrogatename)
    return cp
  end
  
  
  def setStrategyByString(nameofclass)
    puts "strategy = " + nameofclass
    parts = nameofclass.split("_")
    @firing_strategy = parts[1].to_i
    
    subparts = parts[2].split("p");
    @fire_power = subparts[0].to_i 
    if subparts.length >1 then
      @fire_power = @fire_power +(subparts[1].to_i / 10.0)
    end
    @aiming_strategy = parts[3].to_i
    @movement_strategy = parts[4].to_i
    @resupply_strategy = parts[5].to_i
    @surrogatename = nameofclass
  end
  
  def tick events
    super(events)
    
  end
  
end