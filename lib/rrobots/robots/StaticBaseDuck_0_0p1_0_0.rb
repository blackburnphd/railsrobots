require_relative '../robust_robot'
require_relative './StaticBasis'



class StaticBaseDuck_0_0p1_0_0 < StaticBasis
  include RobustRobot

  def initialize
    super()
    @firing_strategy = 0
    @aiming_strategy = 0
    @movement_strategy = 0
    @fire_power = 0.1
    
    # remember
    # 3 - shoot the helicopter
    # 2 - shoot the cargo
    # 1 - GET the cargo and reusupply
    # 0 - Ignore cargo and helicopter, just fight the opponent
    
    @resupply_strategy = 1
  
  
  end
  
  
  
end
