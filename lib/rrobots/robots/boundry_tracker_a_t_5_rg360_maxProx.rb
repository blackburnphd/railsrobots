require_relative 'BoundaryTracker_Base'

class Boundry_Tracker_A_T_5_rg360_maxProx < BoundaryTracker_Base
  def initialize
    super
    @circle_direction = :counter_clockwise
    @surrogate_type = "BoundaryTracker_Surrogate"
  end
  
  def handle_surrogate_response(response)
    if response == "SWITCH"
      # Reverse turning direction
      if @circle_direction == :counter_clockwise
        @circle_direction = :clockwise
      else
        @circle_direction = :counter_clockwise
      end
      
      # Reverse movement direction
      @current_direction = (@current_direction + 2) % 4
    end
  end
end