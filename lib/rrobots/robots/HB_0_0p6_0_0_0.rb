require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_0_0p6_0_0_0  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 0
    @fire_power = 0.6
    @aiming_strategy = 0
    @movement_strategy = 0
    @resupply_strategy = 0
  end
end
