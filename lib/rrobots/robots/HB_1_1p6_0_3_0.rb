require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_1p6_0_3_0  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 1.6
    @aiming_strategy = 0
    @movement_strategy = 3
    @resupply_strategy = 0
  end
end
