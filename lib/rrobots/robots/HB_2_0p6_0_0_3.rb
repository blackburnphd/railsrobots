require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_2_0p6_0_0_3  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 2
    @fire_power = 0.6
    @aiming_strategy = 0
    @movement_strategy = 0
    @resupply_strategy = 3
  end
end
