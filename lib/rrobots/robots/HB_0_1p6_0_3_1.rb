require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_0_1p6_0_3_1  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 0
    @fire_power = 1.6
    @aiming_strategy = 0
    @movement_strategy = 3
    @resupply_strategy = 1
  end
end
