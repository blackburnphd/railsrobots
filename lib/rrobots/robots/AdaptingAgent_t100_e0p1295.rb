require_relative 'AdaptingAgent'

class AdaptingAgent_t100_e0p1295 < AdaptingAgent

def initialize

    super()
    
    # do not adapt before this time
    @adaptAtTime=100
    
    # probability that the opponent will be misclassified (1.0 - 0.8705)
    @probabilityClassifierError = 0.1295
end

end
