require_relative 'AdaptingAgent'

class AdaptingAgent_t2000_e0p05 < AdaptingAgent

def initialize

    super()
    
    # do not adapt before this time
    @adaptAtTime=2000
    
    # probability that the opponent will be misclassified
    @probabilityClassifierError = 0.05
end

end
