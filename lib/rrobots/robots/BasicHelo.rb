require_relative '../robot'

class BasicHelo
  include Robot
  
  attr_reader :heloX
  attr_reader :heloY
  attr_reader :time_between_appearances
 
  attr_accessor :time
  attr_accessor :battlefield
  attr_reader :angle_radians


  attr_accessor :resX
  attr_accessor :resY
  attr_reader :isOnScreen
  attr_reader :run_away_count
  attr_accessor :damage



  def initialize
    @cargo_energy = 100
    
    #default battlefield resolution
    @resX=800
    @resY=800
    
    @heloX = 0
    @heloY = 0
    @time_between_appearances=500
    @next_appearance_time=5
    @helo_speed = 2
    @current_helo_speed=2
    @runaway_helo_speed=10
    @helo_hover_time = 100
    
    @isOnScreen=false
    @heloHitsAvailable=100
    @helo_is_dead=false
    @time = 0
    @battlefield =nil
    
    @entry_position = Array.new(2)
    @exit_position = Array.new(2)
    @delivery_position = Array.new(2)
    
    @currentFlightState=0
    @has_achieved_delivery=false
    @has_performed_delivery=false
    @angle_radians =0
    
    @damage =0;
    @runaway_damage_threshold=5
    @run_away_count=0
  end
  
  
  def executeDelivery
    #puts "executing delivery"
  end
  
  #effectively this is a leave map with a different message
  def runAway
    
   
    @current_helo_speed= @runaway_helo_speed

    
    @has_achieved_delivery =true
   
    #puts "RUNNING AWAY, next appearance time ="+@next_appearance_time.to_s + " current time = " + @time.to_s + " damage =" + @damage.to_s 
    
  end
  
  def leaveMap

    @isOnScreen = false
    @next_appearance_time = @time + @time_between_appearances
    puts "LEAVING MAP, next appearance time ="+@next_appearance_time.to_s + " current time = "+@time.to_s
  end
  
  def enterMap
    @damage=0
    @current_helo_speed = @helo_speed
    #puts "ENTERING MAP at time="+@time.to_s
    @has_achieved_delivery=false
    @isOnScreen = true
    @heloX = @entry_position[0]
    @heloY = @entry_position[1]
  end
  
  def move
    
    if @isOnScreen==false then
      return
    end
    
    if @has_achieved_delivery==false then
      nextPoint = @delivery_position
    else
      nextPoint = @exit_position
    end
    # now we need to get the vector from the current point to the next point
    v = Array.new(2)
    v[0]=nextPoint[0]-@heloX
    v[1]=nextPoint[1]-@heloY
    length_of_vector = Math.sqrt( (v[0]*v[0] )+(v[1]*v[1]))
    
    
    # puts "moving distance of " + length_of_vector.to_s+" x="+@heloX.to_s+" y="+@heloY.to_s
    
    if length_of_vector > @current_helo_speed then
      v[0]=v[0]/length_of_vector
      v[1]=v[1]/length_of_vector
      v[0]=v[0]*@current_helo_speed
      v[1]=v[1]*@current_helo_speed
      @angle_radians = Math.atan2(v[1],v[0]) + (Math::PI/2)
      
      
      
      
    else
      #puts "I am very close to the end point "+length_of_vector.to_s
      # we have arrived, change to the next point
        if @has_achieved_delivery == false then
          @has_achieved_delivery = true
          #puts "achieved Delivery!"
          #  puts "Arrived at cargo location"
          @battlefield.addResupplyLocation(@heloX,@heloY,@cargo_energy)
        else
          leaveMap
        end
          
      
    end
    
    @heloX = v[0]+@heloX
    @heloY = v[1]+@heloY
    
    #puts "current position ="+ @heloX.to_s+","+@heloY.to_s
    
  end
  
  
  
  def generateFlightPlan
    #first determine which side to start from
    startwall = Random.rand(4)
    exitwall = (startwall +2) % 4
    
    
    
    #puts "start wall = "+startwall.to_s+" endwall = "+exitwall.to_s
    
    # arrive on the north wall
    if startwall==0
      @entry_position[0]=Random.rand(@resX)
      @entry_position[1]=0
      
      @exit_position[0]=Random.rand(@resX)
      @exit_position[1]=@resY
    end
    
    # arrive on the south wall
    if startwall==2
      @entry_position[0]=Random.rand(@resX)
      @entry_position[1]=@resY
      
      @exit_position[0]=Random.rand(@resX)
      @exit_position[1]=0
    end    
    
        # arrive on the east wall
    if startwall==1
      @entry_position[0]=@resX
      @entry_position[1]=Random.rand(@resY)
      @exit_position[0]=0
      @exit_position[1]=Random.rand(@resY)
    end  

     #arrive on the west wall
    if startwall==3 then
      @entry_position[0]=0
      @entry_position[1]=Random.rand(@resY)
      @exit_position[0]=@resX
      @exit_position[1]=Random.rand(@resY)
    
    end
    # now choose a delivery position
    @delivery_position[0]=Random.rand(@resX)
    @delivery_position[1]=Random.rand(@resY)
    
    
   # puts "start position ="+ @entry_position[0].to_s+","+@entry_position[1].to_s
    
    #puts "delivery position ="+ @delivery_position[0].to_s+","+@delivery_position[1].to_s
    
    #puts "exit position ="+ @exit_position[0].to_s+","+@exit_position[1].to_s
    
  end
  
  

  # may need this later...
  def moveTo(toX,toY)
    
  end
  
  def takeHit(dmg)
    if @isOnScreen == false then
      puts "I AM TAKING DAMAGE BUT I AM NOT ON SCREEN"
    end
    
    @damage = @damage + dmg
    if @damage >= @runaway_damage_threshold then
      runAway
    end
  end
    



  def tick(events)
    @time = events[:mytime]
    if @next_appearance_time >= @time then
      if @isOnScreen ==false then
    
        
        if @time >= @next_appearance_time then
          generateFlightPlan
          enterMap
        end
       # @next_appearance_time = @time + @time_between_appearances
       #    puts "next appearance time ="+@next_appearance_time.to_s
      end
    end
    
    if @isOnScreen == true then
      move
     
    end
  end
end