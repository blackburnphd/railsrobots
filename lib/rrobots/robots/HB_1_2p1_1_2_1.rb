require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_2p1_1_2_1  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 2.1
    @aiming_strategy = 1
    @movement_strategy = 2
    @resupply_strategy = 1
  end
end
