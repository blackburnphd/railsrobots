require_relative 'AdaptingAgent'

class AdaptingAgent_t3000_e0p077 < AdaptingAgent

def initialize

    super()
    
    # do not adapt before this time
    @adaptAtTime=3000
    
    # probability that the opponent will be misclassified
    @probabilityClassifierError = 0.0777
end

end
