require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_2p1_0_2_2  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 2.1
    @aiming_strategy = 0
    @movement_strategy = 2
    @resupply_strategy = 2
  end
end
