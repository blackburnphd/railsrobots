require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_0p6_1_0_1  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 0.6
    @aiming_strategy = 1
    @movement_strategy = 0
    @resupply_strategy = 1
  end
end
