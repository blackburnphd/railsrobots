
require_relative '../robot'

class Static_v4_6
   include Robot

  def tick events
         
    accelerate  1
    if events['robot_scanned'].empty?
      fire 5.0e-001
      turn 0
      turn_gun -30
      accelerate  1*3
      @shotLast = 1
      say "Scanned"
    else
      if @shotLast ==1 
        turn -3
        turn_gun 30/2
        fire 5.0e-001
        say "One after scan"
      else
        turn_gun 30
        turn 5
        fire 0
        @shotLast = 0
        say "Looking"
      end
      accelerate  1
    end
    turn_radar 0
     
     
     
     
  end
end
