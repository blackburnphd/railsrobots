require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_1p1_1_3_2  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 1.1
    @aiming_strategy = 1
    @movement_strategy = 3
    @resupply_strategy = 2
  end
end
