require_relative 'AdaptingAgent'

class AdaptingAgent_t2000_e0p15 < AdaptingAgent

def initialize

    super()
    
    # do not adapt before this time
    @adaptAtTime=2000
    
    # probability that the opponent will be misclassified
    @probabilityClassifierError = 0.15
end

end
