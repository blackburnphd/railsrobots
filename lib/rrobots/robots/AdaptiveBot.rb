require_relative '../robot'
require_relative '../utils/vector2D'
require_relative '../utils/timestampedVector2D'

class AdaptiveBot
 include Robot

 EDGEWIDTH=200

    Pi = 3.141592653589793
    
  def initialize
    super
   
    @lastX=0
    @lastY=0
    @lastSector= 0
    @nextGoal = Vector2D.new
    @nextGoal.set(rand(300)+EDGEWIDTH,rand(300)+EDGEWIDTH)
    @lastDistanceToGoal=9999
    @sameDistanceIterations=0
    @radarRotationSpeed=1
    @radarPings = Array.new
    @currentRadarAngle_deg=0
    @lastRadarAngle_deg =0
    
   
    
    @currentlyTurning = false
    @myCurrentPosition = Vector2D.new
    
    @assumedTarget = TimestampedVector2D.new
    
    puts " initial next goal = "+@nextGoal.stringify
    
  end
  
  # translate from a screen coordinate to cartesian coordinates
  def fromScreenCoordinate(v)
    v2= v.copyMake()
    tempy = v2.getY()
    tempx = v2.getX()
    tempy = @battlefield_height - tempy
    v2.set(tempx,tempy)
    return v2
  end
  
  
  def getBottomMost
    bottommost = @battlefield_height - EDGEWIDTH
    return bottommost
  end
  
  def getLeftMost
    leftmost = @battlefield_width - EDGEWIDTH
    return leftmost
  end
  
  # translate the current position into a vector by recycling the myCurrentPosition variable
  def getCurrentPositionAsVector
    @myCurrentPosition.set(@x,@y)
    return @myCurrentPosition
  end
  
  
  def calculateNextPoint
 
    currentPosition = getCurrentPositionAsVector()
   
    
    if @nextGoal==nil then
      puts "there is no next goal"
    end

    dist =@nextGoal.distance(currentPosition)
    if dist>5
      #puts "distance greater than 5 to next goal "+@nextGoal.stringify
      return
    end



    szX = @battlefield_width - ( EDGEWIDTH * 2)
    szY = @battlefield_height - ( EDGEWIDTH * 2)
    
    @nextGoal = Vector2D.new
  
    goalX = EDGEWIDTH + rand(szX)
    goalY = EDGEWIDTH + rand(szY)
    
    @nextGoal.set(goalX,  goalY)
   #   puts "new goal is "+@nextGoal.stringify
  
  end
  
  
  # calculate the angle to the next goal
  
  def calculateAngleToGoal
    
    if @nextGoal == nil then
   #   puts "no next goal"
      return @heading
    end  
    
    currentposition = Vector2D.new
    currentposition.set(@x,@battlefield_height- @y)
    mygoal =  Vector2D.new
    mygoal.set(@nextGoal.getX(), @battlefield_height- @nextGoal.getY())
    
    

    angle= Math.atan2(mygoal.getY()- currentposition.getY(), mygoal.getX() - currentposition.getX())
    angle = angle * (180 / Pi)
   
    
    #angle = currentPosition.directionToDeg(getNextGoalAbsCoordinate())
    return angle
  end
  

  # break up gameboard into 9 sections

  # 1 = upper left
  # 2 = upper middle
  # 3 = upper right
  # 4 = middle left
  # 5 = middle middle
  # 6 = middle right
  # 4 = lower left
  # 5 = lower middle
  # 6 = lower right
  
  def determineSector
    
    lmr = 2
    tmb = 3
    

    
    if @x<EDGEWIDTH then
      lmr=1
    end
    
    if @x> getLeftMost() then
      lmr = 3
    end

    if @y < EDGEWIDTH then
      tmb =0
    end
    
    if @y > getBottomMost() then
      tmb = 6
    end
    
    return tmb + lmr
    
  end
  

  
 def amICloseToEdge
   
   
   
   if @y<30 then
      puts "close bottom"
  #    return true
   end
    
   if @y > ( @battlefield_height-30 ) then
      puts "close top"
   #   return true
   end
      
   if @x < 30 then
      puts "close left"
   #   return true
    end
    
    if @x > ( @battlefield_width-30 ) then
      puts "close right"
    #  return true
    end  
 
  end


  # calculate a turn radius and direction, then perform as much as the turn as possible  
  def performTurn

    currentPosition = getCurrentPositionAsVector()
    puts "current position asdfasdfasdfasdf "+currentPosition.stringify
    angleNeeded = calculateAngleToGoal()
    angleNeeded = angleNeeded + 180

    
    puts "angle needed ="+angleNeeded.to_s()
    
    
    deltaAngle =  angleNeeded -@heading
    
   # if deltaAngle > 180 then
   #   deltaAngle = deltaAngle - 180
   #   deltaAngle = deltaAngle * (-1)
   # end
    
    
    
    if (deltaAngle * deltaAngle) <2
      @currentlyTurning = false
      return
    end
  
    @currentlyTurning=true
   
    if deltaAngle>10
      deltaAngle = 10
    end
    
    if deltaAngle < -10 then
      deltaAngle = -10
    end
     
    turn(deltaAngle)
  end
  
  
  def tick(events)
     
       
     @lastX=@x
     @lastY=@y


     here = getCurrentPositionAsVector()
     
     if @nextGoal == nil then
      
        calculateNextPoint()
     end
     
     
    
     dist = here.distance(@nextGoal)
  
     angle = calculateAngleToGoal()
      
     if dist < 20 then
       # we have sort-of arrived at our goal
       
     end 
      
      
     if @heading == nil then
       puts "heading = nil"
     end
    
     puts "location "+here.stringify+" goal "+@nextGoal.stringify+" distance = "+dist.to_s() +" angle = "+angle.to_s() + " heading = "+@heading.to_s()

    
     
       
     @lastSector = determineSector()
     
     if @lastSector != 5 then
      
         if @currentlyTurning == false then
           calculateNextPoint()
           @currentlyTurning = true
         end
         performTurn()
         here = getCurrentPositionAsVector()
         dist = @nextGoal.distance(here)
         if dist == @lastDistanceToGoal then
           @sameDistanceIterations = @sameDistanceIterations +1
         end    
         @lastDistanceToGoal = dist
        
         if @sameDistanceIterations>3 then
          accelerate(1)
         end
     else
       accelerate(1)
       @sameDistanceIterations = 0
     end
     

     processRadarPings()
     
   #  shootAtAssumedTarget()
 
     
  end


  def processRadarPings
    
    @lastRadarAngle_deg = @currentRadarAngle_deg
    turn_radar @radarRotationSpeed
    @currentRadarAngle_deg = @currentRadarAngle_deg+@radarRotationSpeed
    @haveTarget = false    
    if event = events['robot_scanned'].pop
     
        distanceToTarget = event.first
       
        
        #assume a mid point radar angle
        # we can get craftier later
        radarAngle_rad =  (180.0/Pi)* ( (@lastRadarAngle_deg/2) +   (@currentRadarAngle_deg/2.0) )
        targetX = @x + Math.cos(radarAngle_rad)*distanceToTarget
        targetY = @y - Math.sin(radarAngle_rad)*distanceToTarget
        
      
        
        @assumedTarget.set(targetX,targetY)
     
        @haveTarget=true
        @radarRotationSpeed = @radarRotationSpeed*(-1)
    end
  end
  
  def shootAtAssumedTarget
    if @haveTarget == true then
      
      mycurrentposition = getCurrentPositionAsVector()
      targetAngle mycurrentposition.directionToRadFlipScreenCoordDeg(@assumedTarget)
      
      
      
      deltaGun = targetAngle - @gun_heading
     
      if deltaGun>180 then
        deltaGun = deltaGun -10
      end
      
      if deltaGun > 10 then
        deltaGun = 10
      end
      
      if deltaGun < -10 then
        deltaGun = -10
      end 
     
      
      newGunHeading = @gun_heading+deltaGun
      
      turn_gun newGunHeading
      
      if deltaGun<2 then
        fire 1.0e+000 unless events['robot_scanned'].empty?
      end
      
      
      
      
    end
  end

end