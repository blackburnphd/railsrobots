require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_1p1_0_4_2  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 1.1
    @aiming_strategy = 0
    @movement_strategy = 4
    @resupply_strategy = 2
  end
end
