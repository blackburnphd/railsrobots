require_relative 'AdaptingSelfHealingAgent'

class Adapting2_t100_e0p1295_t3000_e0p077_HB < AdaptingSelfHealingAgent

def initialize
  
  
# Adapting Agent Experiment
# time 1        prob      time 2       prob    Name
# 100           15%       3000         5%      Adapting2_t100_e0p15_t3000_e0p05_HB
# 500           15%       3000         5%      Adapting2_t500_e0p15_t3000_e0p05_HB
# 1000          15%       3000         5%      Adapting2_t1000_e0p15_t3000_e0p05_HB
# 1500          15%       3000         5%      Adapting2_t1500_e0p15_t3000_e0p05_HB
# 2000          15%       3000         5%      Adapting2_t2000_e0p15_t3000_e0p05_HB
# 2500          15%       3000         5%      Adapting2_t2500_e0p15_t3000_e0p05_HB
# 100           15%       1000         5%      Adapting2_t100_e0p15_t1000_e0p05_HB
# 500           15%       1000         5%      Adapting2_t500_e0p15_t3000_e0p05_HB
# 100           15%       2000         5%      Adapting2_t100_e0p15_t2000_e0p05_HB
# 500           15%       2000         5%      Adapting2_5100_e0p15_t2000_e0p05_HB
# 1000          15%       2000         5%      Adapting2_t1000_e0p15_t2000_e0p05_HB
# 1500          15%       2000         5%      Adapting2_t1500_e0p15_t2000_e0p05_HB


  
  
# Adapting2_t100_e0p1295_t3000_e0p077
puts "Initializing Adapting2_t100_e0p1295_t3000_e0p077_HB"
    super()    
    # do not adapt before this time
    @adaptAtTime=[100,3000]
    # probability that the opponent will be misclassified
    @probabilityClassifierError = [0.1295,0.0777]
end

end
