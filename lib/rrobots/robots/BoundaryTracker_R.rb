require_relative 'BoundaryTracker_Base'

class BoundaryTracker_R < BoundaryTracker_Base
  def initialize
    super
    @circle_direction = :clockwise
  end
end