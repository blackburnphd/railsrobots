require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_1_2p6_0_3_1  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 1
    @fire_power = 2.6
    @aiming_strategy = 0
    @movement_strategy = 3
    @resupply_strategy = 1
  end
end
