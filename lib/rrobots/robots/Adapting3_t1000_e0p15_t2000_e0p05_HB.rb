require_relative 'AdaptingSelfHealingAgentTwoBranch'

class Adapting3_t1000_e0p15_t2000_e0p05_HB < AdaptingSelfHealingAgentTwoBranch

def initialize
  

  
  
    puts "Initializing Adapting2_t1000_e0p15_t2000_e0p05_HB"
    super()    
    # do not adapt before this time
    @adaptAtTime=[1000,2000]
    # probability that the opponent will be misclassified
    @probabilityClassifierError = [0.15,0.05]
end

end
