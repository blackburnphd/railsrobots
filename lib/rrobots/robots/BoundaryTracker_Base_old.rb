require_relative '../RobustRobot'
require 'socket'

class BoundaryTracker_Base
  include RobustRobot

  def initialize
    @current_direction = 0
    @desired_heading = 0
  end
  
  def tick(events)
    
    # Set the initial current_direciton based on whichever direction we start pointing
    @current_direction = heading / 90 if time == 0
    
    # Make the radar "wobble" back and forth because it can only detect other bots via rotation
    if time == 0
      turn_radar(5)
    else
      if time % 2 == 0
        turn_radar(10)
      else
        turn_radar(-10)
      end
      # Wiggle wiggle wiggle wiggle wiggle, yeah.
    end
   
    # Point the gun towards the "inside" of the circle we are moving in, then hold it there.
    if @circle_direction == :counter_clockwise
      turn_gun_towards(90.relative_to(heading))
    else
      turn_gun_towards(270.relative_to(heading))
    end
    
    update_current_direction()
    @desired_heading = @current_direction * 90
    
    # Turn towards our desired_heading
    turn_towards(@desired_heading)
    
    # Always move forward
    accelerate(5)
    
    # Shoot if you saw something
    fire(2) if !events['robot_scanned'].empty?
    
    if !events['got_hit'].empty? && @surrogate_type != nil && !@surrogate_type.empty?
      send_to_surrogate("HIT " + time.to_s())
    end
  end
  
  
  def update_current_direction()
    if @circle_direction == :counter_clockwise
      # If we've hit the wall we were heading towards, update the current_direction
      case @current_direction
      when 0 #heading towards the right wall
        @current_direction = 1 if x + size == battlefield_width
        
      when 1 #heading towards the top wall
        @current_direction = 2 if y == size
        
      when 2 #heading toward the left wall
        @current_direction = 3 if x == size
        
      when 3
        @current_direction = 0 if y + size == battlefield_height
      end
    else
      case @current_direction
      when 0 #heading towards the right wall
        @current_direction = 3 if x + size == battlefield_width
        
      when 1 #heading towards the top wall
        @current_direction = 0 if y == size
        
      when 2 #heading toward the left wall
        @current_direction = 1 if x == size
        
      when 3
        @current_direction = 2 if y + size == battlefield_height
      end
    end
  end
end
