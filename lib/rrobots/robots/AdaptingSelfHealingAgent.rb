require_relative '../robust_robot'
require_relative './behaviors/OrderOfBattle'
require_relative './behaviors/behaviorBase'
require_relative './behaviors/ProbabilityLoader'

# BP 2014-12-19
require_relative './behaviors/HealingAdaptiveAgentBehaviorSurrogate'

class AdaptingSelfHealingAgent
 include RobustRobot

  @nameOfOpponent=nil
  @opponentIndex=(-1)
  
  # remember when an enemy fires at the cargo or helo so we can guess his healing strategy
  @enemy_shots_on_cargo = 0
  @enemy_shots_on_helo = 0
  

# this is the default starting duck.  
# also sets the default behavior agent name
# if the @randomizeInitialBehavior flag is set
# to true, then it will randomly choose a starting 
# behavior.  Otherwise it defaults to Agent_0_0p1_0_0.
  def decideHowToInitialize
      @behaviorAgentName="Agent_0_0p1_0_0_0"
    
    if @randomizeInitialBehavior == true then
      @behaviorAgentName = getRandomBehavior()
      puts("Random starting behavior = "+@behaviorAgentName)
    end
    return @behaviorAgentName
  end


def getRandomBehavior
      len = @behaviors.length
      len = len - 1
      index = rand(0..len)
      randomBehavior = @behaviors.keys[index]
      return randomBehavior
end


def cargoHitObserved
    @enemy_shots_on_cargo = @enemy_shots_on_cargo +1
 
end

def heloHitObserved
  @enemy_shots_on_helo = @enemy_shots_on_helo + 1
end


# makes sure that the agent names are aligned to the right format
def ensureNamingCorrect(agentName)
  parts = agentName.split("_")
  if parts[2].length ==1 then
    parts[2]=parts[2]+"p0"
  end
  answer=""
  first = false
  parts.each do |item|
     place=""
     if first then
       place = "_"
     end
     answer+=place+item
     first = true
  end
  return answer
end

def changeClassNaming(cs)
  if cs==nil then
    puts("Received a nil cs")
    return
  end 
  if cs.start_with?("StaticBaseDuck") then
     
     return cs.sub("StaticBaseDuck","Agent")
  end
  return cs
end


  def initialize
    puts "initializing self healing agent"
 # randomize the selection of the initial behavior
    @randomizeInitialBehavior=true
    
    # do not adapt before this time
    @adaptAtTime=[0,1]

    # probability that the opponent will be misclassified
    @probabilityClassifierError = [1.00,1.00]
    
    # this flag is for internal use only.  It indicates that this adaptive agent has
    # completed it's behavior change.
    
    @alreadyChangedBehavior=[false,false]
    

     puts ">preparing to load probabilities"
     ProbabilityLoader.useSelfHealingProbabilities()
     ProbabilityLoader.loadProbabilities()  
     @behaviors={}
     puts ">probabilities loaded"

     agentcount = ProbabilityLoader.getAgentCount
     # populate behaviors hash with surrogate classes
     puts ">Iterate over each agent name"
     for i in 0..(agentcount-1)
       newAgentName = ProbabilityLoader.getAgentName(i)
       #HealingAdaptiveAgentBehaviorSurrogate
       surrogate = HealingAdaptiveAgentBehaviorSurrogate.new
       surrogate.setStrategyByString(newAgentName)
       @behaviors[newAgentName] = surrogate
     end
     
     
    
#     
     
     
     puts ">Decide how to initialize"
#   
     @loadThisBot = decideHowToInitialize
     mysurrogate = @behaviors[@loadThisBot]
     puts ">Make a copy of " + @loadThisBot 
     @myduck = mysurrogate.makeACopy
  end
  

  




  # execute the tick method the selected behavior
 def tick events
   
     @myduck.clearall()
     #initialize states
     @myduck.fire=0
     @myduck.time = time
     @myduck.x=@x
     @myduck.y=@y
     @myduck.size=@size
     @myduck.battlefield_height=@battlefield_height
     @myduck.battlefield_width=@battlefield_width
     @myduck.energy = @energy
     @myduck.dead = @dead
     @myduck.heading = @heading
     
     
     
     
     
     @myduck.tick(events)
    
     opposing_force= OrderOfBattle.getOpponentName(self.class.name)
     if opposing_force==nil then
       puts("No opposing class")
     else
       
       
       opposing_force = changeClassNaming(opposing_force)
       countIndex = [0,1]
       countIndex.each do |myCountIndex| 
       
                   if ((@nameOfOpponent==nil) && (@alreadyChangedBehavior[myCountIndex]==false)  ) then
                       myIndex=  ProbabilityLoader.getAgentIndex(opposing_force)
                       if myIndex<(-1) then
                         if opposing_force!=nil then
                           puts("No candidate system identified!  Make sure that "+opposing_force+" is in the data set!")
                         end
                       else
                           if @adaptAtTime[myCountIndex] < @time then
                               @nameOfOpponent = opposing_force
                               error_check = rand()
                               if error_check < @probabilityClassifierError[myCountIndex] then
                                 @nameOfOpponent =getRandomBehavior()
                               end
                               
                               @opponentIndex=  ProbabilityLoader.getAgentIndex(opposing_force)
                               if @opponentIndex<0 then
                                 puts("I can not find this enemy-"+opposing_force)
                               end
                               ProbabilityLoader.solve(@nameOfOpponent)
                               bestIndex= ProbabilityLoader.pickBestIndex(@opponentIndex)
                               @behaviorAgentName=ProbabilityLoader.getAgentName(bestIndex)
                               @behaviorAgentName = ensureNamingCorrect(@behaviorAgentName)
                               puts("behavior "+myCountIndex.to_s+" = "+@behaviorAgentName)
                               # now change myduck to adapt to the behavior
                               @myduck = @behaviors[@behaviorAgentName]
                               if @myduck==nil then 
                                 puts("CRITICAL FAIL NO DUCK!!!")
                               end
                               @myduck.fire=0
                               @myduck.time = time
                               @myduck.x=@x
                               @myduck.y=@y
                               @myduck.size=@size
                               @myduck.battlefield_height=@battlefield_height
                               @myduck.battlefield_width=@battlefield_width
                               @myduck.energy = @energy
                               @myduck.dead = @dead
                               @myduck.heading = @heading
                               @myduck.tick(events)
                               @alreadyChangedBehavior[myCountIndex]=true
                            end
                       end
                    end
                 end
           # end do looop
           end     
       
    accelerate(@myduck.performAccelerate)
    fire(@myduck.fire)
    turn_gun(@myduck.turn_gun)
    turn(@myduck.turn)
    turn_radar(@myduck.turn_radar)
    if @myduck.say!=nil then
        say(@myduck.say)
    end
    if @myduck.performStop then
       stop
    end
   end
  
end