require_relative '../robust_robot'
require_relative '../utils/vector2D'
require_relative '../utils/timestampedVector2D'

# use this class to descend new static robots from
# This is the base class for all robots that heal

# robot names follow this pattern:
#  HB_<firing_stragegy>_<fire_power with 'p'>_<aiming_strategy>_<movement_strategy>_<resupply_strategy>


#  Strategies
#  firing_strategy [0,1,2]
#  fire_power [0.1, 0.6, 1.1, 1.6, 2.1, 2.6,1.0]
#  aiming_strategy [0,1]
#  movement_strategy [0,1,2,3,4]
#  resupply_strategy [0,1,2,3]





class StaticBasis
  include RobustRobot


  attr_accessor :resupplyX
  attr_accessor :resupplyY
  attr_accessor :resupplyName
  attr_accessor :resupplyEnergy
  
  attr_accessor :heloX
  attr_accessor :heloY
  attr_accessor :heloX_last
  attr_accessor :heloY_last
  attr_accessor :heloIsOnScreen
  

  def initialize
    
    
    
    
    @heloX = -999
    @heloY = -999
    @heloIsOnScreen = false
    # use the _last variables to estimate where to shoot
    @heloX_last = -999
    @heloY_last = -999
    
   
    @gun_direction = 1
    @lost_level = 3
    @turn_rate = 30
    @lost = 3
    @level2 = 0
    @level1 = 0
    @level0 = 0
    @level3_rate = 30
    @level2_rate = 15
    @level1_rate = 5
    @level0_rate = 1
    
    @max_fire_power = 3
    @min_fire_power = 0.1
    @max_power_distance = 2260
    @min_power_distance = 10
    @slope = -(@max_fire_power - @min_fire_power)/(@max_power_distance - @min_power_distance)
    
    @current_direction = 0
    @desired_heading = 0
    @circle_direction = :counter_clockwise #options are ":counter_clockwise" or ":clockwise"
                
    @shotLast = 0
    
    @firing_strategy = 0
    @aiming_strategy = 0
    @movement_strategy = 0
    @fire_power = 0.1
    @resupply_strategy =3
    
    @resupplyX = -999;
    @resupplyY = -999;
    @resupplyName = nil
    @resupplyEnergy = 0
    
    
    @cargoX=-999
    @cargoY=-999
    @distance_to_cargo=99999999
    @cargoName =nil
    @energy_at_cargo = 3
  end
  
  
  def tick events
    if @resupply_strategy != 0
      tick_resupply events
    else
      tick_battle events
    end
  end
  
  
  # Use this tick when there is a non-zero resupply strategy
  def tick_resupply events
  # case 0 means ignore all resupply activities
    # case 1 means attempt to get the supplies
    # case 2 means always shoot the supplies
    # case 3 means always shoot at helicopter    
    case @resupply_strategy
      when 0
        tick_battle(events)
      when 1
        # determine if there is a new cargo platform available
        if self.resupplyX != -999 then
          @cargoX = @resupplyX
          @cargoY = @resupplyY
          
        end
        
        if @cargoX == -999 then
          # there is no cargo
            tick_battle(events)
        else
          if @resupplyX != -999 then
            puts "resupplyX = "+@resupplyX.to_s
          end
          
          if @resupplyEnergy == 0 then
            if @cargoX == -999 then
              # we do not have any cargo to worry about
              puts "we have no cargoX"
              tick_battle(events)
              return
            end
          end
          
          # we have cargo to go get
       
          myangle = computeAngle(@x,@y,@cargoX,@cargoY)
         # puts "angle ="+myangle.to_s
          if myangle == -999 then
              @cargoX =@resupply_x
              @cargoY =@resupply_y
              @cargoName=@resupply_name
              tick_battle(events)
          else
              turn_towards(myangle)
              accelerate 1  
          end
            
          
            
        end
      when 2
       
         if @cargoX == -999 then
           tick_battle(events)
           return
         else
       #    puts "we have a resupply"
          # calculate necessary gun angle
          myangle = computeAngle(@x,@y,@cargoX*2,@cargoY*2)
          turn_gun_towards(myangle, maximum_turn = 30)
          delta_heading  = myangle - gun_heading;
          if delta_heading > 180 then
            delta_heading = delta_heading - 360
          end
          if delta_heading < -180 then
            delta_heading = delta_heading + 360
          end
          
        
          if delta_heading.abs < 2 then
            fire @energy_at_cargo
           
          end
        
         end
        
      when 3
        # Shoot the Helo
        if heloIsOnScreen == false then
           tick_battle(events)
           return
        else
          
          
            pt = estimateHeloFiringPoint()
          
            myangle = computeAngle(@x,@y,pt[0]*2,pt[1]*2)
            turn_gun_towards(myangle, maximum_turn = 30)
            delta_heading  = myangle - gun_heading;
            if delta_heading > 180 then
              delta_heading = delta_heading - 360
            end
            if delta_heading < -180 then
              delta_heading = delta_heading + 360
            end
            
            puts delta_heading.to_s
            if delta_heading.abs < 2 then
              fire @energy_at_cargo
              # puts "fire"
            end
          
        end
              
      else
        puts "Unknown resupply strategy = "+@resupply_strategy.to_s
    end
     
  end
  
  
  def tick_battle events

      detect = !(events['robot_scanned'].empty?)

# firing code
          
    case @firing_strategy
          
      when 0 # use default fire power
            
        fire @fire_power if detect
      
      when 1 # use adaptive fire power based on distance to target
        
        if detect
          
          @dist = events['robot_scanned'][0][0]
          mod_fire_power = @slope*@dist + @max_fire_power
          
          say "#{mod_fire_power}"
          
          fire mod_fire_power
          
        else
          
          fire 0.1
        
        end
        
      when 2 # no firing - null case

        if !detect
          fire 1
          @shotLast =1
        else
          if @shotLast == 1
            fire 1
          else
            fire 0
            @shotLast = 0
          end
        end
        
    end      
            
# aiming code
    
    case @aiming_strategy
          
      when 0 # Javier's aiming code
           
        if !detect && @lost == 2 && @level2 >= 3
          @lost = 3
          @level2 = @level1 = @level0 = 0
          @turn_rate = @level3_rate
        elsif !detect && @lost == 1 && @level1 >= 4
          @lost = 2
          @level2 = @level1 = @level0 = 0
          @turn_rate = @level2_rate
        elsif !detect && @lost == 0 && @level0 >= 6
          @lost = 1
          @level2 = @level1 = @level0 = 0
          @turn_rate = @level1_rate   
        elsif detect && @lost == 3
          @lost = 2
          @level2 = @level1 = @level0 = 0
          @level2 = @level2 + 1
          @turn_rate = @level2_rate
          @gun_direction = @gun_direction * (-1)
        elsif detect && @lost == 2
          @lost = 1
          @level2 = @level1 = @level0 = 0
          @level1 = @level1 + 1
          @turn_rate = @level1_rate
          @gun_direction = @gun_direction * (-1)
        elsif detect && @lost == 1
          @lost = 0
          @level2 = @level1 = @level0 = 0
          @level0 = @level0 + 1
          @turn_rate = @level0_rate
          @gun_direction = @gun_direction * (-1)
        elsif detect && @lost == 0
          @gun_direction = @gun_direction * (-1)
        else
          @level2 = (@level2 + 1) if @lost == 2
          @level1 = (@level1 + 1) if @lost == 1
          @level0 = (@level0 + 1) if @lost == 0
        end
    
        turn_gun (@turn_rate * @gun_direction)
     
    when 1 # Staticv2_2's aiming
      
      if !detect
        turn_gun -10
        @shotLast =1
      else
        if @shotLast == 1
          turn_gun 15
        else
          turn_gun 5
          @shotLast = 0
        end
      end
      turn_radar 0
       
    end
    
# movement code
        
    case @movement_strategy
      
      when 0 # circular motion without evasion
        
        turn(4)
        accelerate(1)   
           
      when 1 # circular motion with evasion
      
        @last_hit = time unless events['got_hit'].empty? 
        if @last_hit && time - @last_hit < 60
          turn(30)
          accelerate(5)
        else
          turn(5)
          accelerate(-1) 
        end
      
      when 2 # stand still
      
      when 3 # boundary tracker
        
          # Set the initial current_direciton based on whichever direction we start pointing
          @current_direction = heading / 90 if time == 0
          
          update_current_direction()
          @desired_heading = @current_direction * 90
          
          # Turn towards our desired_heading
          turn_towards(@desired_heading)
          
          # Always move forward
          accelerate(5)
         
      when 4 # Static_v2_2's movement
        
        accelerate 1
      
        if !detect
          turn 0
          accelerate 2
          @shotLast = 1
        else
          if @shotLast == 1
            turn (-2)
          else
            turn 10
            @shotLast = 0
          end
          accelerate 1
        end
                 
    end
        
  end

  def update_current_direction()
     if @circle_direction == :counter_clockwise
      # If we've hit the wall we were heading towards, update the current_direction
      case @current_direction
      when 0 #heading towards the right wall
        @current_direction = 1 if x + size == battlefield_width
        
      when 1 #heading towards the top wall
        @current_direction = 2 if y == size
        
      when 2 #heading toward the left wall
        @current_direction = 3 if x == size
        
      when 3
        @current_direction = 0 if y + size == battlefield_height
      end
    else
      case @current_direction
      when 0 #heading towards the right wall
        @current_direction = 3 if x + size == battlefield_width
        
      when 1 #heading towards the top wall
        @current_direction = 0 if y == size
        
      when 2 #heading toward the left wall
        @current_direction = 1 if x == size
        
      when 3
        @current_direction = 2 if y + size == battlefield_height
      end
    end
  end
  
  # use this function to compute the angle from the start poition to the end position  BP 2014-02-03
  # if the point is less than .5 away, return -999
  def computeAngle(startX, startY,nextX,nextY)
  
   #   puts "startX = " + startX.to_s 
   #   puts "  startY = " + startY.to_s
   #   puts "  nextX = " + nextX.to_s
   #   puts "  nextY = " + nextY.to_s
   #   puts "  speed = " + speed.to_s
    
    currentposition = Vector2D.new
    currentposition.set(startX,@battlefield_height- startY)
    mygoal =  Vector2D.new
    mygoal.set(nextX, @battlefield_height-nextY)
    
    myPi =3.141592
    
    
    dx = mygoal.getX() - currentposition.getX()
    dy = mygoal.getY()- currentposition.getY()
   # dy = dy * (-1)  # flip the y direction

    angle= Math.atan2(dy, dx)
    angle = angle * (180 / myPi)
    
   
    
    #angle = currentPosition.directionToDeg(getNextGoalAbsCoordinate())
    return angle
 
   end
   
   
   def resupplyEnergyAward(award)
     
      puts "received energy award of "+award.to_s
     
      @resupplyEnergy = award+@resupplyEnergy
      @energy = @energy + award
      if @energy > 100 then
        @energy = 100
      end
      
      # now that we have received an energy award, lets reset so that we can 
      # continue fighting
      
      @resupplyX = -999
      @resupplyY = -999
      @resupplyName = nil
      @cargoX=-999
      @cargoY=-999
      @distance_to_cargo=99999999
      @cargoName =nil
 
      
   end
   
   # called by the robot_runner when a new resupply cargo arrives on the board
   def resupplyArrives(rx,ry,rname)
    @resupplyX = rx*2
    @resupplyY = ry*2
    @resupplyName = rname
    
    @cargoX = rx
    @cargoY = ry
    @cargoName= rname
    puts "***********  SUCCESS **************"
   end
   
  

  def clearResupplyData
    puts "clearing resupply data"
    @resupplyX = -999
    @resupplyY = -999
    @resupplyName = nil
    @resupplyEnergy = 0
    
   
  end
       
  def doesResupplyExist
    if @resupplyX == -999 then
      return false
    end
    return true
  end
       
  def resupplyIsDestroyed(cargo_name)
    if @resupplyName == cargo_name then
      
       if @resupplyName == cargo_name then
          @cargoX = -999
          @cargoY = -999
          @cargoName = nil
          @distance_to_cargo = 99999999
         
        end
        clearResupplyData
    end
  end

  # this is where the battlefield notifies the robot about the
  # helo position.  When a new position arrives, we keep the 
  # old position so we can estimate a point to fire at.
  def updateHeloInformation(hx,hy,isonscr)
    @heloX_last = @heloX
    @heloY_last = @heloY
    @heloX = hx
    @heloY = hy
    @heloIsOnScreen = isonscr
    # puts "helo is at x="+hx.to_s+" y="+hy.to_s+" onscreen="+isonscr.to_s
  end
  
  # return an array for a 2D point to shoot at
  
  def estimateHeloFiringPoint
    vec = Array.new(2)
    vec[0] = @heloX - @heloX_last
    vec[1] = @heloY - @heloY_last
    # guess how far ahead to shoot
    r = rand() * 50
    vec[0]=(vec[0]*r) + @heloX
    vec[1]=  ( vec[1]*r ) + @heloY
    
    return vec
    
  end
  

     
end
