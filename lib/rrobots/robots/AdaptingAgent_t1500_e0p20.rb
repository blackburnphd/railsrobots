require_relative 'AdaptingAgent'

class AdaptingAgent_t1500_e0p20 < AdaptingAgent

def initialize

    super()
    
    # do not adapt before this time
    @adaptAtTime=1500
    
    # probability that the opponent will be misclassified
    @probabilityClassifierError = 0.20
end

end
