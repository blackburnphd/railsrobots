
require_relative '../robot'

class Static_v2_7
   include Robot

  def tick events
    accelerate 1
    turn 5
    fire 1.0e+000 unless events['robot_scanned'].empty?
    turn_gun 30
    turn_radar 0

  end
end
