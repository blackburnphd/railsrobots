#-------------------------------------------------------------------------------
# StrategyDuck.rb
#  A sample robot to show how you might use the new strategy system to control
#  a robot's behavior.  The strategy system still isn't completely done yet,
#  so a lot may change.
#-------------------------------------------------------------------------------
require_relative '../robust_robot'
require_relative '../strategies/strategy_factory'
require_relative '../strategies/talking_strategy'

class StrategyDuck
  include RobustRobot

  def initialize
    @strategies = []
    add_example_strategies()
  end
  
  # In a more advanced version of this robot, there would be either an external
  # surrogate or a separate system embedded within the class that would
  # populate and maintain the strategies array.  I also think, in the long run,
  # we'd want more control over that array, so that we could remove or rearrange
  # the individual strategy objects in there.  For now, it's more of a proof of
  # concept.
  def add_example_strategies()
    # This is actually SittingDuck's logic, except I commented  out the code to
    # turn the gun.  That goes in the next strategy.
    @strategies << StrategyFactory.create_strategy do |robot, hash|
      robot.turn_radar 5 if robot.time == 0
      robot.fire 3 unless robot.events["robot_scanned"].empty? 
      #robot.turn_gun 10
          
      @last_hit = robot.time unless robot.events['got_hit'].empty? 
      if @last_hit && robot.time - @last_hit < 20
          hash["stopping"] = false
          robot.accelerate(1)
      else
          hash["stopping"] = true
          robot.stop  
      end
    end
    
    # Here's the turn_gun call from SittingDuck's logic.  Just here to show you
    # that you can also define a dynamic strategy by passing it a string.  This
    # is important for supporting strategies delivered from a surrogate, which
    # can only communicate by sending text over TCP.
    @strategies << StrategyFactory.create_strategy("robot.turn_gun 10")
    
    
    # Something dumb just to show that we can pass data between strategies, and
    # that strategies can exist as prebuilt classes.
    @strategies << TalkingStrategy.new()
  end


  # This method doesn't have to be anything more than this.  In a more advanced
  # version, there might be code to report status back to a surrogate and
  # request any updates to the strategy set, or a call to a local method to
  # update strategies, but probably not much more than that.  Technically even
  # that stuff could be contained inside a strategy.
  def tick events
    # Re-initialize the hash every tick, so that we're not exposed to stale data
    @strategy_information = {}
    
    # Execute each attached strategy in sequence
    @strategies.each {|strategy| strategy.execute(self, @strategy_information)}
  end
end