require_relative '../robot'

class AdaptDuckBase
   include Robot

  def tick events
    #puts @gun_heat.to_s + ' for AdaptDuckBase'
    turn_radar 3 if time == 0
    fire 2 unless events['robot_scanned'].empty? 
    turn_gun 10
    turn 2
    
    if !events['got_hit'].empty?
      @hit_me = events['got_hit'][0][2].name
    end
    if @hit_me != nil
      say @hit_me
    end
    
    #say(recent_dpt.to_s())
  
  @last_hit = time unless events['got_hit'].empty? 
  if @last_hit && time - @last_hit < 20
      turn(30)
      accelerate(5)
      fire 2 unless events['robot_scanned'].empty? 
  else
      turn(5)
      accelerate(-2) 
  end
  end
end