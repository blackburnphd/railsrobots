require_relative 'AdaptingSelfHealingAgentTwoBranch'

class Adapting3_t500_e0p15_t1000_e0p05_HB < AdaptingSelfHealingAgentTwoBranch

def initialize
  
  

  
  
    puts "Initializing Adapting3_t500_e0p15_t1000_e0p05_HB"
    super()    
    # do not adapt before this time
    @adaptAtTime=[500,1000]
    # probability that the opponent will be misclassified
    @probabilityClassifierError = [0.15,0.05]
end

end
