require_relative '../robust_robot'
require_relative './StaticBasis'
class HB_0_0p1_1_2_2  < StaticBasis
  include RobustRobot
  def initialize
    super()
    @firing_strategy = 0
    @fire_power = 0.1
    @aiming_strategy = 1
    @movement_strategy = 2
    @resupply_strategy = 2
  end
end
