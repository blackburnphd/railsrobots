
require_relative '../robot'

class Static_v2_2
   include Robot

  def tick events
    accelerate 1
    turn 10
    fire 5.0e-001 unless events['robot_scanned'].empty?
    turn_gun 15
    turn_radar 0

  end
end
