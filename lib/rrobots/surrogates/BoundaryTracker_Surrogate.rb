require_relative 'surrogate_base'

class BoundaryTrackerSurrogate < SurrogateBase
  def initialize(port = 50000)
    super
    @last_switch_time = 0
  end
  
  def handle_command(command)
    command_segments = command.split()
    
    if command_segments[0] == "HIT"
      hit_time = command_segments[1].to_i()
      if hit_time - @last_switch_time > 100
        @last_switch_time = hit_time
        return "SWITCH"
      end
    end
    
    return "NOOP"
  end
end
