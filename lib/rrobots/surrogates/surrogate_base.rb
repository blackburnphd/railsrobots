#############################################################################################
# SurrogateBase
#  Base class for all surrogates, including the surrogate server.
#
#############################################################################################
require 'socket'

class SurrogateBase
  attr_accessor :debug
  attr_accessor :port
  
  def initialize(port = 50000)
    # This is the port on which the surrogate will listen for connections
    @port = port
    @debug = false
    @server_thread = nil
  end
  
  # Runs the server in its own thread, then returns control
  def run_server
    # If the server is already running, don't try to run it again.
    if @server_thread == nil || @server_thread.status == false then
      @server_thread = Thread.new() do
        # Here we create a TCPServer object and listen for commands.  This hangs the thread, so we're
        # doing it in a separate thread.
        server = TCPServer.new(@port)
        
        loop do
          # Accept client connection and read command from its input stream.
          client = server.accept()
          command = client.gets().chomp() #chomp removes any \r\n nonsense.
          
          # Debug information        
          puts("Server at #{@port.to_s()} received command: #{command}") if @debug
          
          # Figure out what to send back.
          if command == "HALT" # HALT Command is hard coded in here so that we can break out of the listening loop
            puts("server at #{port.to_s()} sending command: HALTING") if @debug
            client.puts("HALTING") 
            break
          else
            # First handle base commands like PING, these override anything defined in a subclass.
            response = internal_handle_command(command)
            
            # If no base commands were detected, defer to the child class' handler
            response = handle_command(command) if response.empty?
          end
        
          if response.empty?
            # catch-all to make sure we don't hang the client accidentally.
            client.puts("\r\n")
          else
            puts("server at #{port.to_s()} sending command: #{response}") if @debug
            client.puts(response)
          end
          
          # Disconnect the client.
          client.close()
        end
        
        # We're about to stop the server.  This method is a hook for child classes to clean up resources.
        on_halt()
        
        server.shutdown()
        Thread.exit()
      end
      
      # Meanwhile, in the main thread...
      # Hit an issue when running out of GUI where the robot would try contacting the server before it was up and running,
      # because the server startup happens in a separate thread.  To prevent that, I added the PING command.
      # Basically, don't return control until the server is running in its thread.
      server_is_running = false
      until server_is_running == true do
        begin
          socket = TCPSocket.open('localhost', @port)
          socket.puts("PING")
          server_is_running = socket.gets().chomp() == "PONG"
        rescue
          # Ignore exceptions
        end
      end
    end
  end
  
  # Base commands shared by all surrogates
  def internal_handle_command(command)
    if command == "PING"
      return "PONG"
    end
    
    return ""
  end
  
  # Stops the server (and its thread) and returns control
  def halt_server
    #don't try to kill the server if it's not running
    if @server_thread != nil && @server_thread.status != false
      # Open a connection to the server and send the kill command
      socket = TCPSocket.open('localhost', @port)
      socket.puts("HALT")
      
      # Join the thread to let it finish up, then get rid of it.
      @server_thread.join()
      @server_thread = nil
    end
  end
  
  # Define these in your subclass
  def handle_command(command)
    return ""
  end
  
  def on_halt()
    
  end
end