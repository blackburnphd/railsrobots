require_relative 'surrogate_base'

class SurrogateServer < SurrogateBase
  
  def initialize(port = 50000)
    super
    @surrogates = []
  end
  
  def handle_command(command)
    command_segments = command.split()
    
    if command_segments[0] == "REQUEST"
      if command_segments.length() >= 2
        #Load the surrogate on a new port and add it to the array
        surrogate_port = @surrogates.length() + 1 + @port
        @surrogates[@surrogates.length()] = load_surrogate(command_segments[1], surrogate_port)
        
        # Return the port number as a string to the caller.
        return surrogate_port.to_s()
      else
        return "ERR INCOMPLETE"
      end
    end
    
    return "ERR UNRECOGNIZED"
  end
  
  def on_halt()
    @surrogates.each do |surrogate|
      surrogate.halt_server()
    end
  end
  
  def load_surrogate(surrogate_name, port)
    puts "Server at #{@port} loaded " + surrogate_name if @debug
    
    begin
      begin
        require_relative surrogate_name
      rescue LoadError
      end 
      begin
        require surrogate_name
      rescue LoadError
      end
    end
    
    surrogate = Object.const_get(surrogate_name).new(port)
    surrogate.debug = @debug
    surrogate.run_server()
    return surrogate
  end
end