class Behaviorrecord
  
  
  attr_reader :tanktype
  attr_reader :stimulus
  attr_reader :percent
  

  
  def initialize(src)
    raw = src.split("\t")
    @tanktype = raw[0]
    @stimulus = raw[1]
    
 #   puts "stimulus = "+raw[1]
    
    
    @percent = -1
    # candidateName contains strings
    @candidateName = Array.new
    # candidateCount contains integers
    @candidateCount = Array.new
    i=0
    ct=0
    
    raw.each_index do |index|
      ct = ct +1
      if ct<3 then
         next
      end
  
      if i==0 then 
        @candidateName.push raw[index]
      end

      if i==1 then
        i=0
        @candidateCount.push raw[index].to_i
      end
      ct = ct + 1
      i = i + 1
      if i==2 then
        i = 0
      end
    end
   end
  
  
  

  
  def addCandidate(cname,ct)
    index = getIndexOfCandidate(cname)
    if index == nil then
      @candidateNames.push cname
      @candidateCount.push ct
    end
    if index !=nil then
      i =  @candidateCount[index]
      @candidateCount[index]=i+ct
    end
  end
  
  # return the percentage chance of this identifying this key
  def getPercentIdentify(key)
      puts "Getting percent"
      total =0
      @candidateCount.each do |item|
        puts "count ="+item.to_s
        total = total + item
      end
      
      myindex = getIndexOfCandidate key
      numerator = @candidateCount[myindex]
      
      return (numerator / total)
  end
  
  
  def getSimpleReportLine
     s = @tanktype
     s = s +"\t"+ @stimulus
     @percent = getPercentIdentify(@tanktype)
     s = s + "\t" + @percent.to_s
    return s
  end
  
  def getIndexOfCandidate(cname)
    return @candidateNames.index(cname)
    
  end
end