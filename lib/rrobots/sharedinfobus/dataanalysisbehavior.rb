require_relative './behaviorrecord'

class Dataanalysisbehavior


    

  def initialize(filename)
    @sourcefile = filename
    @itemHash = Hash.new
    readFile   
  end
  
  #  Read in the file and store into the hash table
  def readFile
     lines = IO.readlines(@sourcefile)
     lines.each do |line|  
        if line.start_with?("#") then
          next
        end
        br = Behaviorrecord.new line
        existing_br = @itemHash[br.tanktype]
        if existing_br==nil then
          @itemHash[br.tanktype] = br
     #     puts "added tank type " + br.tanktype
        end 
      end
  end

# Write all lines to the report
  def issueReport(outputFileName)
    
    
    
    
    target = File.open(outputFileName, 'w')
    @itemHash.each do |key, value|       
      myline = value.getSimpleReportLine + "\n"
      target.puts myline
      puts "output line"
    end
    
    # now determine statistics for this 
    
    
    target.close
  end  

end

puts "Creating Report Generator"
reportgenerator = Dataanalysisbehavior.new("C:/Users/Brian/Dropbox/Conferences/CSER2014/Data/counterfile.xls")

puts "Issuing Report"
reportgenerator.issueReport("C:/Users/Brian/Dropbox/Conferences/CSER2014/Data/behaviorcorrected.xls")
puts "**** finished! ****"

