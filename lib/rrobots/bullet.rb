class Bullet
  attr_accessor :x
  attr_accessor :y
  attr_accessor :heading
  attr_accessor :speed
  attr_accessor :energy
  attr_accessor :dead
  attr_accessor :origin

  def initialize bf, x, y, heading, speed, energy, origin
    @x, @y, @heading, @origin = x, y, heading, origin
    @speed, @energy = speed, energy
    @battlefield, dead = bf, false
  end

  def state
    {:x=>x, :y=>y, :energy=>energy}
  end

  def tick
    # Make sure to add in logic to detect when a bullet hits a helicopter or cargo - BP 15 Jan
    return if @dead
    @x += Math::cos(@heading.to_rad) * @speed
    @y -= Math::sin(@heading.to_rad) * @speed
    @dead ||= (@x < 0) || (@x >= @battlefield.width)
    @dead ||= (@y < 0) || (@y >= @battlefield.height)
    @battlefield.robots.each do |other|
      if (other != origin) && (Math.hypot(@y - other.y, other.x - @x) < 40) && (!other.dead)
        explosion = Explosion.new(@battlefield, other.x, other.y)
        @battlefield << explosion
        damage = other.hit(self)
        origin.damage_given += damage
        origin.kills += 1 if other.dead
        @dead = true
      end
    end
    
    
    if @battlefield.helo != nil then
      if @battlefield.helo.isOnScreen then
        if @battlefield.closestBullet != nil then
          if @battlefield.closestBulletRange < 30 then
          #   puts "BANG dist =" + @battlefield.closestBulletRange.to_s + " total damage = " + @battlefield.helo.damage.to_s
            @battlefield.helo.takeHit(@energy)
            explosion = Explosion.new(@battlefield, @battlefield.closestBullet.x, @battlefield.closestBullet.y)
            @battlefield << explosion
            @battlefield.closestBullet.dead = true
            if @battlefield.closestBullet != nil then
              @battlefield.closestBullet = nil
            end
            # puts "dead bullet"
          end
        end
      end
    end
    
    

    
    @battlefield.cargoSupplyLocations.each do |cargo|
      
      if cargo.x < 0 then
        next
      end
      
      if cargo.dead == false then
        if cargo.closestBullet == self then
          if cargo.closestRange < 30 then
            cargo.takeHit(@energy)
            explosion = Explosion.new(@battlefield, cargo.x*2 -10, cargo.y*2-10)
            @battlefield << explosion
            cargo.closestBullet.dead = true
            if cargo.dead==true then
              @battlefield.announceDeadCargo(cargo.name)
            end
          end
        end
      end
    end
    
  end
  
end