require 'descriptive-statistics'

require_relative 'battlereport'
require_relative 'battlestatistic'
require_relative 'robotpoint'
require_relative 'resupply'

class Battlefield
   attr_reader :width
   attr_reader :height
   attr_reader :robots
   attr_reader :teams
   attr_reader :bullets
   attr_reader :explosions
   attr_reader :time
   attr_reader :seed
   attr_reader :timeout  # how many ticks the match can go before ending.
   attr_reader :game_over
     

   
   attr_reader :time_tick_100
   
   # AP 2012-11-17 - Added experiment_id
   attr_reader :experiment_id
   
   attr_reader :history1
   attr_reader :history2
   attr_reader :myBattleStatistic1
   attr_reader :myBattleStatistic2
   
   attr_reader :cargoSupplyLocations
   attr_reader :helo
   
   attr_accessor :closestBullet
   attr_accessor :closestBulletRange

  def initialize width, height, timeout, seed, experiment_id = 0
    @helo = nil
    @width, @height = width, height
    @seed = seed
    @time = 0
    @robots = []
    @teams = Hash.new{|h,k| h[k] = [] }
    @bullets = []
    @explosions = []
    @timeout = timeout
    @game_over = false
    srand @seed
    @experiment_id = experiment_id
    @time_tick_reader =0
    
    @history1 = Array.new
    @history2 = Array.new
    @startingPositions = Array.new
   
   
    @startingX1 =0
    @startingX2 =0
    @startingY1 =0
    @startingY2 =0
    @collectedStartingPosition = false
    @lastShot1 =0
    @lastShot2 =0
    @allowedToSaveHistory = false
    
    @cargoSupplyLocations = Array.new
    
    @closesBullet = nil
    @closestBulletRange = 999999
    @cargoindex=0
    
    @newestCargo = nil
    
    prepareHistoryFile
  end


  def addResupplyLocation(x,y,healing_energy)
      resup= Resupply.new(x,y,healing_energy)
      @cargoindex = @cargoSupplyLocations.length
      resup.name ="cargo_"+ @cargoindex.to_s
      
      @cargoSupplyLocations << resup
      @newestCargo = resup
  end
  
  def removeResupplyLocation(x,y)
     @cargoSupplyLocations.each do |item|
       if item.x == x then
         if item.y == y then
           @cargoSupplyLocations.delete(item)
           if @newestCargo==item then
             @newestCargo = nil
           end
         end
       end
     end
     
  end



  def << object
    case object
    when RobotRunner
      @robots << object
      @teams[object.team] << object
    when Bullet
      @bullets << object
    when Explosion
      @explosions << object
    end
  end

# specify the name of the helo being used
  def setHeloName heloName
    
    mydir=File.expand_path(File.dirname(File.dirname(__FILE__)))
    require_relative './robots/' + heloName
    @helo = Object.const_get(heloName).new
    @helo.battlefield = self
    
  end
  
  
  

  def prepareHistoryFile
    # puts "Preparing History File"
    # f = File.expand_path("~/history.txt")
    # puts f
    # @@myHistoryFile =  File.open(f, 'a')
    
    
  end

  def saveHistory  
    if @allowedToSaveHistory == true then
        f = File.expand_path("~/historyworking250.xls")
        @myHistoryFile =  File.open(f, 'a')
        r1 = @myBattleStatistic1.report
        if $hasInitialized!=true then
          $hasInitialized=true     
          @myHistoryFile.write(@myBattleStatistic1.heading+"\n")
        end
        @myHistoryFile.write(r1+"\n")
        r2 = @myBattleStatistic2.report
        @myHistoryFile.write(r2+"\n")
        @myHistoryFile.close
    end
   
  end

  def clearHistoryArray
    #don't forget descriptive-statistics
  
    @history1.clear
    @history2.clear
    @startingPositions.clear
  end
  
  

  def processHistory
  
    

    first=true
    lasttime=0
    avgXList = Array.new
    avgYList = Array.new
    avgDist = Array.new
    avgTurretRotation = Array.new
    shotsList = Array.new
    powerList = Array.new
    lastTurretRotation1= -9999
    lastTurretRotation2= -9999
    deltaTurretRotation=0
    ct_good=0
    ct_bad=0
    
    ##########################
    # Statistic 1
    ##########################    
     @myBattleStatistic1 = Battlestatistic.new     
     @history1.each do |element|
       @myBattleStatistic1.tankClass = element.tankClass
       if(first==true)
          @myBattleStatistic1.mintime = element.time 
          first=false
       end
       @myBattleStatistic1.maxtime = element.time
       avgXList.push element.x
       avgYList.push element.y
       avgDist.push element.distToWall
       avgTurretRotation.push element.turretRotation
       deltaTurretRotation = lastTurretRotation1- element.turretRotation
       if deltaTurretRotation > 180 then
         deltaTurretRotation = 360 - deltaTurretRotation
       end
       lastTurretRotation1 = element.turretRotation
       if deltaTurretRotation < 0 then
         deltaTurretRotation = deltaTurretRotation *(-1)
       end
       
      
       isSpecial = false
       
       if (deltaTurretRotation == 5) ||(deltaTurretRotation == -10)||(deltaTurretRotation == 15) then
           isSpecial = true;
           ct_good = ct_good+1
           if @allowedToSaveHistory ==true then
             if @allowedToSaveHistory ==true then
               puts "*good "+deltaTurretRotation.to_s
             end
           end
       else
           ct_bad = ct_bad+1
           if @allowedToSaveHistory == true then
             if @allowedToSaveHistory ==true then
               puts "*bad "+deltaTurretRotation.to_s
             end
           end
       end
       
       shotsList.push element.shots
      
       powerList.push element.lastShotPower.to_f
       
    end
    
    statsX = DescriptiveStatistics::Stats.new(avgXList)
    statsY = DescriptiveStatistics::Stats.new(avgYList)
    statsDist = DescriptiveStatistics::Stats.new(avgDist)
    statsTurret = DescriptiveStatistics::Stats.new(avgTurretRotation)
    statsShots =DescriptiveStatistics::Stats.new(shotsList)
    
   
    statsPower = DescriptiveStatistics::Stats.new(powerList)
    
    
    
    @myBattleStatistic1.avgX = statsX.mean
    @myBattleStatistic1.avgY = statsY.mean
    @myBattleStatistic1.stdevX = statsX.standard_deviation
    @myBattleStatistic1.stdevY = statsY.standard_deviation
    @myBattleStatistic1.distToWall= statsDist.mean
    @myBattleStatistic1.avgTurretRotation = statsTurret.mean
    @myBattleStatistic1.stdevTurretRotation=statsTurret.standard_deviation
    @myBattleStatistic1.shots = statsShots.sum
    if ct_bad>0 then
       @myBattleStatistic1.suspectedAimingStrategy=1
    else
       @myBattleStatistic1.suspectedAimingStrategy=0
    end
    
  
    
    @myBattleStatistic1.lastShotPower = statsPower.median
    
    

    ##########################
    # Statistic 2
    ##########################
    

    first=true
    lasttime=0
    avgXList.clear
    avgYList.clear
    avgDist.clear
    avgTurretRotation.clear
    shotsList.clear
    powerList.clear
    
     @myBattleStatistic2 = Battlestatistic.new     
    @history2.each do |element|
      @myBattleStatistic2.tankClass = element.tankClass
      if(first==true)
        @myBattleStatistic2.mintime = element.time 
        first=false
      end
      @myBattleStatistic2.maxtime = element.time
      avgXList.push element.x
      avgYList.push element.y
      avgDist.push element.distToWall
      avgTurretRotation.push element.turretRotation
      shotsList.push element.shots
      powerList.push element.lastShotPower.to_f
      
      
       deltaTurretRotation = lastTurretRotation2 - element.turretRotation
       if deltaTurretRotation > 180 then
         deltaTurretRotation = 360 - deltaTurretRotation
       end
       
       lastTurretRotation2 = element.turretRotation
       if deltaTurretRotation < 0 then
         deltaTurretRotation = deltaTurretRotation *(-1)
       end
       
       ct_good =0
       ct_bad =0
       isSpecial = false
       
       if (deltaTurretRotation == 15) ||(deltaTurretRotation == -10)||(deltaTurretRotation == 5) then
           isSpecial = true;
           ct_good = ct_good+1
         else
           ct_bad = ct_bad+1
         end
       
       
      
    end
    
    # compute starting distances
    
    r0 = robots[0]
   
    st0 = @startingPositions[0]
  
    r0pt = Robotpoint.new
    r0pt.fromRobot r0
    d0 = st0.distance r0pt 
   
   
    r1 = robots[1]
    st1 = @startingPositions[1]
    r1pt = Robotpoint.new
    r1pt.fromRobot r1
    d1 = st1.distance r1pt 
   
    @collectedStartingPosition=false
  
    statsX = DescriptiveStatistics::Stats.new(avgXList)
    statsY = DescriptiveStatistics::Stats.new(avgYList)
    statsDist = DescriptiveStatistics::Stats.new(avgDist)
    statsTurret = DescriptiveStatistics::Stats.new(avgTurretRotation)
    statsShots =DescriptiveStatistics::Stats.new(shotsList)
    statsPower = DescriptiveStatistics::Stats.new(powerList)
     
    @myBattleStatistic2.avgX = statsX.mean
    @myBattleStatistic2.avgY = statsY.mean
    @myBattleStatistic2.stdevX = statsX.standard_deviation
    @myBattleStatistic2.stdevY = statsY.standard_deviation
    @myBattleStatistic2.distToWall= statsDist.mean
    @myBattleStatistic2.avgTurretRotation = statsTurret.mean
    @myBattleStatistic2.stdevTurretRotation=statsTurret.standard_deviation
    @myBattleStatistic2.shots = statsShots.sum
    @myBattleStatistic2.lastShotPower = statsPower.median
    if  ct_bad > 0 then
       @myBattleStatistic2.suspectedAimingStrategy=1
    else
       @myBattleStatistic2.suspectedAimingStrategy=0
    end
    
    
    
    
    @myBattleStatistic2.distTraveled=d1
 
  end

    @closesBullet = nil
    @closedBulletRange = 999999
    
    
    
  ##############################################################
  #                                                            #
  #                           tick                             #
  #                                                            #
  ############################################################## 
  def tick
    
   #Move helo first
   if @helo != nil then
     @myevents = Hash.new
     @myevents[:mytime]=@time
     
     @helo.tick(@myevents)
   end
   
   
   
    
    explosions.delete_if{|explosion| explosion.dead}
    explosions.each{|explosion| explosion.tick}

    bullets.delete_if{|bullet| bullet.dead}
    bullets.each{|bullet| bullet.tick}

    # Pick the closest bullet from the helo    
    @closestBulletRange = 99999999
    @closesBullet = nil
     if @helo != nil then
      bullets.each do |bullet| 
        if bullet.dead then
          next
        end
         dx = (bullet.x/2) - @helo.heloX
         dy = (bullet.y/2) - @helo.heloY
         dd = Math.sqrt( (dx*dx)+(dy*dy))  
     #   dd = Math.hypot( (bullet.y/2)-@helo.heloY , (bullet.x/2) - @helo.heloX)
        if dd<@closestBulletRange then
          @closestBulletRange = dd
          @closestBullet = bullet
        end
      end
    end
    
    
    # find the closest bullet to each cargo container
    @cargoSupplyLocations.each do |cargo|
      if cargo.dead == true then
        next
      end
      cargo.closestRange = 99999
      cargo.closestBullet = nil
      bullets.each do |bullet|
        if bullet.dead == true then
          next
        end
        
        dd = Math.hypot((cargo.y*2) - (bullet.y), (bullet.x) - (cargo.x*2))
        if dd<cargo.closestRange then
          cargo.closestRange = dd
          cargo.closestBullet = bullet
        end
      end
    end
    

      
    # @cargoSupplyLocations.each do |cargo|
       # if cargo.dead == true then
               # robots.each do |robot3|
                 # robot3.resupplyIsDestroyed(cargo.name)
               # end
            # end
    # end  
    
    @cargoSupplyLocations.delete_if{|cargo| cargo.dead}
  
    @time_tick_reader=@time_tick_reader+1
    
    # place all of the starting points on the list
    # if this is the first tick of the grading period
     if @collectedStartingPosition==false then
        @collectedStartingPosition = true
        robots.each do |robot2|
       
          pt = Robotpoint.new
          pt.x = robot2.x
          pt.y = robot2.y
          @startingPositions.push pt
        end
     end   

    myorder =0
    robots.each do |robot|
      begin
        # let the robot know where the helicopter is
        if @helo != nil then
          hx = @helo.heloX
          hy = @helo.heloY
          isonscr = @helo.isOnScreen
          
          robot.updateHeloInformation(hx,hy,isonscr)
        else  
          robot.updateHeloInformation(-999,-999,false)
        end
        
          
        
        # check to see if the robots have hit a resupply cargo container
        # if so, apply the healing.  BP 02-02-2014
        
        @cargoSupplyLocations.each do |cargo|
            if cargo.dead == true then
              
              next
            end
            dd = Math.hypot(cargo.y - (robot.y/2), (robot.x/2) - cargo.x)
          #  puts "distance = "+dd.to_s
            if dd<40 then
                robot.resupplyAction(cargo.energy)
                cargo.dead=true
            end
        end
        # notify all robots that a new cargo is available
        # note, I don't use the radar to detect cargo arrival, mostly because
        # the mechanic doesn't directly impact the behavioral decision process.  BP 2014-02-04
        if @newestCargo != nil then
           robot.resupplyArrives(@newestCargo.x,@newestCargo.y,@newestCargo.name)
        end
       
        
   
        
        
        # now execute the normal process 
        robot.send :internal_tick unless robot.dead
     #   sleep(0.02)
        
        br=Battlereport.new
        br.time = @time
        br.tankClass = robot.name
        br.x= robot.x
        br.y= robot.y
        br.speed = robot.speed
        br.heading = robot.heading
        br.gun_heading = robot.gun_heading
        br.distToWall = distanceToWall(robot.x,robot.y)
        br.turretRotation= robot.gun_heading
        br.lastShotPower = robot.getLastShot
        tempLastShot = @lastShot1
        if myorder==1 then 
           tempLastShot = @lastShot2
        end
        br.shots = robot.shots_fired - tempLastShot
        if myorder==0 then
          @history1.push br
          @lastShot1 = robot.shots_fired
        end
        
        if myorder==1 then
          @history2.push br
          @lastShot2 = robot.shots_fired
        end
   
        myorder = myorder+1

           
          
        
      rescue Exception => bang
        puts "#{robot} made an exception:"
        puts "#{bang.class}: #{bang}", bang.backtrace
        robot.instance_eval{@energy = -1}
      end
      
      
    end

   @newestCargo=nil

    if @time_tick_reader==250 then
    #  puts "Processing 250 step history at time ="+@time.to_s
      @time_tick_reader = 0
      processHistory
      saveHistory
      clearHistoryArray
    end
    

    @time += 1
    live_robots = robots.find_all{|robot| !robot.dead}
    @game_over = (  (@time >= timeout) or # timeout reached
                    (live_robots.length == 0) or # no robots alive, draw game
                    (live_robots.all?{|r| r.team == live_robots.first.team})) # all other teams are dead
    not @game_over
  
  end

  def state
    {:explosions => explosions.map{|e| e.state},
     :bullets    => bullets.map{|b| b.state},
     :robots     => robots.map{|r| r.state}}
  end


  # determine how many pixels are between these two points.
  def distanceBetween(x1,y1,x2,y2)
     dx = x2-x1
     dy = y2-y1
     dx = dx * dx
     dy = dy * dy
     distance = Math.sqrt(dx + dy)
     return distance
  end


  # compute the shortest distance to any wall.
  def distanceToWall(x, y)
    x1 = x
    x2 = width - x
    xMin = x1
    if x2<x1 then
      xMin = x2
    end
    y1 =y
    y2 = height-y
    yMin = y
    if y2 < y1 then
      yMin = y2
    end
    if xMin < yMin then 
      return xMin
    end
    return yMin
  end
    
  def announceDeadCargo(cargo_name)
      robots.each do |myrobot|
              myrobot.resupplyIsDestroyed(cargo_name)
      end
  end


end
