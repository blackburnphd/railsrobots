module RobustRobot
  require_relative 'robot'
  include Robot
  
  attr_accessor :surrogate_server_host
  attr_accessor :surrogate_server_port
  attr_reader :surrogate_type
  
  
  #-------------------------------------------------------------
  # Reporting Functions
  # BP
  #-------------------------------------------------------------
  
  def getReportLine
    
  end
  
  
  
  
  #-------------------------------------------------------------
  # Communication functions
  #-------------------------------------------------------------  
  def send_to_surrogate(information)
    server_response = tcp_request(@surrogate_server_host, @surrogate_port, information)
    handle_surrogate_response(server_response)
  end
  
  def handle_surrogate_response(response)
    # Define this yourself.
  end
  
  def request_surrogate()
    server_response = tcp_request(@surrogate_server_host, @surrogate_server_port, "REQUEST " + @surrogate_type)
    @surrogate_port = server_response.to_i()
  end

  def tcp_request(hostname, port, request)
    begin
      socket = TCPSocket.open(hostname, port)
      socket.puts(request)
      return socket.gets().chomp()
    rescue
      #TODO: Better exception handling here.
    end
  end
  
  #-------------------------------------------------------------
  # Movement functions
  #-------------------------------------------------------------
  # Turn the body towards a specific heading
  def turn_towards(new_heading, maximum_turn = 360)
    turn(angle_distance_with_max(heading, new_heading, maximum_turn))
  end
  
  # Turn the gun towards a specific heading
  def turn_gun_towards(new_heading, maximum_turn = 360)
    turn_gun(angle_distance_with_max(gun_heading, new_heading, maximum_turn))
  end
  
  # Turn the radar towards a specific heading
  def turn_radar_towards(new_heading, maximum_turn = 360)
    turn_radar(angle_distance_with_max(radar_heading, new_heading, maximum_turn))
  end
    
  #-------------------------------------------------------------
  # Utility functions
  #-------------------------------------------------------------
  private

  # Calculate the difference between two angles, finding the shortest turn direction.
  def angle_distance(from_angle, to_angle)
    if to_angle > from_angle
      return angle_distance(to_angle, from_angle) * -1
    else
      if from_angle - to_angle > 180
        return (to_angle + 360) - from_angle
      else
        return (from_angle - to_angle) * -1
      end
    end
  end
  
  
  # Calculate the difference between two angles, finding the shortest turn direction,
  # and allowing the caller to specify that the result should be no greater than +/- maximum_angle
  def angle_distance_with_max(from_angle, to_angle, maximum_angle)
    angle = angle_distance(from_angle, to_angle)
    if angle > 0
      angle = [angle, maximum_angle].min()
    else
      angle = [angle, maximum_angle * -1].max()
    end  
  end
end

# Add a little enhancement to the Numeric class so that we can do things like point the
# gun 90 degrees relative to the body's heading.
class Numeric
  def relative_to(x)
    return (self + x) % 360
  end
end