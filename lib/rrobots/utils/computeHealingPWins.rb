require 'csv'

# This is where I mine data from
$sourcedir = "C:/Users/Brian/Dropbox/Journals/ACM Transactions on Autonomous and Adaptive Systems (TAAS)/data/"


R1WINS ="r1wins"
R2WINS = "r2wins"
$healArray = [[{},{},{ },{ }],
              [{ },{ },{ },{ }],
              [{ },{ },{ },{ }],
              [{ },{ },{ },{ }]]



def initializeArray
   $healArray.each do |arr|
    arr.each do |scoreObj|
        
        scoreObj["score"]=0
        scoreObj[R1WINS]=0
        scoreObj[R2WINS]=0
    end
  end
end

# Use the name to determine the healing strategy
def getHealStrategyFromName thename
  
  thename = thename.strip()
  len = thename.length
  len = len -1
  
  
  theStrategy = thename[len]
  return theStrategy.to_i
  
end

#Find the probability of one healing strategies winning against another
def computeHealStrategies
  
  left = "%-6s" % "0"
  left = left+ "%-6s" % "1"
  left = left+ "%-6s" % "2"
  left = left+ "%-6s" % "3"
  line = "_heal1_ "+left
  puts line
  $healArray.each_with_index do |arr, indexA|
    line = "%-4s" % indexA.to_s
    arr.each_with_index do |scoreObj, indexB|
       
       
       total = scoreObj[R1WINS].to_f + scoreObj[R2WINS].to_f 
       myscore = scoreObj[R1WINS].to_f/ total
       myscore = myscore.round(2)
       scoreObj["score"] =myscore
      
       line = line + "%-6s" % myscore  
    
    end
    puts line
  end
end


def reportHealScores
  $healArray.each_with_index do |arr, indexA|
    arr.each_with_index do |scoreObj, indexB|
        
        score = scoreObj["score"].to_f
    
    end
  end
end




def processFile(filename)
 puts "processing " + filename
 ct =0
 CSV.foreach(filename) do |row|
   
   if ct ==0 then 
     ct =1
     next
   end
   
   
   r1_name = row[1]
   r1_draws = row[47].to_i
   r1_wins = row[57].to_i
   r1_loss = row[50].to_i
   
   r2_name = row[58]
   r2_draws = row[104].to_i
   r2_wins = row[114].to_i
   r2_loss = row[107].to_i
   
 
   if r1_name ==nil then 
     next
   end
   if r2_name ==nil then
     next
   end
 
   r1_strategy = getHealStrategyFromName r1_name
   r2_strategy = getHealStrategyFromName r2_name


   stat = $healArray[r1_strategy][r2_strategy]
   stat[R1WINS] = stat[R1WINS]+ r1_wins
   stat[R2WINS] = stat[R2WINS]+ r2_wins
   
 
 
 end
end


# Check every file in that directory.  If the file ends with .CSV then
# call processFile  
def checkDir dirname
  
  Dir.glob(dirname+"/*.CSV") do |my_csv_file|
       processFile(my_csv_file) 
  end

end


# find all subdirectories.  Call checkDir for each of the subdirectories.
def checkFiles
  Dir.entries($sourcedir).select do |f|
    entirefile = $sourcedir +f
    if File.directory?(entirefile) then
      if f == ".." then 
        next
      end
      if f == "." then
        next
      end
      checkDir(entirefile)
    end   
  end
 
end








initializeArray()
checkFiles()
computeHealStrategies()
reportHealScores()
