require 'csv'

$sourcefile = "C:/Users/Brian/Documents/APTANA~1/railsrobots/output/Adapting3 t1000 e0p15 t2000 e0p05 H B .csv"

 # C:/Users/Brian/Documents/APTANA Studio 3 Workspace/railsrobots/output/export_export__HB_ting3_t1000_e0p15_t2000_e0p05_H_B.CSV
 # C:/Users/Brian/Documents/APTANA Studio 3 Workspace/railsrobots/output/export_export__HB_ting3_t500_e0p15_t3000_e0p05_H_B.CSV

$mainHash = {}
$arrayOfCandidates =[];
 
 
if File.exists?($sourcefile) == false then
  
  puts "can not fild "+$sourcefile
  return
else
  puts "file found"
end 
 

def processFileAllRobots(filename)
 puts "processing " + filename
 
 CSV.foreach(filename) do |row|
   
   r1_name = row[1]
   r1_draws = row[47].to_i
   r1_wins = row[57].to_i
   r1_loss = row[50].to_i
   
   r2_name = row[58]
   r2_draws = row[104].to_i
   r2_wins = row[114].to_i
   r2_loss = row[107].to_i
   
   
     obj1 = $mainHash[r1_name]
     if obj1 == nil then
       obj1 = {}
       obj1["name"] = r1_name
       obj1["draws"] = 0
       obj1["wins"] =0
       obj1["loss"] =0
       $mainHash[r1_name]=obj1
     end
     obj1["draws"] = obj1["draws"] + r1_draws
     obj1["wins"] = obj1["wins"] + r1_wins
     obj1["loss"] = obj1["loss"] + r1_loss
   
   
   
   
     obj2 = $mainHash[r2_name]
     if obj2 == nil then
       obj2 = {}
       obj2["name"] = r2_name
       obj2["draws"] = 0
       obj2["wins"] =0
       obj2["loss"] =0
       $mainHash[r2_name]=obj2
     end
     obj2["draws"] = obj2["draws"] + r2_draws
     obj2["wins"] = obj2["wins"] + r2_wins
     obj2["loss"] = obj2["loss"] + r2_loss
   
 end
end


def computeProbabilityOfWin
  puts "computing probabilities"
  $mainHash.each do |key,value|
    $arrayOfCandidates << value
    puts value
     total =  value["wins"] + value["loss"] + value["draws"]
     wins = value["wins"]
     
     pwin = wins.to_f / total.to_f
     value["pwin"]=pwin 
  end
end



def ensureNoZeroPwin
  $arrayOfCandidates.each do |item|
    if item["pwin"].nan? ==true then
      item["pwin"] = 0.0
    end
  end
end

def sortResults
  puts "sorting results"
  #|argonite| argonite[:full_name]
  $arrayOfCandidates.sort_by { |a| a["pwin"] }
end

def reportResults(onlyTop)
  puts "reporting"
  ct = 1
  
  newarray =  $arrayOfCandidates.sort_by { |a| a["pwin"] }
  
  newarray.each do |item|
    line = ct.to_s+". " + item["name"] + ":     " + item["pwin"].to_s
    puts line    
    
    if onlyTop == true then 
      return
    end
    ct = ct + 1
  end
end


processFileAllRobots($sourcefile)
computeProbabilityOfWin
ensureNoZeroPwin
sortResults
reportResults(false)
