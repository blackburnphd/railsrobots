class Vector2D
# This is general utility library for vector computations in 2D
# Author - Brian Phillips

  
  @x =0
  @y =0
  Pi = 3.141592653589793
  
  # set x coordinate
  def setX(xx)
    @x = xx
  end
  
  # return a string of the type (x,y)
  # tested 4/20/2013
  def stringify
    answer="("+@x.to_s()
    answer = answer + ","+@y.to_s()+")"
    return answer
  end
  
  
  # set y coordinate
  def setY(yy)
    @y = yy
  end
  
  # get the x coordinate
  def getX()
    return @x
  end
  
  # get the Y coordinate
  def getY()
    return @y
  end
  
  
  
  # set both coordinates
  # tested 4/20/2013
  def set(xx,yy)
    
    @x = xx  
    @y = yy
  end
  
  # set all coordinates from another vector
  # tested 4/20/2013
  def setVector(v)
    @x = v.getX()  
    @y = v.getY()
  end
  
  # add another vector to this vector and update this value
  # tested 4/20/2013
  def add(v)
    @x = @x + v.getX()
    @y = @y + v.getY()
  end
  
  # Add these two numbers to x and y respectively
  # tested 4/20/2013
  def addNum(dx,dy)
    @x = @x + dx
    @y = @y + dy
  end
  
  
  # subtract another vector from this vector and update this vector with the value
  # tested 4/20/2013
  def subtract(v)
    @x = @x - v.getX()
    @y = @y - v.getY()
  end
  
  # compute the magnitude of this vector
  # tested 4/20/2013
  def magnitude
    raw = (@x * @x) + (@y * @y)
    return Math.sqrt(raw)
  end
  
  # compute the dot product of this vector
  # tested 4/20/2013
  def dot(v)
    raw = (v.getX() * @x)+(v.getY()* @y)
    return raw
  end
  

 # multiply all properties by d
  # tested 4/20/2013
  def scale(d)
    setX( getX() * d)
    setY( getY() * d)
  end
  
  # Reduce this vector so that it has a magnitude of 1.0
  # tested 4/20/2013
  def normalize
    mag = magnitude()
    mag = 1.0 / mag
    scale(mag)
  end
  
  # Factory method to create a new vector based on the current vector
  # tested 4/21/2013
  def copyMake
    vv = Vector2D.new
    vv.setX( @x )
    vv.setY( @y)
    return vv
  end
  

 # given that this point, and another point v are in a screen coordinate system 
 # where ht is the height of the screen and 0,0 is the upper left origin, y increases
 # going down, calculate the absolute space angle between them
 # Returns the angle in radians
 def directionToRadFlipScreenCoordRad(v,ht)
   v1 = v.copyMake()
   myy = v1.getY()
   myy=ht-y
   v1.setY(my)
   
   v2 = copyMake()
   myy = ht -v2.getY() 
   v2.setY(myy)
   
   v1.subtract(v2)
   angle= Math.atan2(v2.getY(), v2.getX())
   
   
 end

 # given that this point, and another point v are in a screen coordinate system 
 # where ht is the height of the screen and 0,0 is the upper left origin, y increases
 # going down, calculate the absolute space angle between them
 # Returns the angle in degrees
 def directionToRadFlipScreenCoordDeg(v,ht)
   d = directionToRadFlipScreenCoordRad(v,ht)
   angle = d*(180 / Pi )
 end
  
  # Given this point v, determine the angle (in radians) toward the other vector v
  # tested 4/21/2013
  def directionToRad(v)
    # make sure this isn't colocated
    # if it is, then the angle is zero
    if v.getX()==@x then
      if v.getY()==@y then
        return 0
      end
    end
 
    # There are three points to this calculation
    # v0 is the current point
    # v1 is an imaginary point east (toward 0 degrees) of V0
    # v2 is the point that we are trying to find the angle to
    puts "copy stuff"
    v0 = copyMake()
    v1 = Vector2D.new
    v1.set(1,0)
    v2 = v.copyMake()
    
    puts "subtract stuff"
    # translate v2 so that the point of angle (v0) lies at the origin relative to v2
    v2.subtract(v0)
    v2.normalize

    puts "get deltas"
#    deltaY = v1.getY() - v2.getY()
#    deltaX = v2.getX() - v1.getX()
 
 
 
 
    deltaY = getY() -v.getY()
    deltaX = v.getX() - getX()
 
# If you want the the angle between the line defined by these two points and the horizontal axis:
# double angle = atan2(y2 - y1, x2 - x1) * 180 / PI;
  
   tempy = v2.getY()
   tempy = 0 - tempy
   tempx = v2.getX()
   v2.set(tempx,tempy)
    
    puts "solve radians"
    radians = Math.atan2(v2.getY(),v2.getX())
    # correct for netagive angles

    if radians < 0 then
      radians = radians + (2.0 * Pi)
    end
    
    
    return radians
    
  end


  # given this point v, determine the angle (in degrees)
  # toward the other vector v
  
  # tested 4/21/2013
  def directionToDeg(v)
    if v==nil then
      puts "v is not valid"
      return 0
    end
    puts "checking directionToRad"
    direction = directionToRad(v)
    
    return radToDegrees(direction)
  end

  # convert degrees to radians
  def degreesToRad(deg)
     radians = deg * (Pi / 180.0)
    return radians
  end

  # convert radians to degrees  
  # tested 4/21/2013
  def radToDegrees(rad)
    degrees = rad * (  180.0 / Pi)
    return degrees
  end
  
  # return the distance from this point to another
  def distance(v)
     dx = getX() - v.getX()
     dx = dx *dx
     dy = getY() - v.getY()
     dy = dy * dy
     return Math.sqrt(dx+dy)
  end
  
  
end
