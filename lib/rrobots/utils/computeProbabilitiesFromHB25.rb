require 'csv'

# This is where I mine data from
$sourcedir = "C:/Users/Brian/Dropbox/Journals/ACM Transactions on Autonomous and Adaptive Systems (TAAS)/data/"

$mainHash = {}

$PROCESS_ALL_ROBOTS=true
$pwin=0
$variance =0
$stdev=0
$winlist = []

def getProbabilityOfWin
  total = 0
  wins = 0
  $mainHash.each do |key,value|
    total = total + value["wins"] + value["loss"] + value["draws"]
    wins = wins + value["wins"] 
  end
  return wins.to_f / total.to_f
end


def getScore(win,loss,draw)
  t =  win.to_f + loss.to_f + draw.to_f
  s = win.to_f / t
  return s
end

def processFileAllRobots(filename)
 puts "processing " + filename
 
 CSV.foreach(filename) do |row|
   
   r1_name = row[1]
   r1_draws = row[47].to_i
   r1_wins = row[57].to_i
   r1_loss = row[50].to_i
   
   r2_name = row[58]
   r2_draws = row[104].to_i
   r2_wins = row[114].to_i
   r2_loss = row[107].to_i
   
   score1 = getScore(r1_wins,r1_loss,r1_draws)
   score2 = getScore(r2_wins,r2_loss,r2_draws)
   if score1.nan? ==false then
   $winlist << score1
   end
   if score2.nan? == false then
    $winlist << score2    
   end

   
   
     obj1 = $mainHash[r1_name]
     if obj1 == nil then
       obj1 = {}
       obj1["name"] = r1_name
       obj1["draws"] = 0
       obj1["wins"] =0
       obj1["loss"] =0
       $mainHash[r1_name]=obj1
     end
     obj1["draws"] = obj1["draws"] + r1_draws
     obj1["wins"] = obj1["wins"] + r1_wins
     obj1["loss"] = obj1["loss"] + r1_loss
   
     
   
   
     obj2 = $mainHash[r2_name]
     if obj2 == nil then
       obj2 = {}
       obj2["name"] = r1_name
       obj2["draws"] = 0
       obj2["wins"] =0
       obj2["loss"] =0
       $mainHash[r2_name]=obj2
     end
     obj2["draws"] = obj2["draws"] + r2_draws
     obj2["wins"] = obj2["wins"] + r2_wins
     obj2["loss"] = obj2["loss"] + r2_loss
   
 end
end


def processFile(filename)
 puts "processing " + filename
 
 CSV.foreach(filename) do |row|
   
   r1_name = row[1]
   r1_draws = row[47].to_i
   r1_wins = row[57].to_i
   r1_loss = row[50].to_i
   
   r2_name = row[58]
   r2_draws = row[104].to_i
   r2_wins = row[114].to_i
   r2_loss = row[107].to_i
   
   score1 = getScore(r1_wins,r1_loss,r1_draws)
   score2 = getScore(r2_wins,r2_loss,r2_draws)
   
  
   
   if r1_wins > r1_loss then
     obj1 = $mainHash[r1_name]
     if obj1 == nil then
       obj1 = {}
       obj1["name"] = r1_name
       obj1["draws"] = 0
       obj1["wins"] =0
       obj1["loss"] =0
       $mainHash[r1_name]=obj1
     end
     obj1["draws"] = obj1["draws"] + r1_draws
     obj1["wins"] = obj1["wins"] + r1_wins
     obj1["loss"] = obj1["loss"] + r1_loss
     $winlist << score1
   end
   
   
   if r2_wins > r2_loss then
   
     obj2 = $mainHash[r2_name]
     if obj2 == nil then
       obj2 = {}
       obj2["name"] = r1_name
       obj2["draws"] = 0
       obj2["wins"] =0
       obj2["loss"] =0
       $mainHash[r2_name]=obj2
     end
     obj2["draws"] = obj2["draws"] + r2_draws
     obj2["wins"] = obj2["wins"] + r2_wins
     obj2["loss"] = obj2["loss"] + r2_loss
      $winlist << score2
   end
 end
end

def checkDir dirname
  
  Dir.glob(dirname+"/*.CSV") do |my_csv_file|
    if $PROCESS_ALL_ROBOTS then
       processFileAllRobots(my_csv_file)
    else
       processFile(my_csv_file) 
    end
    
  end
  
end


def checkFiles
	Dir.entries($sourcedir).select do |f|
	  entirefile = $sourcedir +f
	  if File.directory?(entirefile) then
	    if f == ".." then 
	      next
	    end
	    if f == "." then
	      next
	    end
	    checkDir(entirefile)
	  end	  
	end
	$pwin = getProbabilityOfWin
	
	puts "Probability of win = " +($pwin * 100).to_s
end



def computeVariance
  
  runningTotal =0
  ct =0
  $winlist.each do |prob|
    delta = prob.to_f - $pwin
    delta = delta * 100.0
    delta = delta * delta
    runningTotal = runningTotal + delta
    ct = ct +1
  end
  $variance = runningTotal.to_f / ct.to_f
  $stdev =  Math.sqrt($variance)
end

def report
  puts "Probability of win = " +($pwin * 100).to_s
  puts "variance = " + $variance.to_s
  puts "standard deviation = " + $stdev.to_s
end





checkFiles()
computeVariance()
report()
