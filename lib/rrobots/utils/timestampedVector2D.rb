require_relative 'vector2D'

class TimestampedVector2D < Vector2D
  
  @timestamp=0
  
  def setTime(t)
    @timestamp=t
  end
  
  def getTime
    return @timestamp
  end
  
  # return this time minus last time
  def timeSince(tV )
    return getTime() - tV.getTime()
  end
  
  # Linear interpolation of this time and another time
  # returns another TimestampedVector2D
  def linearInterpolate(newtime,tV)  
    timeDistance = timeSince(tV)
    smallDistance = @timestamp - newtime
    timeRatio = smallDistance / timeDistance
    currentMark = copyMake()
    currentMark.subtract(tV)
    currentMark.scale(timeRatio)
    currentMark.addVector(tV)
    return currentMark
  end


  

  
end