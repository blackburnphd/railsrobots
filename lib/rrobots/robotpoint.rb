class Robotpoint

  attr_accessor :x
  attr_accessor :y
  
  def initialize
    @x=0
    @y=0
  end
  
  def distance(pt)
    dx =  (pt.x - @x)
    dy = (pt.y - @y)
    dx2 = dx * dx
    dy2 = dy*dy
    d = Math.sqrt(dx2+dy2)
    return d
  end

  def fromRobot(r)
    @x = r.x
    @y = r.y
  end

end
