require_relative 'base_worker'
require_relative 'experiment_summary_worker'

class ExperimentWorker
  include BaseWorker
  
  attr_accessor :experiment_id, :degree_of_parallelism, :robot_list, :run_from_background
  attr_reader :matches_started, :matches_completed, :match_count
  
  def initialize
    super
    
    # Default value.  I made it a property in case we want to change it.
    @degree_of_parallelism = 5
    @robot_list = nil
    @run_from_background = false
  end
  
  # Main method that does all the work.
  def run
      puts "*****  Running Experiment Worker  *************"
      Rails.logger.info("YYYY Running Experiment Worker  YYYY")
    # Step 1: Validate input
    if @experiment_id == nil or @experiment_id <= 0
      @status_message = "Error: Missing experiment_id property"
      Rails.logger.info("YYYY DEATH - no experiment id  YYYY")
      return false
    end
    
    
    # Step 2: Load Experiment object
    # NOTE: At this point, we're loading the experiment object directly.  We're still in the main thread, and if the
    #       worker is being called as part of a controller response, trying to use ActiveResource here will cause a
    #       resource deadlock.  Once we enter multithreaded space, we start using ActiveResource.
    experiment = nil
    begin
      if @run_from_background
        experiment = ExperimentResource.find(@experiment_id)
        Rails.logger.info("YYYY Running from background  YYYY")
      else
        experiment = Experiment.find(@experiment_id)
        Rails.logger.info("YYYY NOT running from background  YYYY")
      end
    rescue
    end
    
    if experiment == nil
      @status_message = "Error: Unable to locate experiment #{@experiment_id}"
      Rails.logger.info("YYYY DEATH - Unable to locate experiment experiment id  YYYY")
      return false
    end
    
    
    # Step 3: Check conditions for being able to run experiment
    if experiment.running?
      # Experiment is already running.  Can't run it again.
      @status_message = "Experiment is already running."
      Rails.logger.info("YYYY DEATH - experiment is already running  YYYY")
      return false
      
    elsif experiment.completed?
      # Experiment has already completed.  Can't run it again without resetting it.
      @status_message = "Experiment has already been completed.  You must reset the experiment before it can be run again."
      Rails.logger.info("YYYY DEATH - experiment has already completed  YYYY")
      return false
      
    else
      # Finally, we're ready to actually run the experiment.
      # First things first, mark the experiment as running.
      experiment.running = true
      Rails.logger.info("YYYY try to save  YYYY")
      experiment.save()
      Rails.logger.info("YYYY saved  YYYY")
        
      # Grab some info off the ActiveRecord Experiment object, then we're done with it.  This way we don't have
      # to worry about any threading issues.
      if @robot_list == nil
        if @run_from_background
          # TODO: Write this (take from batch worker?  Put in base worker?)
          
          Rails.logger.info("YYYY DEATH - This code not implemented  YYYY")
          @robot_list = experiment.robots.map {|r| [r.file_name, r.id]}
        else
            Rails.logger.info("YYYY not running in background  YYYY")
          @robot_list = experiment.robots.map {|r| [r.file_name, r.id]}
        end
      end
      @match_count = experiment.match_count
      
      Rails.logger.info("YYYY preparing for helo YYYY")
      puts  "******   preparing for helo ************"
      helos = Helicopter.where("expid = ?",@experiment_id)
      Rails.logger.info("YYYY get helo name  YYYY")
      @heloname = helos[0].name;
      puts "*** helo name = "+@heloname+" *****"
      Rails.logger.info("YYYY spawn worker thread  YYYY")
      # Kick off the worker thread that will handle the rest.
      spawn_worker_thread()
      Rails.logger.info("YYYY running  YYYY")
      @status_message = "Experiment is running."
      return true
    end
  end  
  
  private
  
  # This method creates the worker thread that spawns child threads to run matches in parallel.
  def spawn_worker_thread    
    
    puts "*****  Spawning Worker Thread  *************"
    # Reset counters
    @matches_started = 0
    @matches_completed = 0
    
    # Spawn the worker thread and let it work.
    @worker_thread = Thread.new() do
      # We're going to remain in a loop here, kicking off matches until a certain number (degree_of_parallelism)
      # are running, then we'll sleep for a second and see how many new ones we can kick off.  Once all of the
      # matches are running, we'll just keep sleeping for a second and repeating, and once all of the matches
      # have completed, we'll exit the loop.
      while @matches_completed < @match_count
        Rails.logger.info("YYYY run_match  YYYY")
        [@match_count - @matches_started, @degree_of_parallelism - (@matches_started - @matches_completed)].min().times do
          run_match()
        end
        
        # Wait 1 second and check again.
        sleep(1)
      end
      
      # All matches are now complete.
      
      # Use ActiveResource to avoid potential threading issues with ActiveRecord.
      experiment_resource = ExperimentResource.find(@experiment_id)
      experiment_resource.running = false
      experiment_resource.calculating_summary = true
      experiment_resource.save()
      
      # Calculate the summary results.
      summary_worker = ExperimentSummaryWorker.new()
      summary_worker.callback_site = @callback_site
      summary_worker.experiment_id = @experiment_id
      summary_worker.run()
      summary_worker.join()
      
      experiment_resource.calculating_summary = false
      experiment_resource.completed = true
      experiment_resource.save()
      
      Thread.exit()
    end
  end
  
  
  # Run a single match, updating the count variables on the class as appropriate.
  def run_match()
    puts "*****  Running Match  *************"
    # Increment the number of matches started
    @matches_started += 1
    
    # Spawn a new thread.
    Thread.new() do
      # Call the actual command
      
      # Added helicopter logic 26 Jan 2014 - BP

      
      

      # previous value /lib/rrobots/rrobots.rb  BP 22 Nov 2014
      
      command = "ruby /home/hatteam/repo/railsrobots/lib/rrobots/rrobots.rb -nogui -experiment=" + @experiment_id.to_s() + " -helo="+@heloname+" " + @robot_list.map {|r| r[0]}.join(" ")
      

Rails.logger.info("YYYY command= "+command+"  YYYY")

      sub = IO.popen(command)
      results_string = sub.read()
      sub.close()
      
      Rails.logger.info("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      Rails.logger.info(results_string)
      Rails.logger.info("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      
      
      # We read the output of the rrobots.rb call into a string.  Check for the RailsRobots specific YAML output,
      # parse it, and send it back to the server if it's there.
      start_index = results_string.index("# Begin RailsRobots Output")
      end_index = results_string.index("# End RailsRobots Output")
      if start_index != nil and end_index != nil
        require 'yaml'
        results = YAML::load(results_string[start_index, end_index])
       
        # Create new Match object, and call save to get the id.
        match = MatchResource.new()
        match.experiment_id = @experiment_id
        match.duration = results["duration"]
        match.save()
        
        results["robots"].each do |robot_info|
          # Create a new result for each robot, and save to get the id.
          result = ResultResource.new()
          
          # Figure out which robot id to use based on the robot's file name.
          selected_robot = @robot_list.select do |array|
            array.include?(robot_info[0])
          end
          result.robot_id = selected_robot.first()[1]
         
          result.parent_type = 'Match'
          result.parent_id = match.id
          result.save()
          
          robot_info[1].each do |data_point|
            # Add a new data point for each item passed to us in the YAML.
            new_data_point = DataPointResource.new()
            new_data_point.data_key = data_point[0]
            new_data_point.data_value = data_point[1]
            new_data_point.parent_id = result.id
            new_data_point.parent_type = 'Result'
            new_data_point.save()
          end
        end
      end
      
      # Update the number of matches completed
      @matches_completed += 1;
      
      # Done
      Thread.exit()
    end
  end
end