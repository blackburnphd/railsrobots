require_relative 'base_worker'

class ExperimentSummaryWorker
  include BaseWorker
  
  attr_accessor :experiment_id
  
  def run
    # Step 1: Validate input
    if @experiment_id == nil or @experiment_id <= 0
      @status_message = "Error: Missing experiment_id property"
      return false
    end
    
    # Worker thread does the rest in the background.
    @worker_thread = Thread.new() do
      # Step 2: Clear any existing result records.
      ResultResource.find(:all, :params => {:experiment_id => @experiment_id}).each do |result|
        result.destroy()  
      end
      
      # Step 3: Gather the data we'll need.
      summary_data = ExperimentResource.find(:all, :params => {:experiment_id => @experiment_id, :summary => true})
      summary_results = Hash.new()
      summary_data_points = Hash.new()
      
      # Step 4: Loop
      summary_data.each do |row|
        # This is the first row for this robot, so we need to create a new result object.
        if summary_results[row.robot_id] == nil
          summary_results[row.robot_id] = ResultResource.new()
          summary_results[row.robot_id].robot_id = row.robot_id
          summary_results[row.robot_id].parent_id = @experiment_id
          summary_results[row.robot_id].parent_type = "Experiment"
          summary_results[row.robot_id].save()
        end
        
        # Create minimum value data point
        min_value = DataPointResource.new()
        min_value.data_key = "min_" + row.data_key
        min_value.data_value = row.min_data_value
        min_value.parent_id = summary_results[row.robot_id].id
        min_value.parent_type = "Result"
        min_value.save()
        
        # Create maximum value data point
        max_value = DataPointResource.new()
        max_value.data_key = "max_" + row.data_key
        max_value.data_value = row.max_data_value
        max_value.parent_id = summary_results[row.robot_id].id
        max_value.parent_type = "Result"
        max_value.save()
        
        # Create average value data point
        avg_value = DataPointResource.new()
        avg_value.data_key = "avg_" + row.data_key
        avg_value.data_value = row.avg_data_value
        avg_value.parent_id = summary_results[row.robot_id].id
        avg_value.parent_type = "Result"
        avg_value.save()
        
        # Create total value data point
        tot_value = DataPointResource.new()
        tot_value.data_key = "tot_" + row.data_key
        tot_value.data_value = row.tot_data_value
        tot_value.parent_id = summary_results[row.robot_id].id
        tot_value.parent_type = "Result"
        tot_value.save()
      end
      
      @status_message = "Summary results calculated"
      Thread.exit()
    end
    
    @status_message = "Calculating summary results" 
  end  
end
