require_relative 'base_worker'
require_relative 'experiment_worker'

class ExperimentBatchWorker
  include BaseWorker
  
  attr_accessor :experiment_ids
  
  def run
    
    
    
    if @experiment_ids == nil or !@experiment_ids.is_a? Array
      @status_message = "Missing experiment_ids values."
Rails.logger.info("XXXX Missing Experiment ID values XXXX")
      return false
    end
    
    @worker_thread = Thread.new() do
      @experiment_ids.each do |experiment_id|
        worker = ExperimentWorker.new()
        worker.callback_site = @callback_site
        worker.experiment_id = experiment_id
        worker.run_from_background = true
        worker.robot_list = get_robot_list_for_experiment(experiment_id)
        
  Rails.logger.info("XXXX Worker Running Experiment  XXXX")      
        
        worker.run()
        worker.join()
      end
      
      @status_message = "Batch complete."
      Thread.exit()
    end
    
    @status_message = "Batch of #{@experiment_ids.count} Experiment(s) is running."
  end

private

  def get_robot_list_for_experiment(experiment_id)
    Rails.logger.info("XXXX get robot list for experiments XXXX")
    # Lazy load main robot list
    if @robot_list == nil
      @robot_list = RobotResource.all().map {|r| [r.file_name, r.id]}
    end
    
    participants = ParticipantResource.find(:all, :params => {:experiment_id => experiment_id})
    experiment_robot_list = []
    participants.each do |participant|
      single_robot = @robot_list.select do |array|
          array.include?(participant.robot_id)
        end
      experiment_robot_list << single_robot[0]
      
    end
    Rails.logger.info("XXXX added to robot list XXXX")
    return experiment_robot_list
  end
end
