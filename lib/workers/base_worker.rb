# BaseWorker Module
# Mix in module to use in all worker classes.
require_relative "../mixins/resource_classes.rb"

module BaseWorker
  attr_accessor :status_message
  @worker_thread = nil
  
  # The callback_site property allows the caller to specify the url to use when invoking the ActiveResource classes.
  # The default is http://localhost:3000, but we've already encountered cases where that's not true.
  attr_reader :callback_site
  def callback_site=(value)
    # When the value on this class is updated, update the class objects of all resource classes.
    RobotResource.site = value
    ResultResource.site = value
    MatchResource.site = value
    ExperimentResource.site = value
    DataPointResource.site = value
    ParticipantResource.site = value
    
    @callback_site = value
  end
  
  # Get the current status of the worker thread.
  def thread_status
    if @worker_thread == nil
      return false
    else
      return @worker_thread.status
    end
  end
  
  # Issue a .join() call on the worker thread.
  def join
    if @worker_thread != nil and @worker_thread.alive?
      @worker_thread.join()
    end
  end
end