module DataPointInteractions
  # Gets the value of the specified data point, by name.
  def get_data_point(data_key)
    data_point = self.data_points.find_all {|d| d.data_key == data_key}.first()
    
    if data_point == nil
      return nil
    else
      return data_point.data_value
    end
  end
  
  # Creates or updates a data point with the given name.
  def set_data_point(data_key, data_value)
    data_point = self.data_points.find_all {|d| d.data_key == data_key}.first()
    
    if data_point == nil
      data_point = DataPoint.new()
      data_point.data_key = data_key
      self.data_points << data_point
    end
    
    data_point.data_value = data_value
  end
  
  # Creates or updates a data point with the given name by adding the specified value to it.
  # If a new data point is created, the value starts at 0.
  def increment_data_point(data_key, increment_value)
    data_point = self.data_points.find_all {|d| d.data_key == data_key}.first()
    if data_point == nil
      data_point = DataPoint.new()
      data_point.data_key = data_key
      data_point.data_value = 0
      self.data_points << data_point
    end
    
    data_point.data_value += increment_value
  end
end