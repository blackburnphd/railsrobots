# These resource classes use the ActiveRecord framework to consume our models using RESTful calls to the rails server.
# They are used by the worker classes (see lib/workers/base_worker.rb) to avoid threading issues while performing
# background tasks.
require 'active_resource'

class DataPointResource < ActiveResource::Base
  self.site = "http://localhost:3000"
  self.element_name = "data_point"
end

class ExperimentResource < ActiveResource::Base
  self.site = "http://localhost:3000"
  self.element_name = "experiment"
end

class MatchResource < ActiveResource::Base
  self.site = "http://localhost:3000"
  self.element_name = "match"
end

class ParticipantResource < ActiveResource::Base
  self.site = "http://localhost:3000"
  self.element_name = "participant"
end

class ResultResource < ActiveResource::Base
  self.site = "http://localhost:3000"
  self.element_name = "result"
end

class RobotResource < ActiveResource::Base
  self.site = "http://localhost:3000"
  self.element_name = "robot"
end